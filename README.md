![pipeline](https://gitlab.com/chocapix/chob/badges/master/pipeline.svg)

# chob

The boilerplate-free build system for C++ for Linux and MacOS. Supports gcc and clang.

## Description

`chob` is designed for small to medium sized projects, assuming they follow the
[pitchfork layout](https://joholl.github.io/pitchfork-website/).


### Features

- Dependencies via `pkg-config` or CMake's `find_package` or embedded subprojects in `external/`

- `ctest` integration:  `chob` can produce a CTestTestfile.cmake file.

- LSP integration: `chob` creates or updates a `compile_commands.json` file at the root of the build tree.

- (planned) Doxygen support

### History

Some time ago, I grew tired of having to explain to CMake where my sources were
and what to link with what. After all, why would I put a .cpp file in src/ if I
didn't want to compile it? Why would I write a main function if I didn't want
to build an executable?

My first solution was a custom makefile which called a python script which used
nm to figure out what to link with what. It quickly became unwieldy so I began
working on a C++ implementation of the whole thing. 11'000 lines of C++ later,
I have a tool that builds complete C++ projects all by itself in a single pass.

### Goals

- Speed

- Lowest possible amount of configuration for simple projects

### Non Goals

- Compete with CMake

- Windows Support

## Installation

`chob` can be installed from source using CMake or a previous (not too old) version of `chob` .

### Using `chob`:
```
$ git clone https://gitlab.com/chocapix/chob.git
$ mkdir chob/build
$ cd chob/build
$ chob .. --install=$HOME/.local
```

### Bootstraping `chob`:

If you neither have CMake nor `chob`, or your version of `chob` is too old to
build the current version, use the `tools/bootstrap.sh` script:
```
$ git clone https://gitlab.com/chocapix/chob.git
$ mkdir chob/build
$ cd chob/build
$ ../tools/bootstrap.sh ..
$ ./bootstrap/chob .. --install=$HOME/.local
```

### Using CMake:

```
$ git clone https://gitlab.com/chocapix/chob.git
$ mkdir chob/build
$ cd chob/build
$ cmake ..
$ cmake --build .
$ cmake --install . --prefix=$HOME/.local
```

## Usage

`chob` will build the given project into the current directory. There should be
a `chob.sh` script at the project root. Typically, you'll have a `build/`
directory at the root of your project and run `chob ..` in it. Altenatively, if
no arguments are given, `chob` will build the project at the current directory
into the `build/` subdirectory (equivalent to `(cd build; chob ..)`).

### Configuration syntax

The chob.sh file is a bash script sourced by `chob` that describe flags,
dependencies, etc. of the project via special functions that are defined prior
to sourcing chob.sh. For very small projects, an empty chob.sh (or no chob.sh at
all) is enough.

Here is a comprehensive example:
```
### compilation flags
#
# cflags += -std=c++20 # spaces around += are important
# cflags += -Wall -Wextra

### link flags
#
# lflags += ... 

### flags means both cflags and lflags
#
# flags += -fopenmp 

### flags can be context sensitive
#
# flags debug += -fsanitize=address # add this flag only in debug mode
# cflags src/foo.o += -Wno-conversion # only for this file

### dependencies through pkg-config
#
# pkg-config "fmt >= 10" sqlite3

### dependencies through CMake
#
# find_package Boost 1.82 COMPONENTS regex
# link_libraries Boost::regex

### chob imports all the compiler macros (ie the output of c++ -dM) into the
### environement. You can use them in shell code.
#
# if [ -n "$__x86_64__" ]; then
#    cflags src/sha/sha256_block_loop.o += -msse4.1 -msha
# fi
```

No need to list all source files. For `chob`, any file in `src/` with an
appropriate extension is a source file. For all source file that defines a
`main` function, an executable will be produced.

`chob` comes with the following default flags:
```
cflags release += -O3 -DNDEBUG
cflags debug += -g
cflags dev += -g -Og
```
The default mode is `release`.

### Running `chob`

```
$ ls 
chob.sh src build
$ cd build
$ chob ..
config  .
compile src/my_exe.o
link    my_exe
$ ls
_chob  my_exe  compile_commands.json
$ ./my_exe
....
```


By default, `chob` doesn't build tests, add `-t` to build tests. `-m` to change mode.
```
$ chob .. -tmdebug && ctest
....
```

### Install your project

To install:
```
$ chob .. --install=$HOME/.local
```
This will put the executables in `$HOME/.local/bin` and will copy the content
of `data/share` to `$HOME/.local/share` if any.

### What if my project is a library?

Add `lib mylib` to `chob.sh` and all non-test files in `src` will be linked
together to form a library.

