set -e

__var(){
    local __out __word

    __out="var "
    for __word in "$@"
    do
        __out="$__out$__word:"
    done
    echo "$__out" >&$__fd
}

cflags(){
    __var cflags "$@"
}
lflags(){
    __var lflags "$@"
}
flags(){
    cflags "$@"
    lflags "$@"
}

lib() {
    __var libname += "$@"
}
description() {
    __var description += "$@"
}
version() {
    __var version += "$@"
}

pkg-config(){
    local lib libs

    if [[ $__verbose > 0 ]]; then
        libs=$*
        libs="$(echo $libs | sed 's/\(>= [^ ]*\)//g')"
        libs="$(echo $libs | sed 's/\(<= [^ ]*\)//g')"
        libs="$(echo $libs | sed 's/\(= [^ ]*\)//g')"

        command pkg-config --modversion "$@" | \
            for lib in $libs; do
                read version
                echo Found $lib version $version
            done
    fi

    command pkg-config --print-errors "$@"
    __var pkg-requires += "$@"
    cflags += $(command pkg-config --cflags "$@")
    lflags += $(command pkg-config --libs "$@")
}

find_package() {
    echo "find_package $@" >&$__fd
}
link_libraries() {
    echo "link_libraries $@" >&$__fd
}

cflags release += -O3 -DNDEBUG
cflags debug += -g
cflags dev += -g -Og
