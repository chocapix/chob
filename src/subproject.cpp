#include <algorithm>
#include <cstdint>

#include "subproject.h"

#include <path.h>
#include <serial.h>
#include <str/algo.h>
#include <str/str.h>
#include <util/file.h>
#include <util/subprocess.h>

#include "common/error.h"
#include "common/fmt.h"
#include "common/print.h"
#include "compile_commands.h"
#include "configuration.h"
#include "depfile.h"
#include "platform/linkdeps.h"
#include "platform/platform.h"
#include "rebuilder/database.h"
#include "rebuilder/rebuilder.h"
#include "resource.h"
#include "util/timer.h"

using namespace std::string_literals;

namespace
{
constexpr std::int64_t pow10(int k) { return k == 0 ? 1 : 10 * pow10(k - 1); }
}  // namespace

int main_configure(std::string_view cfg_in,
                   std::string_view mode,
                   std::string_view cfg_out) {
    configuration cfg;
    cfg.init(cfg_in, mode);
    serial::serializer ser;
    ser.write(cfg);

    file out{cfg_out, "w+"};
    out.write(ser.buf);

    // serial::to_file(cfg, cfg_out);
    print(2, "configure {} written to {}\n", cfg_in, cfg_out);
    return 0;
}

struct configure_task : task
{
    subproject* proj;
    configure_task(subproject* proj)
        : proj{proj}  //
    {
        append(prods, proj->cfg_file);
        if (path::exists(proj->project_dir / "chob.sh"))
            append(deps, proj->project_dir / "chob.sh");
        for (auto&& child : proj->children) append(deps, child->cfg_file);

        append(flags, proj->main->mode, resource->compiler_flags);
        priority = pow10(18);

        append(argv,
               resource->progname,
               "--configure",
               ::format("{}", max_verbosity),
               proj->project_dir / "chob.sh",
               proj->main->mode,
               prods[0]);

        print(2, "configure_task created\n{}", str());
    }

    str desc() { return ::format("\033[33mconfig\033[m  {}", proj->rel_path); }

    void post() { proj->init(); }
};

struct bin_task : task
{
    subproject* proj;
    str bin;
    bool is_test;

    bin_task(subproject* proj, str_vec const& objs)
        : proj{proj}  //
    {
        str_view abs_bin = path::without_extension(objs[0]);
        bin = path::filename(abs_bin);

        str_view rel_obj = path::relative_to(objs[0], proj->output_dir);
        is_test =
            path::extension(bin) == ".test" || rel_obj.starts_with("tests/");

        // str dir{path::parent(objs[0])};
        // bin = path::without_extension(path::filename(objs[0]));
        str_vec lflags = proj->cfg.var.get("lflags", proj->ctx + bin);

        append(prods, abs_bin);
        append(deps, objs);
        append(flags, resource->compiler_flags, lflags);
        priority = proj->main->db.get_priority(prods[0]);
        if (priority == 0) compute_priority();

        append(argv, resource->compiler_flags, objs, "-o", prods[0], lflags);
        print(2, "new bin task: {} in {}\n", bin, proj->rel_path);
    }

    str desc() { return ::format("\033[36mlink\033[m    {}", bin); }

    void post() {
        if (did_run) {
            compute_priority();
            proj->main->db.set_priority(prods[0], priority);
        }

        if (is_test) proj->main->add_test(prods[0]);
        else if (proj->parent == 0) proj->main->add_bin(prods[0]);
    }
    //
    void compute_priority() { priority = proj->depth() * pow10(15) + us_time; }
};

struct compile_task : task
{
    subproject* proj;
    str main_obj;
    str srcdep;

    compile_task(subproject* proj, str source)
        : proj{proj}  //
    {
        auto main_src = proj->rel_to_main(source);
        main_obj = path::replace_extension(main_src, ".o");

        auto proj_src = proj->rel_to_proj(source);
        str proj_obj = path::replace_extension(proj_src, ".o");

        auto target = proj->main->output_dir() / main_obj;
        srcdep = target + ".d";

        prods.emplace_back(target);
        deps.emplace_back(source);
        proj->main->db.get_srcdeps(deps);
        append(flags,
               proj->common_cflags,
               proj->cfg.var.get("cflags", proj->ctx + proj_obj));
        // clang-format off
        append(argv,
               flags, "-fdiagnostics-color=always", "-MMD",
               "-MF", srcdep, "-c", "-o", prods[0], deps[0]);
        // clang-format on
        priority = proj->main->db.get_priority(prods[0]);
        if (priority == 0) compute_priority();

        print(2, "{}", str());
    }
    void post() {
        PROFILE_FUNCTION;
        platform::symbols syms;

        if (did_run) {
            depfile df{srcdep};
            deps = df.load();
            proj->main->db.set_srcdeps(deps);
            compute_priority();
            proj->main->db.set_priority(prods[0], priority);
            syms = proj->cfg.pfm.read_symbols(prods[0]);
            proj->main->db.set_symbols(prods[0], syms);
        } else {
            syms = proj->main->db.get_symbols(prods[0]);
        }

        for (str_vec const& objs : proj->ld.insert(prods[0], syms)) {
            print(2, "(obj) detected main: {}\n", objs);
            auto tsk = proj->main->build.emplace<bin_task>(proj, objs);
            proj->main->build.schedule(tsk);
        }
    }

    str desc() { return ::format("\033[32mcompile\033[m {}", main_obj); }

    void compute_priority() { priority = proj->depth() * pow10(12) + us_time; }
};

str unique_name(std::string_view pathname) {
    if (!path::exists(pathname)) return str{pathname};

    int num = 0;
    auto stem = path::without_extension(pathname);
    auto ext = path::extension(pathname);

    str unique;
    do {
        ++num;
        unique = ::format("{}.{}{}", stem, num, ext);
    } while (path::exists(unique));

    return unique;
}

int main_static_link(str prod, str_vec const& objs) try
{
    tempdir dir{"./static_link_XXXXXX"};

    for (auto&& obj : objs) {
        if (obj.ends_with(".a")) {
            auto name = path::filename(obj);
            str obj_dir = unique_name(dir.path() / name);

            path::create_dir(obj_dir);
            auto _ = path::push_dir(obj_dir);
            subprocess{}.argv("ar", "-x", obj).run();
        } else {
            auto name = path::filename(obj);
            str linkname = unique_name(dir.path() / name);
            path::create_symlink(obj, linkname);
        }
    }

    str_vec objs2;

    for (auto&& file : path::files(dir.path(), path::recursive))
        if (path::extension(file) == ".o")  //
            objs2.emplace_back(file);

    subprocess{}.argv("ar", "-crs", prod, objs2).run();

    return 0;
} catch (std::exception& ex) {
    fmt::print(stderr, "error: {}: {}\n", prod, ex.what());
    return 2;
} catch (...) {
    fmt::print(stderr, "error: {}: unknown error\n", prod);
    return 2;
}

struct staticlib_task : task
{
    subproject* proj;
    str lib;

    staticlib_task(subproject* proj, str_vec const& objs)
        : proj{proj}  //
    {
        str_vec varlib = proj->cfg.var.get("libname");
        if (varlib.size() < 1) throw error("invalid lib name {}", varlib);
        lib = "lib" + varlib[0] + proj->cfg.pfm.staticlib_ext;
        str_vec lflags = proj->cfg.var.get("lflags", proj->ctx + lib);

        append(prods, proj->output_dir / "lib" / lib);
        deps = objs;
        if (proj->parent != 0) append(deps, proj->parent->cfg_file);
        append(argv,
               resource->progname,
               "--static-link",
               ::format("{}", max_verbosity),
               prods[0],
               objs);
        // append(argv, "ar", "-crs", prods[0], objs);

        priority = proj->main->db.get_priority(prods[0]);
        if (priority == 0) compute_priority();

        print(2, "{}", str());
    }

    str desc() { return ::format("\033[36mlink\033[m    {}", lib); }

    void post() {
        if (proj->parent != 0) {
            for (str_vec const& objs : proj->parent->ld.insert(prods[0])) {
                print(2, "(lib) detected main: {}\n", objs);
                auto tsk =
                    proj->main->build.emplace<bin_task>(proj->parent, objs);
                proj->main->build.schedule(tsk);
            }
        }

        if (proj->parent == 0) {
            proj->main->add_lib(prods[0]);
        }
    }
    void compute_priority() { priority = proj->depth() * pow10(15) + us_time; }
};

struct sharedlib_task : task
{
    subproject* proj;
    str lib;

    sharedlib_task(subproject* proj, str_vec const& objs)
        : proj{proj}  //
    {
        str_vec varlib = proj->cfg.var.get("libname");
        if (varlib.size() < 1) throw error("invalid lib name {}", varlib);
        lib = "lib" + varlib[0] + proj->cfg.pfm.sharedlib_ext;
        str_vec lflags = proj->cfg.var.get("lflags", proj->ctx + lib);

        append(prods, proj->output_dir / "lib" / lib);
        deps = objs;
        if (proj->parent != 0) append(deps, proj->parent->cfg_file);

        priority = proj->main->db.get_priority(prods[0]);
        if (priority == 0) compute_priority();

        append(argv,
               resource->compiler_flags,
               "-shared",
               "-fdiagnostics-color=always",
               deps,
               lflags,
               "-o",
               prods[0]);

        print(2, "{}", str());
    }

    str desc() { return ::format("\033[36mlink\033[m    {}", lib); }

    void post() {
        if (proj->parent == 0) proj->main->add_lib(prods[0]);
    }
    void compute_priority() { priority = proj->depth() * pow10(15) + us_time; }
};

main_project::main_project(std::string_view root,
                           std::string_view mode,
                           std::string_view build_dir,
                           size_t jobs,
                           bool do_tests)
    : root{root}
    , mode{mode}
    , build_dir{build_dir}
    , jobs{jobs}
    , do_tests{do_tests}
    , db{output_dir() / "_db"}
    , sys{&db}
    , build{&sys, output_dir(), jobs}
    , cmd{build_dir}  //
{
    // clang-format off
    print(2,
          "main_project:\n"
          "  root         {}\n"
          "  mode         {}\n"
          "  build in     {}\n"
          "  database     {}\n"
          "  build output {}\n",
          root, mode, build_dir, db.filename(), output_dir());
    // clang-format on

    subproject* sub = create_subproject(root);
    projects.emplace_back(sub);

    for (auto&& p : projects) {
        // clang-format off
        print(2,
              "subproject {}:\n"
              "  parent        {}\n"
              "  project_dir   {}\n"
              "  output_dir    {}\n"
              "  cfg_file      {}\n"
              "  common cflags {}\n"
              "  export cflags {}\n",
              p->rel_path, p->parent ? p->parent->rel_path : "(null)", p->project_dir,
              p->output_dir, p->cfg_file, p->common_cflags, p->export_cflags);
        // clang-format on
    }
}

void main_project::schedule() {
    for (auto&& proj : projects) {
        proj->schedule();
    }
}

void main_project::handle_pending() {
    for (auto&& proj : projects) proj->schedule_pending();
    build.wait();
}

void main_project::create_ctest() {
    if (tests.empty()) return;

    file ctest{build_dir / "CTestTestfile.cmake", "w"};
    fmt_defer print{ctest.cfile()};
    for (auto&& test : tests) {
        print("add_test({} {})\n", path::filename(test), test);
    }
}

static void symlink_to_build(std::string_view build_dir,
                             std::string_view prod) {
    path::create_symlink(prod, build_dir, path::relative | path::overwrite);
}

void main_project::add_bin(std::string_view prod) {
    symlink_to_build(build_dir, prod);

    bins.emplace_back(prod);
}

void main_project::add_test(std::string_view prod) {
    symlink_to_build(build_dir, prod);
    tests.emplace_back(prod);
}

void main_project::add_lib(std::string_view prod) {
    symlink_to_build(build_dir, prod);
    libs.emplace_back(prod);
}

subproject* main_project::create_subproject(std::string_view path) {
    vector<subproject*> children;
    str ext = path / "external";
    if (path::exists(ext)) {
        for (auto&& dir : path::directories(ext)) {
            if (!path::exists(dir / "chob.sh")) continue;

            print(1, "subproject {}\n", path::relative_to(dir, root));
            subproject* sub = create_subproject(dir);
            projects.emplace_back(sub);
            children.push_back(sub);
        }
    }
    return new subproject{this, 0, path, children};
}

subproject::subproject(main_project* main,
                       subproject* parent,
                       std::string_view project_dir,
                       vector<subproject*> const& children)
    : main{main}
    , parent{parent}
    , children{children}
    , project_dir{project_dir}
    , rel_path{path::relative_to(project_dir, main->root)}
    , output_dir{main->output_dir() / rel_path}
    , cfg_file{output_dir / "_config"}
    , ctx{context::mk(main->mode)}  //
{
    for (auto&& child : children) child->parent = this;

    path::create_dirs(output_dir);
    output_dir = path::realpath(output_dir);

    if (path::exists(project_dir / "include"))
        append(export_cflags, "-I" + project_dir / "include");
    for (auto&& child : children) append(export_cflags, child->export_cflags);

    append(common_cflags, resource->compiler_flags, export_cflags);
    if (path::exists(project_dir / "src"))
        append(common_cflags, "-I"sv + project_dir / "src");
    else  //
        append(common_cflags, "-I"sv + str(project_dir));

    cfg_task = main->build.emplace<configure_task>(this);
}

// static std::string_view fs_extension(std::string_view path) {
//     size_t pos = path.find_last_of('.');
//     pos = pos == path.npos ? path.size() : pos;
//     return path.substr(pos);
// }
//

void subproject::maybe_insert(str_vec& src, std::string_view path, bool force) {
    if (!main->extensions.contains(path::extension(path))) return;

    bool is_test = path::extension(path::without_extension(path)) == ".test";

    if (force || !is_test || main->do_tests) src.emplace_back(path);
}

str_vec subproject::sources() {
    str_vec src;

    if (path::exists(project_dir / "src"))
        for (auto&& file : path::files(project_dir / "src", path::recursive))
            maybe_insert(src, file, false);
    else
        for (auto&& file : path::files(project_dir))
            maybe_insert(src, file, false);

    if (main->do_tests && path::exists(project_dir / "tests"))
        for (auto&& file : path::files(project_dir / "tests", path::recursive))
            maybe_insert(src, file, true);

    return src;
}

void subproject::schedule() { main->build.schedule(cfg_task); }

bool subproject::schedule_pending() {
    bool has_pending = false;
    for (auto&& objs : ld.pending()) {
        has_pending = true;
        auto tsk = main->build.emplace<bin_task>(this, objs);
        print(0,
              "\033[35mwarning\033[m: did not find all symbols for {}\n",
              tsk->bin);
        main->build.schedule(tsk);
    }

    return has_pending;
}

static bool find1(str_vec vec, std::string_view item) {
    for (size_t i = 1; i < vec.size(); ++i)
        if (vec[i] == item) return true;
    return false;
}

void subproject::lib_init() {
    auto cfg_lib = cfg.var.get("libname");
    bool is_lib = !cfg_lib.empty() || parent != 0;

    libname = path::filename(rel_path);
    if (cfg_lib.size() >= 1) libname = cfg_lib[0];

    bool cfg_static = find1(cfg_lib, "static");
    bool cfg_shared = find1(cfg_lib, "shared");

    is_static_lib = parent != 0 || (is_lib && cfg_static);
    is_shared_lib = parent == 0 && (is_lib && cfg_shared);

    if (is_lib && !(is_shared_lib || is_static_lib)) is_shared_lib = true;

    if (is_shared_lib || parent != 0) {
        print(2, "adding -fPIC to {}\n", rel_path);
        cfg.var.load("var cflags:+=:-fPIC");
    }

    if (is_lib) {
        print(2,
              "lib{}{} '{}'\n",
              is_shared_lib ? " [shared]" : "",
              is_static_lib ? " [static]" : "",
              libname);
    }

    return;
}

static bool is_test(std::string_view project_dir, std::string_view path) {
    if (path::is_prefix(project_dir / "tests", path)) return true;
    return path::extension(path::without_extension(path)) == ".test";
}

void subproject::init() {
    {
        mapped_file mf(cfg_file, "r");
        serial::deserializer deser{mf.view()};
        deser.read(cfg);
    }
    // serial::from_file(cfg, cfg_file);
    ld.init(&cfg.pfm);
    lib_init();

    vector<task*> tasks;
    str_vec lib_obs;

    for (auto&& src : sources()) {
        auto tsk = main->build.emplace<compile_task>(this, src);
        tasks.push_back(tsk);
        main->cmd.insert(tsk->argv, tsk->deps[0], tsk->prods[0]);

        // main->cmd.insert(fmt::format("{}", fmt::join(tsk->argv, " ")),
        //                  tsk->deps[0],
        //                  tsk->prods[0]);

        if (!is_test(project_dir, src)) append(lib_obs, tsk->prods[0]);
    }

    std::sort(tasks.begin(),
              tasks.end(),
              [](auto a, auto b) { return a->priority > b->priority; });

    for (auto&& child : children) {
        assert(child->is_static_lib);
        append(lib_obs,
               child->output_dir / "lib" / "lib" + child->libname
                   + cfg.pfm.staticlib_ext);
    }

    if (is_static_lib) {
        auto tsk = main->build.emplace<staticlib_task>(this, lib_obs);
        tasks.push_back(tsk);
    }
    if (is_shared_lib) {
        auto tsk = main->build.emplace<sharedlib_task>(this, lib_obs);
        tasks.push_back(tsk);
    }

    for (task* tsk : tasks) main->build.schedule(tsk);

    print(2, "configure {} loaded\n", cfg_file);
}
