#include <string_view>

#include "installer.h"

#include <fmt/std.h>
#include <path.h>

#include "common/fmt.h"
#include "str/algo.h"

namespace
{
void copy_file(std::string_view from, std::string_view to) {
    print(0, "copy {} to {}\n", from, to);
    path::create_parent(to);
    path::copy(from, to, path::overwrite);
}
void symlink(std::string_view target, std::string_view linkname) {
    print(0, "symlink {} to {}\n", linkname, target);
    path::create_parent(linkname);
    path::create_symlink(target, linkname, path::overwrite);
}

void copy_dir(std::string_view from, std::string_view to) {
    print(0, "copy {} to {}\n", from, to);
    path::create_parent(to);
    path::copy(from, to, path::overwrite | path::recursive);
}

}  // namespace

void installer::install_data() {
    if (path::exists(data)) copy_dir(data, prefix / "share");
}
void installer::install_bins() {
    for (auto&& bin : main->bins)
        copy_file(bin, prefix / "bin" / path::filename(bin));
}
void installer::install_include() {
    if (path::exists(include)) copy_dir(include, prefix / "include");
}
void installer::install_libs() {
    auto cfg = &main->root_project()->cfg;
    str version = ::format("{}", fmt::join(cfg->var.get("version"), ""));

    str dir = prefix / "lib";
    for (auto&& lib : main->libs) {
        auto libname = path::filename(lib);
        if (path::extension(libname) == cfg->pfm.sharedlib_ext) {
            auto names = cfg->pfm.dyn_names(libname, version);

            copy_file(lib, dir / names.realname);
            if (names.soname != names.realname)
                symlink(names.realname, dir / names.soname);
            if (names.soname != names.realname)
                symlink(names.realname, dir / names.namelink);
        } else {
            copy_file(lib, dir / libname);
        }
    }
}
void installer::install_pkgconfig() {
    auto cfg = &main->root_project()->cfg;
    auto name = "lib" + cfg->var.get("libname")[0];
    auto desc = fmt::format("{}", fmt::join(cfg->var.get("description"), " "));
    auto version = fmt::format("{}", fmt::join(cfg->var.get("version"), " "));

    auto path = prefix / "lib/pkgconfig" / name + ".pc";
    print(0, "installing {}\n", path);
    path::create_parent(path);
    file pc{path, "w"};
    fmt_defer print{pc.cfile()};

    print("prefix=${{pcfiledir}}/../..\nexec_prefix=${{prefix}}\n");
    print("libdir=${{exec_prefix}}/lib\nincludedir=${{prefix}}/include\n\n");
    print("Name: {}\nDescription: {}\nVersion: {}\n", name, desc, version);

    auto required = cfg->var.get("pkg-requires");
    ::print(2, "pkg-required = {}\n", required);
    if (!required.empty()) {
        print("Requires.private: {}\n", fmt::join(required, ", "));
    }

    print("Cflags: -I${{includedir}}\n");
    print("Libs: -L${{libdir}}");
    str_set libs;
    for (auto&& lib : main->libs) {
        auto libname = path::without_extension(path::filename(lib));
        if (libname.starts_with("lib")) libname.remove_prefix(3);
        libs.emplace(libname);
    }
    for (auto lib : libs) {
        print(" -l{}", lib);
    }
    print("\n");
}
