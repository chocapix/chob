#pragma once

#include <cstdint>
#include <string>
#include <vector>

#include <fmt/ranges.h>
#include <path.h>
#include <str/str.h>
#include <util/types.h>

#include "compile_commands.h"
#include "configuration.h"
#include "platform/linkdeps.h"
#include "rebuilder/database.h"
#include "rebuilder/rebuilder.h"
#include "common/print.h"

struct subproject;
struct main_project : noncopyable
{
    str root;
    str mode;
    str build_dir;
    size_t jobs;
    bool do_tests;

    database db;
    struct system sys;
    rebuilder build;
    compile_commands cmd;

    str_set extensions{".cpp", ".CPP", ".cc", ".C", ".cxx"};

    vector<std::unique_ptr<subproject>> projects;

    str_vec bins;
    str_vec libs;
    str_vec tests;

    main_project(std::string_view root,
                 std::string_view mode,
                 std::string_view build_dir,
                 size_t jobs,
                 bool do_tests);

    subproject* root_project() const { return projects.back().get(); }

    str output_dir() const { return build_dir / "_chob" / mode; }

    void schedule();
    void wait() { build.wait(); }
    void handle_pending();
    void create_ctest();

    void add_bin(std::string_view prod);
    void add_test(std::string_view prod);
    void add_lib(std::string_view prod);

  private:
    subproject* create_subproject(std::string_view rel_path);
};

struct subproject : noncopyable
{
    main_project* main;
    subproject* parent;
    vector<subproject*> children;

    str project_dir;
    str rel_path;
    str output_dir;
    str cfg_file;
    context ctx;

    str_vec common_cflags;
    str_vec export_cflags;

    task* cfg_task;
    configuration cfg;
    link_deps ld;

    str libname;
    bool is_static_lib;
    bool is_shared_lib;

    subproject(main_project* main,
               subproject* parent,
               std::string_view rel_project,
               vector<subproject*> const& chidlren);

    std::int64_t depth() const {
        if (parent == 0) return 1;
        return 1 + parent->depth();
    }

    void init();

    void schedule();

    bool schedule_pending();

    str_view rel_to_main(std::string_view path) const {
        return path::relative_to(path, main->root);
    }

    str_view rel_to_proj (str_view path) const {
        return path::relative_to(path, project_dir);
    }

  private:
    void lib_init();
    str_vec sources();
    void maybe_insert(str_vec& src, std::string_view path, bool force);
};

int main_configure(std::string_view cfg_in,
                   std::string_view mode,
                   std::string_view cfg_out);

int main_static_link(str prod, str_vec const& objs);
