#include "configuration.h"

#include <serial.h>

int main() {
    {
        context ctx;
        serial::from_str<context>(serial::to_str(ctx));
    }
    {
        variable_def vdef;
        serial::from_str<variable_def>(serial::to_str(vdef));
    }
    {
        variables vars;
        serial::from_str<variables>(serial::to_str(vars));
    }
    {
        configuration cfg;
        serial::from_str<configuration>(serial::to_str(cfg));
    }
}
