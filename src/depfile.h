#pragma once

#include <str/str.h>
#include <str/vec.h>

struct depfile
{
    str pathname;

    depfile(std::string_view pathname)
        : pathname {pathname} {}

    str_vec parse(std::string_view);
    str_vec load();
};

