#pragma once

#include <str/map.h>
#include <str/str.h>
#include <util/types.h>

#include "platform/platform.h"

struct context
{
    using is_serial = int;
    str_vec vec;

    template<string_view_like SV>
    static context mk(SV const& sv) {
        context ctx;
        ctx += sv;
        return ctx;
    }

    // void init(std::initializer_list<std::string_view> list) {
    //     for (auto&& elt : list) vec.emplace_back(elt);
    // }

    void clear() { vec.clear(); }

    void operator+=(std::string_view str) { vec.emplace_back(str); }

    bool contains(std::string_view str) const {
        for (auto&& elt : vec)
            if (elt == str) return true;
        return false;
    }

    bool is_subset_of(context const& other) const {
        for (auto&& elt : vec)
            if (!other.contains(elt)) return false;
        return true;
    }
    friend context operator+(context a, std::string_view b) {
        a += b;
        return a;
    }
};

struct variable_def
{
    using is_serial = int;
    context ctx;
    str_vec values;

    void init(std::string_view text);

    bool matches(context const&) const;
};

struct variables
{
    using is_serial = int;
    str_map<vector<variable_def>> defs;

    void init(std::string_view filename);

    str_vec get(std::string_view name,
                context const& current_context = {}) const;

    void load(std::string_view text);
};

struct configuration
{
    using is_serial = int;
    variables var;
    platform pfm;

    void init(std::string_view filename, std::string_view mode);

};
