#pragma once

#include <str/vec.h>

struct resource
{
    void init(std::string_view argv0);
    str dir;
    str_vec compiler_flags;
    str progname;
};
extern resource* resource;
