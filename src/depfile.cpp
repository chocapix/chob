#include "depfile.h"

#include <str/algo.h>
#include <util/file.h>

#include "common/error.h"

static bool is_space(char c) { return c == ' ' || c == '\n' || c == '\t'; }
static void skip_spaces(std::string_view& text) {
    while (!text.empty()) {
        if (is_space(text[0])) text.remove_prefix(1);
        else if (text.starts_with("\\\n")) text.remove_prefix(2);
        else break;
    }
}

std::string_view parse_name(std::string_view& text) {
    std::string_view text0 = text;
    text = skip_to(text, " \t\n");
    return text0 - text;
}

str_vec depfile::parse(std::string_view text) {
    text = skip_to(text, ":");
    if (text.empty()) throw error("parsing {}", pathname);
    text.remove_prefix(1);

    str_vec deps;
    for (;;) {
        skip_spaces(text);
        if (text.empty()) break;
        deps.emplace_back(parse_name(text));
    }
    return deps;
}

str_vec depfile::load() {
    mapped_file df{pathname, "r+"};
    str_vec deps = parse(df.view());
    return deps;
}
