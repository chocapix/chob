#pragma once

#include <cstdint>
#include <memory>

#include <str/map.h>
#include <str/vec.h>
#include <sys/resource.h>
#include <util/file.h>

#include "rebuilder/database.h"

struct task
{
    using i64 = std::int64_t;
    enum status
    {
        NONE = 0,
        IGNORED,
        WAITING,
        READY,
        RUNNNIG,
        DONE,
        MAX_STATUS,
    };

    str_vec prods, deps, flags;
    str_vec argv;

    i64 priority = 0;
    status stat = NONE;
    size_t iwait = 0;
    bool did_run = false;
    i64 us_time = 0;

    virtual ~task() {}
    virtual str desc() = 0;
    virtual void post() {}

    str to_str() const;
};

struct task_result
{
    enum condition
    {
        NO_STATUS,
        EXITED,
        SIGNALED,
        STOPPED,
        UNKNOWN_STATUS
    };
    struct status
    {
        condition cond;
        int info;

        status() = default;

        bool success() const { return cond == EXITED && info == 0; }

        status(condition cond, int info)
            : cond{cond}
            , info{info} {}
        explicit status(int posix_status);
    };

    task* tsk;
    status stat;
    rusage ru;
    file output;
};

struct isystem
{
    virtual ~isystem() {}

    // --- file operations ---
    struct ihandle
    {
        virtual ~ihandle() {}
        virtual std::string_view view() = 0;
    };
    virtual std::unique_ptr<ihandle> open(std::string_view filename) = 0;
    virtual bool exists(str const& filename) = 0;

    // --- hash store ---
    virtual void set_hash(std::string_view key, std::string_view cache) = 0;
    virtual str get_hash(std::string_view key) = 0;

    // --- subprocesses ---
    struct interrupt
    {
    };
    virtual void start(task* tsk) = 0;
    virtual size_t running_count() = 0;
    virtual task_result wait() = 0;
};

struct system : isystem
{
    system(database* db);
    ~system();

    std::unique_ptr<ihandle> open(std::string_view filename);
    bool exists(str const& filename);

    void set_hash(std::string_view key, std::string_view hash);
    str get_hash(std::string_view key);

    void start(task* tsk);
    size_t running_count() { return results.size(); }
    task_result wait();

  private:
    struct exists_bool
    {
        bool value;
        explicit exists_bool(str const& filename);
    };
    database* db;
    str_map<exists_bool> exists_cache;
    map<pid_t, task_result> results;
};

struct rebuilder
{
    rebuilder(isystem* system, std::string_view output_dir, size_t jobs);
    ~rebuilder();

    template<typename Task, typename... Args>
    Task* emplace(Args&&... args) {
        task* tsk = insert(new Task(std::forward<Args>(args)...));
        return dynamic_cast<Task*>(tsk);
    }

    void schedule(task* tsk);

    struct task_failed
    {
    };
    void wait();

  private:
    task* insert(task* tsk);

    struct impl;
    std::unique_ptr<impl> pimpl;
};
