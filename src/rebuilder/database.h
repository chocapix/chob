#pragma once

#include <memory>

#include <str/vec.h>
#include <util/types.h>

#include "platform/platform.h"

struct database : noncopyable
{
    using i64 = std::int64_t;
    database(std::string_view filename);
    ~database();

    std::string_view filename() const;
    void dump();

    void get_srcdeps(str_vec& deps);
    void set_srcdeps(str_vec const& deps);

    str get_hash(std::string_view target);
    void set_hash(std::string_view target, std::string_view hash);

    i64 get_priority(std::string_view target);
    void set_priority(std::string_view target, i64 priority);

    void set_symbols(std::string_view filename, platform::symbols const& syms);
    platform::symbols get_symbols(std::string_view filename);

    static void full_reset(std::string_view filename);
    static void reset_main(std::string_view filename, std::string_view root);
    static void srcdeps_reset(std::string_view filename);

  private:
    struct impl;
    std::unique_ptr<impl> pimpl;
};
