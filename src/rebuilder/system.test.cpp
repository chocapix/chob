#include <memory>

#include "common/print.h"
#include "rebuilder/rebuilder.h"

using namespace std::string_view_literals;

struct shell_task : task
{
    shell_task(std::string_view cmd) {
        for (auto&& a : {"sh"sv, "-c"sv, cmd}) argv.emplace_back(a);
    }

    str desc() { return argv[2]; }
};

int main(int argc, char** argv) {
    database db{"_system_test/_db"};
    struct system sys
    {
        &db
    };

    vector<std::unique_ptr<shell_task>> tasks;

    for (int i = 1; i < argc; ++i) {
        tasks.emplace_back(new shell_task{argv[i]});
    }

    for (auto&& tsk : tasks) {
        sys.start(tsk.get());
    }

    try {
        while (sys.running_count() > 0) {
            task_result res = sys.wait();
        }
    } catch (isystem::interrupt) {
        print(0, "interrupted by user\n");
    }
}
