#include <cstdlib>
#include <cstring>
#include <string>
#include <string_view>
#include <thread>

#include <path.h>
#include <signal.h>
#include <str/vec.h>
#include <sys/signal.h>
#include <sys/stat.h>
#include <util/file.h>
#include <util/subprocess.h>
#include <util/sys/posix.h>
#include <util/timer.h>

#include "common/error.h"
#include "common/print.h"
#include "rebuilder/database.h"
#include "rebuilder/rebuilder.h"

using namespace std::chrono_literals;

// --- file operations ---

namespace
{
struct handle : isystem::ihandle
{
    mapped_file mf;
    handle(std::string_view filename)
        : mf{filename, "r"} {}
    std::string_view view() { return mf.view(); };
};
}  // namespace

std::unique_ptr<isystem::ihandle> system::open(std::string_view filename) {
    return std::unique_ptr<isystem::ihandle>{new handle{filename}};
}
inline profiler prof_path_exists{"path::exists"};
system::exists_bool::exists_bool(str const& filename) {
    auto _ = prof_path_exists();
    value = path::exists(filename);
}
bool system::exists(str const& filename) {
    auto [it, inserted] = exists_cache.try_emplace(filename, filename);
    return it->second.value;
}

// --- hash store ---
void system::set_hash(std::string_view key, std::string_view hash) {
    db->set_hash(key, hash);
}
str system::get_hash(std::string_view key) { return db->get_hash(key); }

// --- subprocesses ---

std::atomic_int interrupt_sig = 0;
namespace
{
void interrupt_handler(int sig) {
    if (interrupt_sig != 0) {
        print(0, "interrupted again, exiting.\n");
        std::exit(2);
    }

    print(2, "build interrupted, waiting for tasks to finish.\n");
    interrupt_sig = sig;
}
}  // namespace

system::system(database* db)
    : db{db}  //
{
    struct sigaction sa;
    sa.sa_handler = interrupt_handler;
    sigaction(SIGINT, &sa, 0);
}
system::~system() {
    struct sigaction sa;
    sa.sa_handler = SIG_DFL;
    sigaction(SIGINT, &sa, 0);
}

void system::start(task* tsk) {
    for (auto&& prod : tsk->prods) path::create_parent(prod);
    task_result res{tsk, task_result::status{}, rusage{}, file::temp()};
    // int ofd = res.output.fileno();
    pid_t pid = subprocess(tsk->argv).outerr(res.output.cfile()).detach();
    // pid_t pid = sys::fork();
    // if (pid == 0) {
    //     struct sigaction sa;
    //     sa.sa_handler = SIG_DFL;
    //     sigaction(SIGINT, &sa, 0);
    //
    //     vector<char*> argv;
    //     for (auto&& arg : tsk->argv) argv.push_back(arg.data());
    //
    //     // sys::close(0);
    //     sys::dup2(ofd, 1);
    //     sys::dup2(ofd, 2);
    //     sys::execvp(argv[0], argv.data());
    //     std::terminate();
    // }
    results.emplace(pid, std::move(res));

    print(2, "pid {} started {}\n", pid, tsk->desc());
}

task_result system::wait() {
    subprocess::status ps;
    do {
        ps = subprocess::wait4();
        // ps = sys::unguarded_wait_pid();
    } while (ps.pid < 0 && errno == EINTR);

    if (ps.pid < 0) throw error("waitpid: {}", strerror(errno));

    // pop result
    auto it = results.find(ps.pid);
    if (it == results.end()) {
        print(1, "warning: system::wait: no such pid {}\n", ps.pid);
        return wait();
    }
    task_result res = std::move(it->second);
    res.stat = task_result::status(ps.status);
    res.ru = ps.ru;
    results.erase(it);
    print(2, "pid {} finished {}\n", ps.pid, res.tsk->desc());
    return res;
}
