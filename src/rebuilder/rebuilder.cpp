#include <atomic>
#include <cstdint>

#include "rebuilder/rebuilder.h"

#include <fmt/ranges.h>
#include <path.h>
#include <sha/sha256.h>
#include <sys/wait.h>
#include <util/timer.h>

#include "common/error.h"
#include "common/fmt.h"
#include "common/print.h"

namespace
{
[[maybe_unused]] char const* name(task::status st) {
    switch (st) {
        case task::NONE: return "NONE";
        case task::IGNORED: return "IGNORED";
        case task::WAITING: return "WAITING";
        case task::READY: return "READY";
        case task::RUNNNIG: return "RUNNNIG";
        case task::DONE: return "DONE";
        case task::MAX_STATUS: return "MAX_STATUS";
    }
    return "?";
}

using i64 = std::int64_t;
i64 us_from_timeval(struct timeval tv) {
    return tv.tv_sec * i64(1'000'000) + tv.tv_usec;
}
}  // namespace

str task::to_str() const {
    fmt_buf format;
    format("task prods    {}\n", prods);
    format("     deps     {}\n", deps);
    format("     flags    {}\n", flags);
    format("     argv     {}\n", argv);
    format("     priority {}\n", priority);
    return format.get();
}

task_result::status::status(int s) {
    if (WIFEXITED(s)) {
        cond = EXITED;
        info = WEXITSTATUS(s);
    } else if (WIFSIGNALED(s)) {
        cond = SIGNALED;
        info = WTERMSIG(s);
    } else if (WIFSTOPPED(s)) {
        cond = STOPPED;
        info = WSTOPSIG(s);
    } else {
        cond = UNKNOWN_STATUS;
        info = s;
    }
}

inline profiler prof_hash_compute("hashes::compute");
inline profiler prof_hash_getfile("hashes::get(file)");
struct hashes
{
    hashes(isystem* sys)
        : sys{sys} {}

    str compute(task const* tsk) const {
        PROFILE_FUNCTION;
        // auto _ = prof_hash_compute();
        fmt_buf format;

        format("flags: {}\ndeps:\n", tsk->flags);
        for (auto&& dep : tsk->deps) format("  {} {}\n", get(dep), dep);

        str r = SHA256()(format.view());
        // print(0, "{:64} compute {}\n", r, tsk->prods);
        return r;
    }

    str get(task const* tsk) const {
        PROFILE_FUNCTION;
        std::string_view key{tsk->prods[0]};

        if (auto it = cache.find(key); it != cache.end()) {
            // print(0, "{:64} get cache {}\n", it->second, tsk->prods);
            return it->second;
        }

        str hash{sys->get_hash(key)};

        cache.emplace(key, hash);
        // print(0, "{:64} get db {}\n", hash, tsk->prods);
        return hash;
    }
    void update(task* tsk) { set(tsk, compute(tsk)); }

  private:
    str get(std::string_view key) const {
        if (auto it = cache.find(key); it != cache.end()) return it->second;

        // auto _ = prof_hash_getfile();
        auto file = sys->open(key);
        str hash = SHA256()(file->view());

        cache.emplace(key, hash);
        return hash;
    }

    void set(task const* tsk, std::string_view hash) {
        auto&& key = tsk->prods[0];

        cache.insert_or_assign(key, hash);
        sys->set_hash(key, hash);
    }

    isystem* sys;
    mutable str_map<str> cache;
};

struct task_queue
{
    struct less_priority
    {
        bool operator()(task* a, task* b) const {
            return a->priority < b->priority;
        }
    };

    size_t size() const { return data.size(); }

    void clear() { data.clear(); }

    void push(task* tsk) {
        data.push_back(tsk);
        std::push_heap(data.begin(), data.end(), less_priority{});
    }

    task* pop() {
        std::pop_heap(data.begin(), data.end(), less_priority{});
        task* tsk = data.back();
        data.pop_back();
        return tsk;
    }
    vector<task*> data;
};
extern std::atomic_int interrupt_sig;
struct interrupt
{
};

struct rebuilder::impl
{
    isystem* sys;
    str output_dir;
    size_t jobs;
    hashes hash;
    vector<std::unique_ptr<task>> tasks;
    str_map<task*> tasks_by_prod;
    str_map<vector<task*>> waiting;
    task_queue queue;
    size_t status_count[task::MAX_STATUS] = {
        0,
    };
    bool is_task_failed = false;

    impl(isystem* sys, std::string_view output_dir, size_t jobs)
        : sys{sys}
        , output_dir{output_dir}
        , jobs{jobs}
        , hash{sys} {}

    ~impl() {}

    task* insert(task* tsk) {
        tasks.emplace_back(tsk);
        init_status(tsk, task::IGNORED);

        for (auto&& prod : tsk->prods) {
            auto [it, inserted] = tasks_by_prod.emplace(prod, tsk);
            if (!inserted)
                throw error("duplicate production {}\n  in  {}\n  and {}\n",
                            prod,
                            tsk->desc(),
                            it->second->desc());
        }
        return tsk;
    }

    void schedule(task* tsk) {
        if (tsk->stat != task::IGNORED) return;
        set_status(tsk, task::WAITING);
        queue.push(tsk);
    }

    void wait() {
        try {
            while (sys->running_count() > 0 || queue.size() > 0) {
                consume_queue();
                if (sys->running_count() > 0) wait_one();
            }
        } catch (task_failed) {
            handlle_task_failed();
            throw;
        } catch (interrupt) {
            handle_interrupt();
        }
    }

    void handlle_task_failed() {
        is_task_failed = true;
        try {
            if (sys->running_count() > 0)
                print(0,
                      "job failure: waiting for {} other jobs to finish.\n",
                      sys->running_count());
            while (sys->running_count() > 0) wait_one();
        } catch (interrupt) {
            handle_interrupt();
        }
    }

    void handle_interrupt() {
        if (sys->running_count() > 0)
            print(0,
                  "interrupt: waiting for {} jobs to finish.\n",
                  sys->running_count());
        while (sys->running_count() > 0) sys->wait();
        std::exit(2);
    }

    void wait_one() {
        auto result = sys->wait();
        if (interrupt_sig != 0) {
            print(2, "wait_one: interrupted\n");
            queue.clear();
            throw interrupt{};
        }
        print_output(result);

        if (!result.stat.success()) {
            queue.clear();
            if (!is_task_failed) throw task_failed{};
            return;
        }

        i64 utime = us_from_timeval(result.ru.ru_utime);
        i64 stime = us_from_timeval(result.ru.ru_stime);
        result.tsk->us_time = utime + stime;
        print(2,
              "{}: user {} + sys {} = {}\n",
              result.tsk->desc(),
              utime,
              stime,
              result.tsk->us_time);

        result.tsk->did_run = true;
        result.tsk->post();
        hash.update(result.tsk);
        set_status(result.tsk, task::DONE);
        notify_done(result.tsk);
    }

    void print_output(task_result const& res) {
        if (res.output.cfile() == 0) return;

        auto map = res.output.map();
        auto content = map.view();

        str desc = max_verbosity == 0
            ? res.tsk->desc()
            : ::format("{}", fmt::join(res.tsk->argv, " "));

        if (res.stat.success()) {
            if (!content.empty()) print(0, "{} said:\n{}", desc, content);
        } else if (!content.empty()) {
            print(0, "{} \033[31mFAILED\033[m:\n{}", desc, content);
        } else {
            print(0, "{} \033[31mFAILED\033[m\n", desc);
        }
    }

    void consume_queue() {
        while (queue.size() > 0 && sys->running_count() < jobs)
            try_rebuild(queue.pop());
    }

    void try_rebuild(task* tsk) {
        PROFILE_FUNCTION;
        for (; tsk->iwait < tsk->deps.size(); tsk->iwait++) {
            auto&& depname = tsk->deps[tsk->iwait];
            task* dep = task_by_prod(depname);

            if (dep != 0) {
                if (dep->stat == task::DONE) continue;
                return notify_waiting(tsk, depname);
            }

            if (path::is_prefix(output_dir, depname)) {
                return notify_waiting(tsk, depname);
            }

            if (!sys->exists(depname))
                throw error(
                    "no way to build {} requested by {}", depname, tsk->prods);
        }
        set_status(tsk, task::READY);
        rebuild(tsk);
    }

    void rebuild(task* tsk) {
        PROFILE_FUNCTION;
        if (hash.compute(tsk) != hash.get(tsk)) {
            if (max_verbosity == 0) print(0, "{}\n", tsk->desc());
            else print(1, "{}\n", fmt::join(tsk->argv, " "));
            set_status(tsk, task::RUNNNIG);
            sys->start(tsk);
            // tsk->did_run = true;
        } else {
            tsk->post();
            set_status(tsk, task::DONE);
            notify_done(tsk);
        }
    }

    void notify_waiting(task* tsk, str const& depname) {
        waiting[depname].push_back(tsk);
    }
    void notify_done(task* tsk) {
        PROFILE_FUNCTION;
        if (is_task_failed) return;
        for (auto&& prod : tsk->prods) {
            auto it = waiting.find(prod);
            if (it == waiting.end()) continue;

            for (task* waiting_task : it->second) {
                try_rebuild(waiting_task);
            }

            waiting.erase(it);
        }
    }

    void init_status(task* tsk, task::status st) {
        tsk->stat = st;
        status_count[tsk->stat]++;
    }
    void set_status(task* tsk, task::status st) {
        status_count[tsk->stat]--;
        tsk->stat = st;
        status_count[tsk->stat]++;
    }

    task* task_by_prod(std::string_view prod) {
        auto it = tasks_by_prod.find(prod);
        return it == tasks_by_prod.end() ? 0 : it->second;
    }
};

rebuilder::rebuilder(isystem* system, std::string_view output_dir, size_t jobs)
    : pimpl{new impl{system, output_dir, jobs}} {}

rebuilder::~rebuilder() {}

void rebuilder::schedule(task* tsk) { pimpl->schedule(tsk); }

inline profiler prof_rb_wait{"rebuilder::wait"};
void rebuilder::wait() {
    auto _ = prof_rb_wait();
    pimpl->wait();
    print(1, "after wait, waiting size {}\n", pimpl->waiting.size());
}

task* rebuilder::insert(task* tsk) { return pimpl->insert(tsk); }
