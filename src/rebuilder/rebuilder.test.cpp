#include <cassert>
#include <functional>
#include <memory>
#include <sstream>
#include <string_view>
#include <thread>
#include <utility>

#include "rebuilder.h"

#include <fmt/ranges.h>
#include <sha/sha256.h>
#include <str/algo.h>
#include <testing.h>

#include "common/error.h"
#include "common/fmt.h"
#include "common/print.h"

using namespace std::string_view_literals;
using namespace std::string_literals;

struct test_task;

size_t content_counter = 1;

struct test_system : isystem
{
    struct handle : ihandle
    {
        std::string_view str;

        handle(std::string_view str)
            : str{str} {}

        std::string_view view() { return str; }
    };

    template<typename Range>
    void touch_range(Range&& range) {
        for (auto&& filename : range) touch(filename);
    }

    void touch(str const& filename) {
        write(filename, ::format("{}\n", content_counter++));
    }
    void write(str const& filename, std::string_view content) {
        file_contents[filename] = content;
    }

    std::unique_ptr<ihandle> open(std::string_view filename) {
        auto it = file_contents.find(filename);
        if (it == file_contents.end())
            throw error("file not found: {}", filename);
        std::string_view str = it->second;
        return std::unique_ptr<ihandle>{new handle{str}};
    }

    bool exists(str const& filename) {
        return file_contents.contains(filename);
    }

    void set_hash(std::string_view key, std::string_view hash) {
        hashes[str{key}] = hash;
    }
    str get_hash(std::string_view key) {
        auto it = hashes.find(key);
        return it == hashes.end() ? str{} : it->second;
    }

    void start(task* tsk);
    size_t running_count() { return running.size(); }
    task_result wait();

    vector<test_task*> running;

    str_map<str> file_contents;
    str_map<str> hashes;
};

size_t run_counter = 1;

struct test_task : task
{
    test_system* sys;
    size_t rank = 0;

    test_task(test_system* sys, std::string_view rule)
        : sys{sys}  //
    {
        str_vec v = vsplit(rule, ':');
        assert(v.size() >= 1);
        prods = vsplit(v[0], ' ');
        if (v.size() >= 2) deps = vsplit(v[1], ' ');
        if (v.size() >= 3) flags = vsplit(v[2], ' ');
    }
    bool did_run() const { return rank != 0; }
    void run() {
        rank = run_counter++;

        fmt::print("run {}\n", desc());
        fmt_buf format;
        format("prods {}, flags {}, deps:\n", prods, flags);
        for (auto&& dep : deps) {
            format("  {} {}\n", SHA256()(dep), dep);
        }

        for (auto&& prod : prods) sys->write(prod, format.view());
    }
    str desc() { return ::format("test{}", prods); }
};

struct test_run
{
    test_system* sys;
    rebuilder build;
    vector<test_task*> tasks;
    str_map<test_task*> tasks_by_prod;

    template<typename Range>
    test_run(test_system* sys, Range&& rules)
        : sys{sys}
        , build{sys, "o", 1} {
        add_rules(rules);
        run();
    }

    template<typename Range>
    void add_rules(Range&& range) {
        for (auto&& rule : range) add_rule(rule);
    }

    void run() {
        for (auto tsk : tasks) build.schedule(tsk);
        build.wait();
        require_dep_order();
    }

    void add_rule(std::string_view rule) {
        auto tsk = build.emplace<test_task>(sys, rule);
        tasks.push_back(tsk);
        for (auto&& prod : tsk->prods) {
            auto [_, inserted] = tasks_by_prod.emplace(prod, tsk);
            assert(inserted), (void)inserted;
        }
    }

    void require_dep_order() {
        for (auto tsk : tasks) {
            if (!tsk->did_run()) continue;

            for (auto&& depname : tsk->deps) {
                auto dep = task_by_prod(depname);
                if (dep != 0) REQUIRE(dep->rank < tsk->rank);
            }
        }
    }

    size_t run_count() const {
        size_t count = 0;
        for (auto&& tsk : tasks) count += tsk->did_run();
        return count;
    }

    str_set run_set() const {
        str_set set;
        for (auto&& tsk : tasks)
            if (tsk->did_run()) set.emplace(tsk->prods[0]);

        fmt::print("run_set: {}\n", set);
        return set;
    }

    test_task* task_by_prod(std::string_view prod) const {
        auto it = tasks_by_prod.find(prod);
        return it == tasks_by_prod.end() ? 0 : it->second;
    }
};

void test_system::start(task* tsk) {
    auto test_tsk = dynamic_cast<test_task*>(tsk);
    assert(test_tsk != 0);
    running.push_back(test_tsk);
}
task_result test_system::wait() {
    test_task* tsk = running.back();
    running.pop_back();

    tsk->run();
    task_result res;
    res.tsk = tsk;
    res.stat.info = 0;
    res.stat.cond = task_result::EXITED;

    return res;
}

template<typename Rules, typename Inputs>
void test_all_none(test_system* sys, Rules&& rules, Inputs&& inputs) {
    sys->touch_range(inputs);
    {
        test_run tr{sys, rules};
        REQUIRE(tr.run_count() == tr.tasks.size());
    }
    require_none(sys, std::forward<Rules>(rules));
}

template<typename Rules>
void require_none(test_system* sys, Rules&& rules) {
    {
        test_run tr{sys, rules};
        REQUIRE(tr.run_count() == 0);
    }
}

TEST_CASE("simple") {
    auto rules = {"o/pouet: a"};
    auto inputs = {"a"};
    test_system sys;
    test_all_none(&sys, rules, inputs);
}

TEST_CASE("simple 2") {
    auto inputs = {"a", "b"};
    auto rules = {"o/pouet: a", "o/prout: b"};
    test_system sys;
    test_all_none(&sys, rules, inputs);

    sys.touch("a");
    {
        test_run tr{&sys, rules};
        REQUIRE(tr.run_set() == str_set{"o/pouet"});
    }
    require_none(&sys, rules);
}

template<typename... Args>
str_set mk_set(Args&&... args) {
    str_set set;
    auto elements = {args...};
    for (auto&& elem : elements) set.emplace(elem);
    return set;
}

TEST_CASE("simple 3") {
    auto inputs = {"a", "b"};
    auto rules = {"o/1: a", "o/2: o/1 b"};
    test_system sys;
    test_all_none(&sys, rules, inputs);

    sys.touch("a");
    {
        test_run tr{&sys, rules};
        REQUIRE(tr.run_set() == mk_set("o/1", "o/2"));
    }
    require_none(&sys, rules);
}

int main(int argc, char** argv) { return testing::main(argc, argv); }
