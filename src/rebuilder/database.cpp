#include <cstring>
#include <string_view>

#include "rebuilder/database.h"

#include <fmt/ranges.h>
#include <path.h>
#include <pmap.h>
#include <serial.h>
#include <util/timer.h>
#include <util/types.h>
#include <util/wyhash++.h>

#include "common/fmt.h"
#include "common/print.h"
#include "fmt/core.h"
#include "platform/platform.h"
#include "str/map.h"

inline profiler db_open{"db open"};
inline profiler db_close{"db close"};

struct database::impl
{
    str filename;
    pmap srcdeps;
    pmap hashes;
    pmap priorities;
    pmap symbols;

    impl(std::string_view filename)
        : filename{ensure_directories(filename)}
        , srcdeps(this->filename + ".srcdeps")
        , hashes(this->filename + ".hashes")
        , priorities(this->filename + ".priorities")  //
        , symbols(this->filename + ".symbols") {}

    static std::string_view ensure_directories(std::string_view file) {
        path::create_parent(file);
        return file;
    }
};

database::database(std::string_view filename)
    : pimpl{new impl{filename}}  //
{
    if (max_verbosity >= 3) {
        print(3, "database loaded {}:\n", filename);
        dump();
        print(3, "database loaded {}: END\n", filename);
    }
}

database::~database() {
    if (max_verbosity >= 3) {
        print(3, "database stored {}:\n", pimpl->filename);
        dump();
        print(3, "database stored {}: END\n", pimpl->filename);
    }
}

std::string_view database::filename() const { return pimpl->filename; }

inline profiler prof_get{"db get"};
inline profiler prof_set{"db set"};
void database::get_srcdeps(str_vec& deps) {
    auto _ = prof_get();

    std::string_view dep_str = pimpl->srcdeps.get(deps[0]);
    if (!dep_str.empty()) {
        serial::deserializer in{dep_str};
        in.read(deps);
    }
}
void database::set_srcdeps(str_vec const& deps) {
    auto _ = prof_set();
    serial::serializer out;
    out.write(deps);

    pimpl->srcdeps.put(deps[0], out.buf);
}

str database::get_hash(std::string_view target) {
    auto _ = prof_get();
    return str{pimpl->hashes.get(target)};
}
void database::set_hash(std::string_view target, std::string_view hash) {
    auto _ = prof_set();
    pimpl->hashes.put(target, hash);
}

database::i64 database::get_priority(std::string_view target) {
    auto _ = prof_get();
    i64 priority = 0;
    auto str = pimpl->priorities.get(target);
    if (!str.empty()) {
        serial::deserializer in{str};
        in.read(priority);
    }
    return priority;
}
void database::set_priority(std::string_view target, i64 priority) {
    auto _ = prof_set();
    serial::serializer out;
    out.write(priority);
    pimpl->priorities.put(target, out.buf);
}

void database::set_symbols(std::string_view filename,
                           platform::symbols const& syms) {
    auto _ = prof_set();
    serial::serializer out;
    out.write(syms);
    pimpl->symbols.put(filename, out.buf);
}
platform::symbols database::get_symbols(std::string_view filename) {
    auto _ = prof_get();
    platform::symbols syms;
    auto str = pimpl->symbols.get(filename);
    if (!str.empty()) {
        serial::deserializer in{str};
        in.read(syms);
    }
    return syms;
}

void database::full_reset(std::string_view filename) {
    str_map<str> configs;
    {
        auto hashes = pmap(filename + ".hashes");
        for (auto&& [key, hash] : hashes) {
            if (key.ends_with("_config")) configs.emplace(key, hash);
        }
    }

    for (auto&& table : {".srcdeps", ".hashes"}) {
        auto tablename = filename + table;
        if (path::exists(tablename)) path::remove(tablename);
    }

    auto hashes = pmap(filename + ".hashes");
    for (auto&& [key, hash] : configs) {
        hashes.put(key, hash);
    }
}

void database::reset_main(std::string_view filename, std::string_view root) {
    {
        auto deps = pmap(filename + ".srcdeps");
        str_vec to_remove;
        for (auto&& [key, _] : deps) {
            for (auto&& subdir : {"src", "tests"}) {
                if (key.starts_with(root / subdir)) {
                    to_remove.emplace_back(key);
                    // deps.put(key, ""sv);
                }
            }
        }
        for (auto&& key : to_remove) {
            print(1, "forgetting deps {}\n", key);
            deps.put(key, ""sv);
        }
    }
    {
        auto base = path::parent(filename);
        auto hashes = pmap(filename + ".hashes");
        str_vec to_remove;
        for (auto&& [key, _] : hashes) {
            for (auto&& subdir : {"src", "tests"}) {
                if (key.starts_with(base / subdir)) {
                    to_remove.emplace_back(key);
                    // deps.put(key, ""sv);
                }
            }
        }
        for (auto&& key : to_remove) {
            print(1, "forgetting hash {}\n", key);
            hashes.put(key, ""sv);
        }
    }
}

void database::srcdeps_reset(std::string_view filename) {
    auto tablename = filename + ".srcdeps";
    if (path::exists(tablename)) path::remove(tablename);
}

void database::dump() {
    for (auto [src, depstext] : pimpl->srcdeps) {
        str_vec deps;
        if (!depstext.empty()) {
            serial::deserializer in{depstext};
            in.read(deps);
        }
        fmt::println("{}: {}", src, fmt::join(deps, " "));
    }

    for (auto [target, hash] : pimpl->hashes)
        fmt::println("{} {}", hash, target);

    for (auto [obj, symtext] : pimpl->symbols) {
        platform::symbols syms;
        serial::deserializer in{symtext};
        in.read(syms);
        fmt::println("{}: def {} undef {}", obj, syms.defined, syms.undefined);
    }

    for (auto [target, prioritytext] : pimpl->priorities) {
        i64 priority;
        serial::deserializer in{prioritytext};
        in.read(priority);
        fmt::println("{}: priority {}", target, priority);
    }
}
