#pragma once

#include "common/fmt.h"

extern size_t max_verbosity;
extern bool debug_print;

void vprint(std::string_view text);

template<typename... T>
void print(unsigned verbosity, fmt::format_string<T...> str, T&&... args) {
    if (verbosity <= max_verbosity)
        vprint(fmt::format(str, std::forward<T>(args)...));
}

#if 0
void vstatus_line(std::string_view line);

template<typename... T>
void status_line(fmt::format_string<T...>&& str, T&&... args) {
    vstatus_line(fmt::format(str, std::forward<T>(args)...));
}

struct printer;

struct slot_printer : noncopyable
{
  private:
    printer* print;
    std::optional<size_t> slot;

    slot_printer(printer* print);
    friend slot_printer slot_print();

  public:
    ~slot_printer();
    void vprint(std::string_view text);

    template<typename... T>
    void operator()(fmt::format_string<T...> str, T&&... args) {
        vprint(fmt::format(str, std::forward<T>(args)...));
    }
};

slot_printer slot_print();

#endif
