
#pragma once

#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/ranges.h>

#include "str/str.h"
#include "util/allocator.h"

template<>
struct fmt::formatter<::str> : formatter<::str_view>
{
    auto format(::str const& str, format_context& ctx) const {
        return formatter<::str_view>::format(::str_view(str), ctx);
    }
};


using fmt_memory_buffer =
    fmt::basic_memory_buffer<char, fmt::inline_buffer_size, allocator<char>>;

inline str vformat(allocator<char> alloc,
                   fmt::string_view format_str,
                   fmt::format_args args) {
    auto buf = fmt_memory_buffer(alloc);
    fmt::vformat_to(std::back_inserter(buf), format_str, args);
    return str(buf.data(), buf.size());
}

template<typename... Args>
inline str format(fmt::string_view format_str, const Args&... args) {
    return vformat(allocator<char>{}, format_str, fmt::make_format_args(args...));
}

struct fmt_buf
{
    fmt_memory_buffer buffer{allocator<char>{}};

    template<typename... T>
    void operator()(fmt::format_string<T...> str, T&&... args) {
        fmt::format_to(
            std::back_inserter(buffer), str, std::forward<T>(args)...);
    }

    void write(str_view sv) { buffer.append(sv.data(), sv.data() + sv.size()); }

    std::string_view view() const { return {buffer.data(), buffer.size()}; }
    str get() const { return str{view()}; }
};

struct fmt_defer : fmt_buf
{
    FILE* out;
    fmt_defer(FILE* out = stdout)
        : out{out} {}
    ~fmt_defer() { std::fwrite(buffer.data(), 1, buffer.size(), out); }
};
