#include <cassert>
#include <chrono>
#include <cstdio>
#include <memory>
#include <mutex>
#include <set>
#include <thread>
#include <vector>

#include "common/print.h"

#include <fmt/ranges.h>
#include <sys/ioctl.h>

size_t max_verbosity = 0;
bool debug_print = true;

void vprint(std::string_view text) {
    std::fwrite(text.data(), 1, text.size(), stdout);
    std::fflush(stdout);
}

#if 0
struct printer
{
    std::mutex mutex;
    vector<std::string> lines;
    std::set<size_t> free_lines;
    size_t num_cols;
    std::optional<std::string> status_line;

    printer() {
        num_cols = 80;  // FIXME: use ioctl
        
        winsize ws;
        int ret = ioctl(1, TIOCGWINSZ, &ws);
        if (ret == 0) {
            num_cols = ws.ws_col;
        }
    }

    ~printer() {
        if (status_line.has_value())
            fmt::print("\e[A");
    }

    size_t line_count() const { return status_line.has_value() + lines.size(); }

    void print(std::string_view text) {
        if (text.empty())
            return;
        auto lock = std::unique_lock {mutex};

        fmt::print("\e[{}A\e[0J{}", line_count(), text);
        if (!text.ends_with("\n"))
            putchar('\n');

        print_all();
    }

    size_t alloc_line() {
        auto lock = std::unique_lock {mutex};
        size_t index;
        if (free_lines.empty()) {
            index = lines.size();
            lines.emplace_back();
            fmt::print("\n");
        } else {
            auto it = free_lines.begin();
            index = *it;
            free_lines.erase(it);
        }

        return index;
    }

    void free_line(size_t index) {
        auto lock = std::unique_lock {mutex};
        print_line(index, "");
        lines[index].clear();
        free_lines.insert(index);

        size_t up = 0;
        while (!lines.empty() && free_lines.contains(lines.size() - 1)) {
            free_lines.erase(lines.size() - 1);
            lines.pop_back();
            ++up;
        }
        if (up > 0) {
            fmt::print("\e[{}A", up);
            std::fflush(stdout);
        }
    }

    void set_line(size_t index, std::string_view text) {
        auto lock = std::unique_lock {mutex};
        lines[index] = sanitize(text);
        print_line(index, lines[index]);
    }

    void set_status(std::string_view text) {
        auto lock = std::unique_lock {mutex};

        std::string stext = sanitize(text);

        size_t count = line_count();

        if (status_line.has_value()) {
            status_line = stext;
            count = line_count();
            fmt::print(
                "\e[{}A{}\e[K\e[G\e[{}B", count, status_line.value(), count);
        } else {
            if (count > 0) {
                fmt::print("\e[{}A\e[0J", count);
            }
            status_line = stext;
            print_all();
        }
        std::fflush(stdout);
    }

  private:
    void print_line(size_t index, std::string_view text) const {
        assert(index < lines.size());

        size_t dist = lines.size() - index;

        fmt::print("\e[{}A{}\e[K\e[G\e[{}B", dist, text, dist);
        std::fflush(stdout);
    }

    std::string sanitize(std::string_view text) const {
        size_t len = std::min(num_cols, text.size());

        auto san = std::string {text.substr(0, len)};
        for (char& c : san)
            if (c == '\n')
                c = ' ';

        return san;
    }

    void print_all() const {
        if (status_line.has_value()) {
            fmt::print("{}\n", status_line.value());
        }
        if (!lines.empty())
            fmt::print("{}\n", fmt::join(lines, "\n"));
        std::fflush(stdout);
    }
};

static printer printer_;

void vprint(std::string_view text) {
    if (debug_print) {
        fmt::print("{}", text);
        return;
    }
    printer_.print(text);
}

void vstatus_line(std::string_view line) {
    if (debug_print) {
        fmt::print("{}\n", line);
        return;
    }
    printer_.set_status(line);
}
slot_printer::slot_printer(printer* print)
    : print {print} {}
slot_printer::~slot_printer() {
    if (slot.has_value())
        print->free_line(slot.value());
}

void slot_printer::vprint(std::string_view text) {
    if (text.empty())
        return;

    if (debug_print) {
        fmt::print("{}\n", text);
        return;
    }

    if (!slot.has_value())
        slot = print->alloc_line();

    printer_.set_line(slot.value(), text);
}

slot_printer slot_print() {
    return {&printer_};
}
#endif
