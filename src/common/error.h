#pragma once

#include <stdexcept>

#include <str/str.h>

#include "common/fmt.h"

struct error : std::runtime_error
{
    [[nodiscard]] error(str const& str)
        : std::runtime_error(std::string(std::string_view(str))) {
        // fmt::print("error::error {}\n", str);
    }

    template<typename... T>
    [[nodiscard]] error(fmt::format_string<T...> str, T&&... x)
        : error(::format(str, std::forward<T>(x)...)) {}
};
