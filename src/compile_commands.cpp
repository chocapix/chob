#include "compile_commands.h"

#include <path.h>
#include <str/algo.h>
#include <str/vec.h>
#include <util/timer.h>

#include "common/error.h"
#include "common/fmt.h"
#include "common/print.h"
#include "str/map.h"
#include "util/file.h"

namespace
{

template<typename... T>
[[noreturn]] void cc_error(fmt::format_string<T...> str, T&&... args) {
    throw error("compile_commands.json:{}",
                ::format(str, std::forward<T>(args)...));
}

str_view sub(str_view text, size_t pos, size_t count) {
    return {text.data() + pos, count};
}

struct tokenizer
{
    str_view text;
    size_t pos = 0, lin = 1;
    str_view curr;

    tokenizer(str_view text)
        : text(text) {
        advance();
    }

    str_view peek() const { return curr; }

    str_view next() {
        str_view tok = curr;
        advance();
        return tok;
    }

    void advance() {
        while (pos < text.size()) {
            switch (text[pos]) {
                case ' ':
                case '\t':
                case '\n': break;

                case '{':
                case '}':
                case '[':
                case ']':
                case ':':
                case ',':
                    curr = sub(text, pos, 1);
                    incr();
                    return;

                case '"': quoted_string(); return;

                default: cc_error(" unexpected character", lin);
            }
            incr();
        }

        curr = "__EOF__"sv;
    }

    void quoted_string() {
        size_t begin = pos;
        for (incr(); pos < text.size(); incr()) {
            switch (text[pos]) {
                case '"':
                    incr();
                    curr = sub(text, begin, pos - begin);
                    return;
                case '\\':
                    incr();
                    if (pos >= text.size())
                        cc_error("{}: unterminate quoted string", lin);
                    break;
            }
        }
        cc_error("{}: unterminate quoted string", lin);
    }

    void incr() {
        lin += text[pos] == '\n';
        pos++;
    }
};

template<char left, char right, typename ParseElem>
void parse_list(tokenizer& tok, ParseElem parse_elem) {
    if (tok.peek()[0] != left) cc_error("{}: expected '{}'", tok.lin, left);
    tok.advance();

    if (tok.peek()[0] == right) {
        tok.advance();
        return;
    }

    parse_elem();

    while (tok.peek()[0] != right) {
        if (tok.peek()[0] != ',') cc_error("{}: expected ','", tok.lin);
        tok.advance();
        parse_elem();
    }
    tok.advance();
}

void parse_str_vec(tokenizer& tok, str_vec& vec) {
    parse_list<'[', ']'>(tok,
                         [&]
                         {
                             if (tok.peek()[0] != '"')
                                 cc_error("{} expected quoted string", tok.lin);

                             vec.emplace_back(tok.peek());
                             tok.advance();
                         });
}

void parse_quoted_string(tokenizer& tok, str& string) {
    if (tok.peek()[0] != '"') cc_error("{} expected quoted string", tok.lin);

    string = tok.peek();
    tok.advance();
}

void parse_key_val(tokenizer& tok, compile_commands::entry& ent) {
    if (tok.peek()[0] != '"') cc_error("{} expected quoted string", tok.lin);

    str_view key = sub(tok.peek(), 1, tok.peek().size() - 2);
    tok.advance();

    if (tok.peek()[0] != ':') cc_error("{}: expected ':'", tok.lin);
    tok.advance();

    // clang-format off
    if (false);
    else if (key == "directory") parse_quoted_string(tok, ent.directory);
    else if (key == "file")      parse_quoted_string(tok, ent.file);
    else if (key == "command")   parse_quoted_string(tok, ent.command);
    else if (key == "output")    parse_quoted_string(tok, ent.output);
    else if (key == "arguments") parse_str_vec      (tok, ent.arguments);
    else cc_error("{}: invalid key", tok.lin);
    // clang-format on
}

void parse_entry(tokenizer& tok, compile_commands::entry& ent) {
    parse_list<'{', '}'>(tok, [&] { parse_key_val(tok, ent); });
}

void parse_entry_list(tokenizer& tok,
                      str_map<compile_commands::entry>& entries) {
    parse_list<'[', ']'>(tok,
                         [&]
                         {
                             compile_commands::entry ent;
                             parse_entry(tok, ent);
                             entries.emplace(ent.file, ent);
                         });
}

void store_str_vec(fmt_buf& out, str_vec const& vec) {
    out.write("["sv);
    bool first = true;
    for (auto&& elem : vec) {
        if (!first) out.write(", "sv);
        first = false;
        out.write(elem);
    }
    out.write("]"sv);
}

void store_entry(fmt_buf& out,
                 str_view qdirectory,
                 compile_commands::entry const& ent) {
    out.write("{\n  \"directory\": "sv), out.write(qdirectory);
    out.write(",\n  \"file\": "sv), out.write(ent.file);
    if (!ent.arguments.empty()) {
        out.write(",\n  \"arguments\": "sv);
        store_str_vec(out, ent.arguments);
    } else  //
        out.write(",\n  \"command\": "sv), out.write(ent.command);

    out.write(",\n  \"output\": "sv), out.write(ent.output);
    out.write("\n}"sv);
}

}  // namespace

str compile_commands::quote(str_view text) {
    str q;
    q.push_back('"');
    for (char c : text) {
        if (c == '\\' || c == '"') q.push_back('\\');
        q.push_back(c);
    }
    q.push_back('"');
    return q;
}
str compile_commands::unquote(str_view qtext) {
    str text;
    assert(qtext.size() >= 2);
    assert(qtext.front() == '"');
    assert(qtext.back() == '"');

    for (size_t i = 1; i + 1 < qtext.size(); ++i) {
        if (qtext[i] == '\\') ++i;
        text.push_back(qtext[i]);
    }
    return text;
}

compile_commands::compile_commands(str_view directory)
    : qdirectory(quote(directory)) {
    PROFILE_FUNCTION;
    filename = directory / "compile_commands.json";
    if (!path::exists(filename)) return;

    auto mf = mapped_file(filename, "r");
    auto tokens = tokenizer(mf.view());

    parse_entry_list(tokens, entries);

    if (tokens.peek() != "__EOF__")
        cc_error("{}: unexpected token", tokens.lin);
}
compile_commands::~compile_commands() {
    print(2, "write compile_commands.json\n");
    store();
}
void compile_commands::insert(str_vec const& arguments,
                              str_view file,
                              str_view output) {
    PROFILE_FUNCTION;
    auto qfile = quote(file);

    entry& ent = entries[qfile];

    ent.file = qfile;
    ent.directory = qdirectory;
    ent.arguments.clear();
    for (auto&& arg : arguments)  //
        ent.arguments.emplace_back(quote(arg));
    ent.command.clear();
    ent.output = quote(output);
}
void compile_commands::store() const {
    PROFILE_FUNCTION;
    fmt_buf buf;

    buf.write("[\n"sv);
    bool first = true;
    for (auto&& [fil, ent] : entries) {
        if (!first) buf.write(",\n"sv);
        first = false;
        store_entry(buf, fil, ent);
    }
    buf.write("]"sv);

    auto out = file(filename, "w");
    out.write(buf.view());
}
