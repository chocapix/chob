#include "resource.h"

#include <path.h>
#include <str/algo.h>

#include "common/fmt.h"
#include "common/print.h"

struct resource* resource;
void resource::init(std::string_view argv0) {
    progname = argv0;
    if (progname.find('/') == progname.npos) progname = path::lookup(progname);
    str bindir{path::parent(path::realpath(progname))};

    for (auto&& d : {bindir / "../../../share/chob",
                     bindir / "../share/chob",
                     bindir / "share/chob"})
    {
        print(2, "trying {}\n", d);
        if (path::exists(d)) {
            dir = d;
            break;
        }
    }

    dir = path::realpath(dir);

    print(2, "resource: {}\n", dir);

    char const* cxx = getenv("CXX");
    if (!cxx) cxx = "c++";

    compiler_flags.push_back(path::lookup(cxx));

    char const* flags = getenv("CXXFLAGS");
    if (flags) {
        for (auto&& flag : split(flags, ' ')) {
            compiler_flags.emplace_back(flag);
        }
    }

    print(2, "compiler & flags: {}\n", compiler_flags);
}
