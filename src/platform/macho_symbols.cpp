#include <bit>
#include <functional>

#include <str/algo.h>

#include "common/error.h"
#include "common/print.h"
#include "platform.h"

void parse_symbols(platform const& pinfo,
                   std::string_view filename,
                   std::string_view content,
                   std::function<void(std::string_view, bool)> on_sym);
namespace
{
enum
{
    MH_MAGIC_64 = 0xfeedfacf
};

struct mach_fat_header
{
    uint32_t magic;
    uint32_t nfat_arch;
};

struct mach_fat_arch
{
    uint32_t cputype;
    int cpusubtype;
    uint32_t offset;
    uint32_t size;
    uint32_t align;
};

struct mach_header_64
{
    uint32_t magic;
    int32_t cputype;
    int32_t cpusubtype;
    uint32_t filetype;
    uint32_t ncmds;
    uint32_t sizeofcmds;
    uint32_t flags;
    uint32_t reserved;
};

struct load_command
{
    uint32_t cmd;
    uint32_t cmdsize;
};

enum
{
    LC_SYMTAB = 0x2,
    LC_LOAD_DYLIB = 0xc,
};

union lc_str
{
    uint32_t offset;
};
struct dylib
{
    union lc_str name;
    uint32_t timestamp;
    uint32_t current_version;
    uint32_t compatibility_version;
};
struct dylib_command
{
    uint32_t cmd;
    uint32_t cmdsize;
    struct dylib dylib;
};
struct symtab_command
{
    uint32_t cmd;
    uint32_t cmdsize;
    uint32_t symoff;
    uint32_t nsyms;
    uint32_t stroff;
    uint32_t strsize;
};
struct nlist_64
{
    union
    {
        uint32_t n_strx;
    } n_un;
    uint8_t n_type;
    uint8_t n_sect;
    uint16_t n_desc;
    uint64_t n_value;
};

enum
{
    N_STAB = 0xe0,
    N_PEXT = 0x10,
    N_TYPE = 0x0e,
    N_EXT = 0x01,
};

enum
{
    N_UNDF = 0x0,
    N_ABS = 0x2,
    N_SECT = 0xe,
    N_PBUD = 0xc,
    N_INDR = 0xa,
};

static_assert(std::endian::native == std::endian::little);

uint32_t from_be(uint32_t x) { return __builtin_bswap32(x); }

}  // namespace

void macho_fat_symbols(platform const& pinfo,
                       std::string_view filename,
                       std::string_view content,
                       std::function<void(std::string_view, bool)> on_sym) {
    auto header = at<mach_fat_header>(content, 0);
    auto archtab = at<mach_fat_arch>(content, sizeof header[0]);
    for (uint32_t i = 0; i < from_be(header->nfat_arch); ++i) {
        uint32_t offset = from_be(archtab[i].offset);
        uint32_t size = from_be(archtab[i].size);
        uint32_t type = from_be(archtab[i].cputype);
        print(3,
              "macho_fat {} cputype {:08x} ? {:08x}\n",
              i,
              type,
              pinfo.macos.macho_cputype);
        if (type == pinfo.macos.macho_cputype) {
            content = content.substr(offset, size);
            return parse_symbols(pinfo, filename, content, on_sym);
        }
    }
    throw error("no suitable cputype ({:x}) in fat binary",
                pinfo.macos.macho_cputype);
}

void macho_symbols(platform const& pinfo,
                   std::string_view filename,
                   std::string_view content,
                   std::function<void(std::string_view, bool)> on_sym) {
    (void)pinfo;

    auto header = at<mach_header_64>(content, 0);

    size_t cmdsize = 0, offset = sizeof *header;
    for (size_t i = 0, ncmds = header->ncmds; i < ncmds; ++i, offset += cmdsize)
    {
        auto cmd = at<load_command>(content, offset);
        cmdsize = cmd->cmdsize;

        if (cmd->cmd == LC_SYMTAB) {
            auto sym_cmd = at<symtab_command>(content, offset);
            auto strings = at<char>(content, sym_cmd->stroff);

            auto begin = at<nlist_64>(content, sym_cmd->symoff);
            auto end = begin + sym_cmd->nsyms;
            for (auto sym = begin; sym < end; ++sym) {
                char const* name = strings + sym->n_un.n_strx;

                uint8_t stab = sym->n_type & N_STAB;
                uint8_t type = sym->n_type & N_TYPE;
                uint8_t ext = sym->n_type & N_EXT;
                if (stab || !ext) continue;

                bool defined;
                if (type == N_SECT) defined = true;
                else if (type == N_UNDF) defined = false;
                else throw error("{}: symbols N_TYPE", filename);

                on_sym(name, defined);
            }
        }
    }
}
