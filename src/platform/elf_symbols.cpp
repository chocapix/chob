#include <functional>

#include <str/algo.h>
#include <util/types.h>

#include "common/error.h"
#include "common/print.h"
#include "platform.h"

using u8 = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;
using u64 = std::uint64_t;

enum
{
    EI_CLASS = 4,
    EI_DATA = 5,
    EI_NIDENT = 16,
};

enum
{
    ELFCLASS32 = 1,
    ELFCLASS64 = 2,
};
enum
{
    ELFDATA2LSB = 1,
    ELFDATA2MSB = 2,
};

enum
{
    ET_NONE = 0x00,
    ET_REL = 0x01,
    ET_EXEC = 0x02,
    ET_DYN = 0x03,
    ET_CORE = 0x04,
};

enum
{
    SHT_NULL = 0x0,  // 	Section header table entry unused
    SHT_PROGBITS = 0x1,  // 	Program data
    SHT_SYMTAB = 0x2,  // 	Symbol table
    SHT_STRTAB = 0x3,  // 	String table
    SHT_RELA = 0x4,  // 	Relocation entries with addends
    SHT_HASH = 0x5,  // 	Symbol hash table
    SHT_DYNAMIC = 0x6,  // 	Dynamic linking information
    SHT_NOTE = 0x7,  // 	Notes
    SHT_NOBITS = 0x8,  // 	Program space with no data (bss)
    SHT_REL = 0x9,  // 	Relocation entries, no addends
    SHT_SHLIB = 0x0A,  // 	Reserved
    SHT_DYNSYM = 0x0B,  // 	Dynamic linker symbol table
    SHT_INIT_ARRAY = 0x0E,  // 	Array of constructors
    SHT_FINI_ARRAY = 0x0F,  // 	Array of destructors
    SHT_PREINIT_ARRAY = 0x10,  // 	Array of pre-constructors
    SHT_GROUP = 0x11,  // 	Section group
    SHT_SYMTAB_SHNDX = 0x12,  // 	Extended section indices
    SHT_NUM = 0x13,  // 	Number of defined types.
};

struct Elf64_Ehdr
{
    u8 e_ident[EI_NIDENT];
    u16 e_type;
    u16 e_machine;
    u32 e_version;
    u64 e_entry;
    u64 e_phoff;
    u64 e_shoff;
    u32 e_flags;
    u16 e_ehsize;
    u16 e_phentsize;
    u16 e_phnum;
    u16 e_shentsize;
    u16 e_shnum;
    u16 e_shstrndx;
};

struct Elf64_Shdr
{
    u32 sh_name;
    u32 sh_type;
    u64 sh_flags;
    u64 sh_addr;
    u64 sh_offset;
    u64 sh_size;
    u32 sh_link;
    u32 sh_info;
    u64 sh_addralign;
    u64 sh_entsize;
};

enum
{
    STB_LOCAL = 0,
    STB_GLOBAL = 1,
    STB_WEAK = 2,
    STB_NUM = 3,
    STB_LOOS = 10,
    STB_GNU_UNIQUE = 10,
    STB_HIOS = 12,
    STB_LOPROC = 13,
    STB_HIPROC = 15,
};
char const* stb_name(u8 stb) {
    switch (stb) {
        case STB_LOCAL: return "STB_LOCAL     ";
        case STB_GLOBAL: return "STB_GLOBAL    ";
        case STB_WEAK: return "STB_WEAK      ";
        case STB_NUM:
            return "STB_NUM       ";
            // case STB_LOOS:
            // return "STB_LOOS      ";
        case STB_GNU_UNIQUE: return "STB_GNU_UNIQUE";
        case STB_HIOS: return "STB_HIOS      ";
        case STB_LOPROC: return "STB_LOPROC    ";
        case STB_HIPROC: return "STB_HIPROC    ";
        default: return "STB_?";
    };
}
enum
{
    STT_NOTYPE = 0,
    STT_OBJECT = 1,
    STT_FUNC = 2,
    STT_SECTION = 3,
    STT_FILE = 4,
    STT_COMMON = 5,
    STT_TLS = 6,
    STT_NUM = 7,
    STT_LOOS = 10,
    STT_GNU_IFUNC = 10,
    STT_HIOS = 12,
    STT_LOPROC = 13,
    STT_HIPROC = 15,
};
char const* stt_name(u8 stt) {
    switch (stt) {
        case STT_NOTYPE: return "STT_NOTYPE    ";
        case STT_OBJECT: return "STT_OBJECT    ";
        case STT_FUNC: return "STT_FUNC      ";
        case STT_SECTION: return "STT_SECTION   ";
        case STT_FILE: return "STT_FILE      ";
        case STT_COMMON: return "STT_COMMON    ";
        case STT_TLS: return "STT_TLS       ";
        case STT_NUM: return "STT_NUM       ";
        case STT_LOOS: return "STT_LOOS      ";
        // case STT_GNU_IFUNC : return "STT_GNU_IFUNC ";
        case STT_HIOS: return "STT_HIOS      ";
        case STT_LOPROC: return "STT_LOPROC    ";
        case STT_HIPROC: return "STT_HIPROC    ";
        default: return "STT_?";
    }
}
enum
{
    STV_DEFAULT = 0,
    STV_INTERNAL = 1,
    STV_HIDDEN = 2,
    STV_PROTECTED = 3,
};
char const* stv_name(u8 stv) {
    switch (stv) {
        case STV_DEFAULT: return "STV_DEFAULT  ";
        case STV_INTERNAL: return "STV_INTERNAL ";
        case STV_HIDDEN: return "STV_HIDDEN   ";
        case STV_PROTECTED: return "STV_PROTECTED";
        default: return "STV_?";
    }
}

struct Elf64_Sym
{
    u32 st_name;
    u8 st_info;
    u8 st_other;
    u16 st_shndx;
    u64 st_value;
    u64 st_size;

    u8 st_bind() const { return st_info >> 4; }
    u8 st_type() const { return st_info & 0x0f; }
};

struct elf_file
{
    std::string_view content;

    Elf64_Ehdr const* header;
    Elf64_Shdr const* section_names;

    elf_file(std::string_view content)
        : content{content} {
        header = at<Elf64_Ehdr>(0);
        bool is_64 = header->e_ident[EI_CLASS] == ELFCLASS64;
        bool is_lsb = header->e_ident[EI_DATA] == ELFDATA2LSB;
        if (!(is_64 && is_lsb))
            throw std::runtime_error("only ELF 64-bit LSB is supported");

        switch (header->e_type) {
            case ET_REL:
            case ET_DYN: break;
            default: throw std::runtime_error("unknowm ELF type");
        }
        section_names = section(header->e_shstrndx);
    }

    Elf64_Shdr const* section(u16 index) const {
        return at<Elf64_Shdr>(header->e_shoff + header->e_shentsize * index);
    }

    Elf64_Shdr const* section(u32 sh_type, std::string_view sh_name) const {
        for (u16 i = 0; i < header->e_shnum; ++i) {
            auto sh = section(i);
            if (sh->sh_type == sh_type && name(sh) == sh_name) return sh;
        }
        return 0;
    }

    char const* name(Elf64_Shdr const* sh) const {
        return content.data() + section_names->sh_offset + sh->sh_name;
    }

    template<typename T, typename Offset>
    T const* at(Offset offset) const {
        return ::at<T, Offset>(content, offset);
    }
};

namespace
{

}

void elf_symbols(platform const&,
                 std::string_view filename,
                 std::string_view content,
                 std::function<void(std::string_view, bool)> on_sym) {
    elf_file elf{content};

    std::string_view strtab = ".strtab";
    auto symbols = elf.section(SHT_SYMTAB, ".symtab");
    if (!symbols) {
        symbols = elf.section(SHT_DYNSYM, ".dynsym");
        strtab = ".dynstr";
    }
    if (!symbols) {
        print(2, "warning: {}: no symbol tables\n", filename);
        return;
    }

    auto strings = elf.section(SHT_STRTAB, strtab);

    if (!strings) {
        throw error("{}: {} not found", filename, strtab);
    }

    size_t count = 0;
    size_t def_count = 0;
    for (u64 offset = symbols->sh_offset,
             end = offset + symbols->sh_size,
             step = symbols->sh_entsize;
         offset < end;
         offset += step)
    {
        auto sym = at<Elf64_Sym>(content, offset);

        bool has_name = sym->st_name != 0;
        // bool is_hidden = sym->st_other == STV_HIDDEN;
        bool is_export =  //
            sym->st_bind() == STB_GLOBAL ||  //
            sym->st_bind() == STB_WEAK ||  //
            sym->st_bind() == STB_GNU_UNIQUE;

        std::string_view name = "?";
        if (has_name) name = content.data() + strings->sh_offset + sym->st_name;

        print(3,
              "  {} {} {} {}\n",
              stt_name(sym->st_type()),
              stb_name(sym->st_bind()),
              stv_name(sym->st_other),
              name);

        if (!has_name || !is_export) continue;

        bool is_defined = sym->st_type() != STT_NOTYPE;

        def_count += is_defined;
        ++count;
        on_sym(name, is_defined);
    }
    print(2, "{}: {} defined {} symbols total\n", filename, def_count, count);
}
