#include <cassert>
#include <functional>

#include <path.h>
#include <util/file.h>

#include "common/print.h"
#include "platform.h"

namespace
{
size_t find_first_of(std::string_view text, char const* set) {
    size_t pos = text.find_first_of(set);
    if (pos == text.npos) pos = text.size();
    return pos;
}
void skip_spaces(std::string_view& text) {
    // fmt::print("{} at '{}'\n", __FUNCTION__, text);
    if (text.empty()) return;
    for (;;) {
        switch (text[0]) {
            case ' ':
            case '\t':
            case '\n': text.remove_prefix(1); continue;
        }
        break;
    }
}
void skip_spaces_comments(std::string_view& text) {
    // fmt::print("{} at '{}'\n", __FUNCTION__, text);
    for (;;) {
        skip_spaces(text);
        if (text.starts_with("/*")) {
            text.remove_prefix(2);
            size_t end = text.find("*/");
            assert(end != text.npos);
            text.remove_prefix(end + 2);
            continue;
        }
        break;
    }
}
std::string_view eat(std::string_view& text, size_t len, size_t token_len) {
    std::string_view token = text.substr(0, token_len);
    text.remove_prefix(len);
    return token;
}
std::string_view eat(std::string_view& text, size_t len) {
    return eat(text, len, len);
}
std::string_view next(std::string_view& text) {
    // fmt::print("{} at '{}'\n", __FUNCTION__, text);
    skip_spaces_comments(text);
    if (text.empty()) return text;
    if (text[0] == '"') {
        text.remove_prefix(1);
        size_t end = text.find('"');

        assert(end != text.npos);
        assert(end > 0);
        return eat(text, end + 1, end);
    }
    if (text[0] == '(' || text[0] == ')' || text[0] == ',') {
        return eat(text, 1);
    }
    size_t end = find_first_of(text, "(), \t\n\"");
    return eat(text, end);
}
vector<std::string_view> lex(std::string_view text) {
    vector<std::string_view> tokens;
    std::string_view token;
    do {
        token = next(text);
        tokens.push_back(token);
    } while (!token.empty());
    return tokens;
}
template<typename... T>
[[noreturn]] void error(fmt::format_string<T...> str, T&&... args) {
    throw std::runtime_error(fmt::format(str, std::forward<T>(args)...));
}
using view_vec = vector<std::string_view>;
using str_vec = vector<str>;
struct parser
{
    static str_vec parse(std::string_view text) {
        parser p{text};
        return p.files;
    }
    struct command
    {
        str name;
        vector<command> args;
    };
    // static str str(command const& cmd);
    static str get(vector<command> const& cmds) {
        str text = "{";
        for (auto&& cmd : cmds) {
            text += get(cmd);
            text += ",";
        }
        text += "}";
        return text;
    }
    static str get(command const& cmd) { return cmd.name + get(cmd.args); }
    view_vec tokens;
    std::string_view const* pos;
    str_vec files;
    parser(std::string_view text)
        : tokens(lex(text))
        , pos(&tokens[0]) {
        auto cmds = parse_list();
        print(2, "parsed: {}\n", get(cmds));
        get_files(cmds);
    }
    template<typename U, typename... T>
    bool in(U x, T&&... words) {
        return ((x == words) || ...);
    }

    void get_files(vector<command> const& cmds) {
        for (command const& cmd : cmds) {
            if (in(cmd.name, "GROUP", "INPUT", "AS_NEEDED")) {
                get_files(cmd.args);
            } else if (cmd.args.empty()) {
                files.emplace_back(cmd.name);
            }
        }
    }
    vector<command> parse_list() {
        // fmt::print("parser::{} at '{}'\n", __FUNCTION__, *pos);
        vector<command> cmds;
        while (!in(*pos, "", ")")) {
            cmds.emplace_back(parse_command());
            if (in(*pos, ",", ";")) pos++;
        }
        return cmds;
    }
    command parse_command() {
        // fmt::print("parser::{} at '{}'\n", __FUNCTION__, *pos);
        command cmd;
        if (in(*pos, "", "(", ")", ",", ";"))
            error("unexpected token: {}", *pos);
        cmd.name = *pos++;
        if (in(*pos, "(")) {
            pos++;
            cmd.args = parse_list();
            if (!in(*pos, ")")) error("expected )");
            ++pos;
        }
        return cmd;
    }
};
}  // namespace
void parse_symbols(platform const& pinfo,
                   std::string_view filename,
                   std::string_view content,
                   std::function<void(std::string_view, bool)> on_sym);
void gnu_script_symbols(platform const& pinfo,
                        std::string_view filename,
                        std::string_view content,
                        std::function<void(std::string_view, bool)> on_sym) {
    str dir{path::parent(filename)};
    for (auto&& item : parser::parse(content)) {
        str fullpath = pinfo.lookup(item, dir, pinfo.libpath);

        print(3, "gnu_script {}: item '{}'\n", filename, item);
        print(2, "gnu_script {}: load '{}'\n", filename, fullpath);
        auto mf = file(fullpath, "r").map();
        // file_mapping mf{fullpath, "r"};
        parse_symbols(pinfo, fullpath, mf.view(), on_sym);
    }
}
