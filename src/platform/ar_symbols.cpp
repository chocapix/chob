#include <cassert>
#include <functional>

#include "common/print.h"
#include "platform.h"

void parse_symbols(platform const& pinfo,
                   std::string_view filename,
                   std::string_view content,
                   std::function<void(std::string_view, bool)> on_sym);
namespace
{

struct ar
{
    struct header
    {
        char name[16];
        char mod_time[12];
        char userid[6];
        char groupid[6];
        char mode[8];
        char size[10];
        char trailer[2];

        std::string_view real_name(std::string_view& data,
                                   std::string_view strings) const {
            using namespace std::string_view_literals;
            std::string_view name_view{name, sizeof name};

            if (name_view.starts_with("#1/")) {
                size_t name_size = scan_size_t(name_view.substr(3));
                name_view = data.substr(0, name_size);
                data.remove_prefix(name_size);
                return trim(name_view, "\0"sv);
            }

            if (name_view.starts_with("/")) {
                size_t name_index = scan_size_t(name_view.substr(1));
                return trim(strings.substr(name_index), "/\n");
            }

            return trim(name_view, "/");
        }

        static std::string_view trim(std::string_view text,
                                     std::string_view trailer) {
            size_t pos = text.find(trailer);
            if (pos != text.npos) text = text.substr(0, pos);
            return text;
        }

        static size_t scan_size_t(std::string_view text) {
            return scan_size_t(text.data());
        }
        static size_t scan_size_t(char const* text) {
            size_t result = 0;
            int r = sscanf(text, "%lu", &result);
            if (r != 1) throw std::runtime_error("invalid archive (name size)");
            return result;
        }
    };

    struct ar_file
    {
        std::string_view filename;
        std::string_view content;

        ar_file(std::string_view f, std::string_view c)
            : filename{f}
            , content{c} {}
    };

    ar(std::string_view data) {
        assert(is_ar(data));
        data = data.substr(8);

        std::string_view strings;

        while (!data.empty()) {
            if (data.size() < sizeof(header))
                throw std::runtime_error("invalid archive (header)");

            auto h = reinterpret_cast<header const*>(data.data());
            data = data.substr(sizeof(header));

            size_t size = h->scan_size_t(h->size);
            std::string_view content = data.substr(0, size);
            std::string_view name{h->name, sizeof h->name};

            if (name.starts_with("// ")) {
                strings = content;
            } else if (name.starts_with("/ ") || name.starts_with("/SYM64/ ")) {
                // skip
            } else {
                name = h->real_name(content, strings);
                if (name != "__.SYMDEF" && name != "__.SYMDEF SORTED")
                    members.emplace_back(name, content);
            }

            // size must be even
            data.remove_prefix(size + size % 2);
        }
    }

    auto begin() const { return members.begin(); }
    auto end() const { return members.end(); }

    vector<ar_file> members;

    static bool is_ar(std::string_view data) {
        if (data.size() < 8) return false;
        return data.substr(0, 8) == "!<arch>\n";
    }
};
}  // namespace

void ar_symbols(platform const& pinfo,
                std::string_view filename,
                std::string_view content,
                std::function<void(std::string_view, bool)> on_sym) {
    // TIMED_FUNCTION;
    for (auto&& member : ar(content)) {
        // if (member.filename == "__.SYMDEF SORTED"
        //     || member.filename == "__.SYMDEF" || member.filename == "/")
        // {
        //     continue;
        // }
        print(4, "ar: parse_symbols {}\n", member.filename);
        std::string member_name =
            fmt::format("{}[{}]", filename, member.filename);
        parse_symbols(pinfo, member_name, member.content, on_sym);
    }
}
