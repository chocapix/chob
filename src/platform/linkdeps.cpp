#include <algorithm>
#include <optional>

#include "linkdeps.h"

#include <str/vec.h>
#include <util/timer.h>

#include "common/print.h"
#include "platform/platform.h"

str_vec link_deps::deps(std::string_view path) {
    str_vec vec;
    auto it = nodes_by_path.find(path);
    if (it == nodes_by_path.end()) return vec;
    node* n = &it->second;

    for (auto&& d : n->deps) {
        vec.push_back(d->path);
    }
    return vec;
}

inline profiler prof_ld_insert{"link_deps::insert"};
vector<str_vec> link_deps::insert(std::string_view path) {
    return insert(path, pinfo->read_symbols(path));
}
vector<str_vec> link_deps::insert(std::string_view path,
                                  platform::symbols const& syms) {
    print(2, "insert {}\n", path);
    auto _ = prof_ld_insert();
    auto [it, inserted] =
        nodes_by_path.emplace(path, node{path, syms.undefined});
    (void)inserted, assert(inserted);
    node* current = &it->second;

    for (auto&& sym : syms.defined) {
        if (sym == pinfo->main_symbol) {
            print(2, "main symbol in {}\n", path);
            pending_mains.emplace(current);
        }

        provider_by_symbol.emplace(sym, current);
        auto [it, end] = users_by_symbol.equal_range(sym);
        for (; it != end; ++it) {
            it->second->update(sym, current);
        }
    }

    for (auto&& sym : syms.undefined) {
        users_by_symbol.emplace(sym, current);
        auto [it, end] = provider_by_symbol.equal_range(sym);
        for (; it != end; ++it) {
            current->update(sym, it->second);
        }
    }

    return process_mains();
}

vector<str_vec> link_deps::pending() {
    vector<str_vec> mains;
    for (auto&& main : pending_mains) {
        // auto [deps, _] = main_deps(main, true);
        // mains.emplace_back(deps);
        std::optional<str_vec> deps = main_deps(main, true);
        assert(deps.has_value());
        mains.emplace_back(deps.value());
    }

    return mains;
}

str_vec link_deps::missing_symbols(std::string_view path) {
    auto it = nodes_by_path.find(path);
    assert(it != nodes_by_path.end());
    return {it->second.undef_syms.begin(), it->second.undef_syms.end()};
}

inline profiler prof_ld_mains{"link_deps::process_mains"};
vector<str_vec> link_deps::process_mains() {
    auto _ = prof_ld_mains();
    vector<str_vec> new_mains;
    vector<node*> to_remove;
    for (auto&& main : pending_mains) {
        // auto [deps, has_pending] = main_deps(main);

        // if (has_pending) {
        //     continue;
        // }
        std::optional<str_vec> deps = main_deps(main, false);
        if (!deps.has_value()) continue;

        print(2, "{} is complete\n", main->path);
        to_remove.push_back(main);
        new_mains.emplace_back(deps.value());
    }
    for (auto&& main : to_remove) pending_mains.erase(main);

    return new_mains;
}

inline profiler prof_ld_update{"link_deps::node::update"};
void link_deps::node::update(str const& sym, node* provider) {
    // auto _p = prof_ld_update();
    bool removed = undef_syms.erase(sym) > 0;
    if (!removed) return;

    auto [_, inserted] = deps.insert(provider);
    if (!inserted) return;
}

bool link_deps::get_deps(str_vec& deps, node* nod, bool force) {
    if (is_marked(nod)) return true;
    mark(nod);
    if (!nod->undef_syms.empty() && !force) return false;

    auto node_deps = vector<node*>(nod->deps.begin(), nod->deps.end());
    std::sort(node_deps.begin(),
              node_deps.end(),
              [](auto a, auto b) { return a->path < b->path; });

    for (node* next : node_deps) {
        bool ok = get_deps(deps, next, force);
        if (!ok) return false;
    }

    deps.push_back(nod->path);
    return true;
}

inline profiler prof_main_deps{"link_deps::main_deps"};
std::optional<str_vec> link_deps::main_deps(node* main, bool force) {
    auto _ = prof_main_deps();

    mark_clear();
    str_vec deps;
    bool ok = get_deps(deps, main, force);
    if (!ok) return {};

    for (size_t i = 0, j = deps.size(); i + 1 < j; ++i, --j) {
        using std::swap;
        swap(deps[i], deps[j - 1]);
    }

    return deps;
}
