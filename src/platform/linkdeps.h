#pragma once

#include <cassert>
#include <optional>

#include "platform.h"
#include "str/map.h"
#include "str/set.h"
#include "util/types.h"

struct link_deps : noncopyable
{
    void init(platform const* info) {
        assert(pinfo == 0);
        pinfo = info;
    }

    vector<str_vec> insert(std::string_view path,
                           platform::symbols const& syms);
    vector<str_vec> insert(std::string_view path);
    vector<str_vec> pending();

    str_vec missing_symbols(std::string_view path);

    str_vec deps(std::string_view path);

  private:
    struct node : noncopyable
    {
        size_t mark_index;
        str path;
        str_set undef_syms;
        set<node*> deps;

        node(node&& other)
            : mark_index{0} {
            using std::swap;
            swap(mark_index, other.mark_index);
            swap(path, other.path);
            swap(undef_syms, other.undef_syms);
            swap(deps, other.deps);
        }

        node(std::string_view path, str_vec const& undef_syms)
            : mark_index{0}
            , path{path}
            , undef_syms(undef_syms.begin(), undef_syms.end()) {}

        void update(str const& sym, node* provider);
    };

    void mark(node* bin) { bin->mark_index = mark_index; }
    bool is_marked(node const* bin) const {
        return bin->mark_index == mark_index;
    }
    void mark_clear() { mark_index++; }

    vector<str_vec> process_mains();

    std::optional<str_vec> main_deps(node* bin, bool force = true);
    bool get_deps(str_vec& deps, node* nod, bool force);

    size_t mark_index = 0;
    platform const* pinfo = 0;
    str_map<node> nodes_by_path;
    str_multimap<node*> provider_by_symbol;
    str_multimap<node*> users_by_symbol;
    set<node*> pending_mains;
};
