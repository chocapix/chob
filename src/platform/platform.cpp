#include <functional>

#include "platform.h"

#include <path.h>
#include <str/algo.h>
#include <str/map.h>
#include <str/str.h>
#include <util/file.h>
#include <util/subprocess.h>
#include <util/timer.h>

#include "common/error.h"
#include "common/print.h"

str platform::lookup1(std::string_view name, std::string_view path) const {
    if (name.starts_with("/")) {
        print(3, "  try {}\n", name);
        if (path::exists(name)) return path::realpath(name);
        return {};
    }

    if (name.starts_with("-l")) {
        for (auto&& ext : ld_ext) {
            str fullpath = ::format("{}/lib{}{}", path, name.substr(2), ext);
            print(3, "  try {}\n", fullpath);
            if (path::exists(fullpath)) return path::realpath(fullpath);
        }
        return {};
    }

    str fullpath = ::format("{}/{}", path, name);
    print(3, "  try {}\n", fullpath);
    if (path::exists(fullpath)) return path::realpath(fullpath);

    return {};
}

platform::lib_names platform::dyn_names(str_view lib, str_view version) {
    bool is_macos = !macos.target.empty();

    size_t major_end = nfind(version, '.');
    auto major = str(version.substr(0, major_end));
    if (!major.empty()) major = "." + major;
    auto ver = str(version);
    if (!ver.empty()) ver = "." + ver;

    lib_names names;
    names.namelink = lib;

    if (is_macos) {
        auto ext = path::extension(lib);
        auto stem = path::without_extension(lib);

        names.soname = stem + major + ext;
        names.realname = stem + ver + ext;
    } else {
        names.soname = lib + major;
        names.realname = lib + ver;
    }

    print(0, "dyn_names {} version {}\n" , lib, version);
    print(0, "  realname {}\n" , names.realname);
    print(0, "  namelink {}\n" , names.namelink);
    print(0, "    soname {}\n" , names.soname);

    return names;
}

void parse_symbols(platform const& pinfo,
                   std::string_view filename,
                   std::string_view content,
                   std::function<void(std::string_view, bool)> on_sym);

inline profiler prof_symbols{"platform::read_symbols"};
struct platform::symbols platform::read_symbols(std::string_view path) const
{
    auto _ = prof_symbols();
    struct platform::symbols syms;
    auto on_sym = [&](std::string_view sym, bool is_def)
    {
        if (stdsyms.contains(sym)) return;
        if (is_def) syms.defined.emplace_back(sym);
        else syms.undefined.emplace_back(sym);
    };

    file_mapping map = file(path, "r").map();
    parse_symbols(*this, map.filename(), map.view(), on_sym);
    print(2,
          "{}: {} def {} undef symbols\n",
          path,
          syms.defined.size(),
          syms.undefined.size());
    return syms;
}

namespace
{

// struct split_string
// {
//     str_vec split;
//     str str;

//     split_string(str_vec const& v)
//         : split {v}
//         , str {fmt::format("{}", fmt::join(v, " "))} {}
// };

str_vec linker_args(str_vec const& compiler_flags) {
    tempdir td{"./chob-XXXXXXX"};
    str_view dir = td.path();
    str srcpath = dir / "main.cpp";
    // str objpath = dir / "main.o";
    str exepath = dir / "main";
    {
        file src{dir / "main.cpp", "w"};
        src.write("int main(){return 0;}\n"sv);
    }

    auto output = file::temp();

    // subprocess proc{};

    str_vec cmd;
    append(cmd, compiler_flags, "--verbose", "-o", exepath, srcpath);
    auto result = subprocess(cmd).err(output).throw_on_error(false).run();
    file_mapping mf = output.map();

    if (!result.success()) {
        ::print(0, "{} \033[31mFAILED\033[0m:\n", fmt::join(cmd, " "));
        ::print(0, "{}", mf.view());
        throw error("subprocess failed");
    }

    str line;
    for (auto&& l : split(mf.view(), '\n')) {
        print(4, "is_linker? {}\n", l);
        if (l.find(fmt::format("-o {}", exepath)) != l.npos) {
            line = l;
            break;
        }
    }
    if (line.empty()) throw error("failed to get linker command line");
    print(3, "is_linker. {}\n", line);

    str_vec ld;
    bool first = true;
    for (auto&& arg : split(line, ' ')) {
        if (!first) ld.emplace_back(arg);
        first = false;
    }

    size_t j = 0;
    for (size_t i = 0; i < ld.size(); ++i) {
        if (ld[i].ends_with(".o") && !path::exists(ld[i])) continue;
        if (ld[i] == "-o" && i + 1 < ld.size() && ld[i + 1] == exepath) {
            i++;
            continue;
        }

        ld[j++] = ld[i];
    }
    ld.resize(j);

    print(3, "linker arguments:\n");
    for (auto&& a : ld) print(3, "  {}\n", a);

    return ld;
}

enum pfm_type
{
    PFM_MACOS,
    PFM_LINUX,
    PFM_UNKNOWN
};

void match_target_triples(str_map<size_t>& triples, str_view text) {
    for (str_view elem : split(text, '/')) {
        if (elem.find('.') != elem.npos) continue;

        str_vec vec = vsplit(elem, '-');
        if (vec.size() != 3)  //
            continue;

        bool is_triple = true;
        for (auto&& x : vec) {
            if (x.empty()) is_triple = false;
        }
        if (!is_triple) continue;

        str triple{elem};
        print(3, "found target triple: {}\n", triple);
        triples[triple]++;
    }
}

pfm_type detect_type(str_vec const& ld) {
    bool is_platform = false;
    for (auto&& arg : ld) {
        if (is_platform && arg == "macos") return PFM_MACOS;

        is_platform = arg == "-platform_version";
    }

    str_map<size_t> triples;
    for (auto&& arg : ld)  //
        match_target_triples(triples, arg);

    size_t max_count = 0;
    str triple;
    for (auto&& pair : triples)
        if (pair.second > max_count) {
            max_count = pair.second;
            triple = pair.first;
        }

    print(3, "{} occurences of target triple {}\n", max_count, triple);

    if (!triple.empty()) return PFM_LINUX;

    return PFM_UNKNOWN;
}

void deduplicate_path(str_vec& paths) {
    size_t j = 0;
    for (size_t i = 0; i < paths.size(); ++i)
        if (path::exists(paths[i])) paths[j++] = path::realpath(paths[i]);
    paths.resize(j);

    deduplicate(paths);
}

str_vec find_libs(str_vec const& ld, platform& pfm) {
    str_map<size_t> nargs = {
        {"-o", 1},
        {"-isystem", 1},

        {"-plugin", 1},
        {"-dynamic-linker", 1},
        {"-z", 1},

        {"-lto_library", 1},
        {"-arch", 1},
        {"-platform_version", 3},
        {"-rpath", 1},
    };

    str_vec libs;

    for (size_t i = 0; i < ld.size(); ++i) {
        if (auto it = nargs.find(ld[i]); it != nargs.end()) {
            i += it->second;
        } else if (ld[i].starts_with("-l")) {
            libs.emplace_back(ld[i]);
        } else if (ld[i] == "-L" && i + 1 < ld.size()) {
            pfm.libpath.emplace_back(ld[i + 1]);
            ++i;
        } else if (ld[i].starts_with("-L")) {
            pfm.libpath.emplace_back(ld[i].substr(2));
        } else if (ld[i] == "-syslibroot" && i + 1 < ld.size()) {
            pfm.sysroot = ld[i + 1];
            ++i;
        } else if (!ld[i].starts_with("-")) {
            libs.emplace_back(ld[i]);
        }
    }
    append(pfm.libpath, "/usr/lib", "/lib");

    if (!pfm.sysroot.empty()) {
        for (size_t i = 0, size = pfm.libpath.size(); i < size; ++i)
            pfm.libpath.push_back(
                ::format("{}/{}", pfm.sysroot, pfm.libpath[i]));
    }

    deduplicate_path(pfm.libpath);
    print(3, "libpath:\n");
    for (auto&& l : pfm.libpath) print(3, "  {}\n", l);

    size_t j = 0;
    for (size_t i = 0; i < libs.size(); ++i) {
        str fullpath = pfm.liblookup(libs[i]);
        if (!fullpath.empty()) {
            if (libs[i].starts_with("/")) print(3, "found {}\n", fullpath);
            else print(3, "found {} at {}\n", libs[i], fullpath);
            libs[j++] = fullpath;
        } else print(3, "did not find {}\n", libs[i]);
    }
    libs.resize(j);
    deduplicate(libs);

    print(3, "libs:\n");
    for (auto&& l : libs) print(3, "  {}\n", l);

    return libs;
}

void detect_linux(platform& pfm, str_vec const&) {
    print(1, "detected linux platform\n");
    append(pfm.ld_ext, ".so", ".a");
    pfm.main_symbol = "main";
    pfm.sharedlib_ext = ".so";
}

void detect_macos(platform& pfm, str_vec const& ld) {
    print(1, "detected macos platform\n");
    append(pfm.ld_ext, ".tbd", ".dylib", ".so", ".a");
    pfm.main_symbol = "_main";
    pfm.sharedlib_ext = ".dylib";

    for (size_t i = 0; i < ld.size(); ++i) {
        if (ld[i] == "-arch" && i + 1 < ld.size()) {
            std::string_view arch = ld[i + 1];
            if (arch == "arm64") {
                pfm.macos.macho_cputype = 0x0100000c;
            } else {
                pfm.macos.macho_cputype = 0x01000007;
            }
            pfm.macos.target = fmt::format("{}-macos", arch);
        }
    }
}

}  // namespace

void platform::detect(str_vec const& compiler_flags) {
    str_vec ld = linker_args(compiler_flags);
    auto type = detect_type(ld);
    switch (type) {
        case PFM_LINUX: detect_linux(*this, ld); break;
        case PFM_MACOS: detect_macos(*this, ld); break;
        default:
            print(0, "\033[35mwarning\033[m: failed to detect platform type");
    }

    str_vec libs = find_libs(ld, *this);

    for (auto&& symbol : {
             "___dso_handle",
             "____asan_globals_registered",
             "_GLOBAL_OFFSET_TABLE_",
         })
        stdsyms.emplace(symbol);
    size_t sym_count = 0;
    auto on_sym = [&, this](std::string_view sym, bool is_def)
    {
        if (is_def) {
            sym_count++;
            auto [it, inserted] = stdsyms.emplace(sym);
            if (!inserted) {
                print(4, "duplicate symbol {}\n", sym);
            }
        }
    };

    size_t prev_size = 0;
    for (auto&& lib : libs) {
        sym_count = 0;
        mapped_file mf{lib, "r"};
        print(3, "symbols from {}\n", lib);
        parse_symbols(*this, mf.filename(), mf.view(), on_sym);
        print(
            3, "added {}/{} symbols\n", stdsyms.size() - prev_size, sym_count);
        prev_size = stdsyms.size();
    }

    print(2, "{} symbols total\n", stdsyms.size());
}

void tapi_symbols(platform const& pinfo,
                  std::string_view filename,
                  std::string_view content,
                  std::function<void(std::string_view, bool)>);
void ar_symbols(platform const& pinfo,
                std::string_view filename,
                std::string_view content,
                std::function<void(std::string_view, bool)>);
void macho_symbols(platform const& pinfo,
                   std::string_view filename,
                   std::string_view content,
                   std::function<void(std::string_view, bool)>);
void macho_fat_symbols(platform const& pinfo,
                       std::string_view filename,
                       std::string_view content,
                       std::function<void(std::string_view, bool)>);
void elf_symbols(platform const& pinfo,
                 std::string_view filename,
                 std::string_view content,
                 std::function<void(std::string_view, bool)>);
void gnu_script_symbols(platform const& pinfo,
                        std::string_view filename,
                        std::string_view content,
                        std::function<void(std::string_view, bool)> on_sym);
namespace
{
bool is_ascii_text(char c) {
    switch (c) {
        case '\n':
        case '\t':
        case '\v':
        case '\r': return true;
        default: return 32 <= c && c <= 126;
    }
}
bool is_ascii_text(std::string_view content) {
    for (char c : content) {
        if (!is_ascii_text(c)) return false;
    }
    return true;
}

enum filetype
{
    TBD,
    MACHO,
    MACHO_FAT,
    AR,
    ELF,
    GNU_SCRIPT,
    UNKNOWN,
};

filetype get_type(std::string_view content) {
    if (content.starts_with("--- !tapi-tbd")) return TBD;
    if (content.starts_with("!<arch>\n")) return AR;
    if (content.starts_with("\xcf\xfa\xed\xfe")) return MACHO;
    if (content.starts_with("\xca\xfe\xba\xbe")) return MACHO_FAT;
    if (content.starts_with("\177ELF")) return ELF;
    if (is_ascii_text(content)) return GNU_SCRIPT;

    return UNKNOWN;
}

}  // namespace

void parse_symbols(platform const& pfm,
                   std::string_view filename,
                   std::string_view content,
                   std::function<void(std::string_view, bool)> on_sym) {
    switch (get_type(content)) {
        case TBD: return tapi_symbols(pfm, filename, content, on_sym);
        case AR: return ar_symbols(pfm, filename, content, on_sym);
        case MACHO: return macho_symbols(pfm, filename, content, on_sym);
        case MACHO_FAT:
            return macho_fat_symbols(pfm, filename, content, on_sym);
        case ELF: return elf_symbols(pfm, filename, content, on_sym);
        case GNU_SCRIPT:
            return gnu_script_symbols(pfm, filename, content, on_sym);
        case UNKNOWN: throw error("{}: unknown file type", filename);
        default: throw error("{}: unimplemented", filename);
    }
}
