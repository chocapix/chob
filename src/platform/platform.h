#pragma once

#include <str/set.h>
#include <str/vec.h>
#include <util/types.h>

struct platform
{
    using is_serial = int;

    void detect(str_vec const& compiler_flags);

    struct lib_names {
        str namelink;
        str soname;
        str realname;
    };
    lib_names dyn_names(str_view lib, str_view version);

    str liblookup(std::string_view name) const { return lookup(name, libpath); }

    template<typename... T>
    str lookup(std::string_view name,
               std::string_view path,
               T&&... others) const {
        str result = lookup1(name, path);
        if (!result.empty()) return result;
        return lookup(name, std::forward<T>(others)...);
    }

    template<typename... T>
    str lookup(std::string_view name,
               str_vec const& paths,
               T&&... others) const {
        for (auto&& path : paths) {
            str result = lookup1(name, path);
            if (!result.empty()) return result;
        }
        return lookup(name, std::forward<T>(others)...);
    }
    str lookup(std::string_view) const { return {}; }

    // str old_liblookup(std::string_view name) const {
    //     assert(name.starts_with("-l"));
    //     str fullname = "lib";
    //     fullname += name.substr(2);
    //     fullname += ld_ext[0];
    //     return old_lookup(fullname);
    // }
    // str old_lookup(std::string_view name) const {
    //     str path = old_lookup(name, sysroot);
    //     if (!path.empty())
    //         return path;
    //     return old_lookup(name, "/");
    // }
    // str old_lookup(std::string_view name, std::string_view root)
    // const;

    struct
    {
        using is_serial = int;
        str target;
        std::uint32_t macho_cputype;
    } macos;

    str main_symbol;
    str sharedlib_ext;
    str staticlib_ext = ".a";

    str sysroot;
    str_vec libpath;
    str_vec ld_ext;

    str_set stdsyms;

    struct symbols
    {
        using is_serial = int;
        str_vec defined;
        str_vec undefined;
    };

    symbols read_symbols(std::string_view path) const;

    // template<class AR>
    // void serial(AR& ar) {
    //     ar(macos.target,
    //        macos.macho_cputype,
    //        main_symbol,
    //        sharedlib_ext,
    //        staticlib_ext,
    //        sysroot,
    //        libpath,
    //        ld_ext,
    //        stdsyms);
    // }

  private:
    str lookup1(std::string_view name, std::string_view path) const;
    // struct symbols read_symbols(std::string_view content) const;
};
