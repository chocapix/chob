#include <cassert>
#include <functional>

#include "common/error.h"
#include "platform.h"

namespace
{
struct entry
{
    size_t indent;
    std::string_view key;
    vector<std::string_view> value;
};

struct tapi_error
{
    char const* pos;
    std::string desc;

    template<typename... T>
    tapi_error(std::string_view text, fmt::format_string<T...> fmt, T&&... arg)
        : pos{text.data()}
        , desc{fmt::format(fmt, std::forward<T>(arg)...)} {}

    size_t line_number(std::string_view text) {
        size_t count = 1;
        for (char const* ptr = text.data(); ptr < pos; ++ptr)
            count += *ptr == '\n';
        return count;
    }
};

bool is_word(char c) {
    return ('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z')
        || ('0' <= c && c <= '9') || c == '-' || c == '_' || c == '/'
        || c == '.';
}

void skip_to_endl(std::string_view& text) {
    size_t i = text.find('\n');
    if (i != text.npos) text.remove_prefix(i);
}

void skip_space(std::string_view& text) {
    size_t i = text.find_first_not_of(" \t");
    if (i == text.npos) i = text.size();
    text.remove_prefix(i);

    if (text.starts_with("#")) skip_to_endl(text);
}

void skip_spaceln(std::string_view& text) {
    for (;;) {
        size_t i = text.find_first_not_of(" \t\n");
        if (i == text.npos) i = text.size();
        text.remove_prefix(i);

        if (!text.starts_with("#")) break;
        skip_to_endl(text);
    }
}

bool expect_quoted(std::string_view& text, std::string_view& atom) {
    assert(!text.empty() && (text[0] == '\'' || text[0] == '"'));
    char quote = text[0];
    std::string_view curr = text;
    curr.remove_prefix(1);
    size_t end = curr.find('\'');
    if (end == curr.npos) {
        throw tapi_error(text, "unterminated {}", quote);
    }

    atom = curr.substr(0, end);
    curr.remove_prefix(end + 1);
    text = curr;
    return true;
}

bool parse_atom(std::string_view& text, std::string_view& atom) {
    if (text.empty()) return false;

    if (text[0] == '\'' || text[0] == '"') return expect_quoted(text, atom);

    if (is_word(text[0])) {
        size_t i = 1;
        while (i < text.size() && is_word(text[i])) ++i;
        atom = text.substr(0, i);
        text.remove_prefix(i);
        return true;
    }
    return false;
}

bool parse_indent(std::string_view& text, size_t& indent) {
    size_t i = text.find_first_not_of(" -\t");
    if (i == text.npos) i = text.size();

    indent = i;
    text.remove_prefix(i);
    return true;
}

bool parse_list(std::string_view& text, vector<std::string_view>& list) {
    assert(!text.empty() && text[0] == '[');
    std::string_view str = text;
    str.remove_prefix(1);

    skip_spaceln(str);

    while (!str.starts_with("]")) {
        std::string_view atom;
        if (parse_atom(str, atom)) {
            list.push_back(atom);
        } else {
            throw tapi_error(str, "expected string");
        }
        skip_spaceln(str);
        if (str.starts_with(",")) {
            str.remove_prefix(1);
            skip_spaceln(str);
        }
    }
    str.remove_prefix(1);
    text = str;
    return true;
}

bool parse_value(std::string_view& text, vector<std::string_view>& value) {
    if (text.starts_with("[")) return parse_list(text, value);

    std::string_view atom;
    if (!parse_atom(text, atom)) return false;

    value.push_back(atom);
    return true;
}

bool parse_entry(std::string_view& text, entry& ent) {
    std::string_view str = text;

    if (!parse_indent(str, ent.indent)) return false;
    if (!parse_atom(str, ent.key)) return false;
    skip_space(str);
    if (!str.starts_with(":")) throw tapi_error(str, "expected ':'");
    str.remove_prefix(1);
    skip_space(str);
    parse_value(str, ent.value);
    text = str;
    return true;
}

bool parse_docbegin(std::string_view& text) {
    static constexpr std::string_view str = "--- !tapi-tbd";
    if (text.starts_with(str)) {
        text.remove_prefix(str.size());
        return true;
    }
    return false;
}

bool parse_document(std::string_view& text, vector<entry>& doc) {
    if (!parse_docbegin(text)) return false;

    skip_spaceln(text);
    for (;;) {
        if (text.starts_with("...")) break;
        entry ent;
        if (!parse_entry(text, ent)) break;
        doc.push_back(ent);
        skip_to_endl(text);
        if (text.starts_with("\n")) text.remove_prefix(1);
    }

    return true;
}

struct entry_range
{
    entry const* first;
    entry const* last;

    entry const* begin() const { return first; }
    entry const* end() const { return last; }
};

entry_range subnodes(vector<entry> const& document, size_t i) {
    assert(i < document.size());
    size_t indent = document[i].indent;
    ++i;
    size_t end = i;
    while (end < document.size() && document[end].indent > indent) ++end;

    return {&document[i], &document[end]};
}

bool contains(vector<std::string_view> v, std::string_view x) {
    for (auto&& y : v)
        if (x == y) return true;
    return false;
}

void process(vector<entry> const& doc,
             std::string_view target,
             std::function<void(std::string_view, bool)> on_sym) {
    for (size_t i = 0; i < doc.size(); ++i) {
        if (doc[i].indent > 0) continue;
        if (!(doc[i].key == "exports" || (doc[i].key == "reexports"))) continue;

        // fmt::print("{}\n", doc[i].key);
        bool target_match = false;
        for (entry const& ent : subnodes(doc, i)) {
            if (ent.key == "targets")
                target_match = contains(ent.value, target);

            if (target_match
                && (ent.key == "symbols" || ent.key == "weak-symbols"))
                for (auto&& sym : ent.value) on_sym(sym, true);
        }
    }
}

}  // namespace

void tapi_symbols(platform const& pinfo,
                  std::string_view filename,
                  std::string_view content,
                  std::function<void(std::string_view, bool)> on_sym) {
    try {
        std::string_view text = content;
        vector<entry> doc;
        while (parse_document(text, doc)) {
            process(doc, pinfo.macos.target, on_sym);
            doc.clear();
        }
    } catch (tapi_error& err) {
        throw error(
            "{}: parsing tapi on line {}", filename, err.line_number(content));
    }
}
