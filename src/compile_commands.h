#pragma once

#include <memory>

#include <str/map.h>
#include <str/vec.h>

struct compile_commands
{
    struct entry
    {
        str file;
        str directory;
        str_vec arguments;
        str command;
        str output;
    };

  private:
    str qdirectory;
    str filename;
    str_map<entry> entries;

  public:
    compile_commands(std::string_view directory);
    ~compile_commands();

    using iterator = str_map<entry>::const_iterator;
    iterator begin() const { return entries.begin(); }
    iterator end() const { return entries.end(); }

    static str quote(str_view text);
    static str unquote(str_view qtext);

    void store() const;

    void insert(str_vec const& arguments,
                std::string_view file,
                std::string_view output);
};
