#include "configuration.h"

#include <path.h>
#include <str/algo.h>
#include <str/vec.h>
#include <util/file.h>
#include <util/subprocess.h>
#include <util/sys/std.h>
#include <util/timer.h>

#include "common/error.h"
#include "common/fmt.h"
#include "common/print.h"
#include "compile_commands.h"
#include "resource.h"

void configuration::init(std::string_view filename, std::string_view mode) {
    var.init(filename);

    // if (is_shared_lib()) var.load("var cflags:+=:-fPIC");

    str_vec compiler_flags = resource->compiler_flags;

    append(compiler_flags, var.get("cflags", context::mk(mode)));
    append(compiler_flags, var.get("lflags", context::mk(mode)));
    pfm.detect(compiler_flags);
    print(2, "configuration::init OK\n");
}

void variable_def::init(std::string_view text) {
    bool past_eq = false;
    for (auto&& word : split(text, ':')) {
        if (past_eq) {
            values.emplace_back(word);
        } else if (word == "+=") {
            past_eq = true;
        } else {
            ctx += word;
        }
    }
}

bool variable_def::matches(context const& curr) const {
    return ctx.is_subset_of(curr);
}

namespace
{
using def_type = str_map<vector<variable_def>>;

str_vec cargs(str_vec const& args) {
    str_vec vec;
    for (size_t i = 1; i < args.size(); ++i) {
        if (args[i] == "-c" || args[i] == "-o") {
            ++i;
            continue;
        }
        vec.emplace_back(args[i]);
    }
    return vec;
}

str_vec args(compile_commands::entry const& entry) {
    str_vec vec = entry.arguments;
    if (vec.empty())
        vec = vsplit(compile_commands::unquote(entry.command), ' ');
    else
        for (str& arg : vec) arg = compile_commands::unquote(arg);

    return cargs(vec);
}

str_vec diff(str_vec const& from, str_vec const& to) {
    str_vec args;
    auto from_set = str_set(from.begin(), from.end());

    for (auto&& arg : to) {
        if (!from_set.contains(arg)) args.emplace_back(arg);
    }

    return args;
}

str_vec cflags(str_view dir) {
    auto cc = compile_commands(dir);

    str_vec base_cflags;
    str_vec main_cflags;

    for (auto&& entry : cc) {
        if (entry.second.output.ends_with("base.cpp.o\""))
            base_cflags = args(entry.second);
        if (entry.second.output.ends_with("main.cpp.o\""))
            main_cflags = args(entry.second);
    }

    str_vec ret = diff(base_cflags, main_cflags);
    print(2, "cflags: {}\n", ret);
    return ret;
}

str_vec largs(str_view filename) {
    auto mf = mapped_file(filename, "r");
    str_view content = mf.view();
    if (content.ends_with('\n')) content.remove_suffix(1);
    str_vec args = vsplit(content, ' ');

    str_vec vec;
    for (size_t i = 1; i < args.size(); ++i) {
        if (args[i] == "-o") {
            ++i;
            continue;
        }
        if (args[i].ends_with(".cpp.o")) continue;
        vec.emplace_back(args[i]);
    }
    return vec;
}

str_vec lflags(str_view dir) {
    str_vec base = largs(dir / "CMakeFiles/base_exe.dir/link.txt");
    str_vec main = largs(dir / "CMakeFiles/main_exe.dir/link.txt");

    str_vec ret = diff(base, main);
    print(2, "lflags: {}\n", ret);
    return ret;
}
void load_line(def_type& defs, str_view line);

void load_find_package2(def_type& defs,
                        str_vec const& packages,
                        str_view libs) {
    auto dir = tempdir("find_package_XXXXXX");
    {
        auto cmlists = file(dir.path() / "CMakeLists.txt", "w");
        auto print = fmt_defer(cmlists.cfile());

        print(
            "cmake_minimum_required(VERSION 3.19)\n"
            "project(toto LANGUAGES CXX)\n"
            "set (CMAKE_EXPORT_COMPILE_COMMANDS true)\n");
        for (auto&& pkg : packages) print("find_package({})\n", pkg);
        print(
            "add_executable(base_exe base.cpp)\n"
            "add_executable(main_exe main.cpp)\n"
            "target_link_libraries(main_exe {})\n",
            libs);
    }
    print(2,
          "CMakeLists.txt:\n{}\n",
          mapped_file(dir.path() / "CMakeLists.txt", "r").view());

    (void)file(dir.path() / "base.cpp", "w");
    (void)file(dir.path() / "main.cpp", "w");

    auto cmake = subprocess("cmake", "-S", dir.path(), "-B", dir.path());
    auto devnull = file("/dev/null", "w");
    if (max_verbosity <= 0)  //
        cmake.out(devnull);
    cmake.run();

    load_line(defs,
              ::format("cflags:+=:{}", fmt::join(cflags(dir.path()), ":")));
    load_line(defs,
              ::format("lflags:+=:{}", fmt::join(lflags(dir.path()), ":")));
}

void load_line(def_type& defs, str_view line) {
    size_t name_end = line.find(':');
    std::string_view name = line.substr(0, name_end);
    std::string_view def = line.substr(name_end);

    vector<variable_def> empty;
    auto [it, inserted] = defs.emplace(name, empty);
    it->second.emplace_back();
    it->second.back().init(def);
}
}  // namespace

void variables::load(std::string_view text) {
    str_vec packages;
    for (str_view line : split(text, '\n')) {
        auto cmd = [&] { return line.substr(line.find(' ') + 1); };

        if (line.starts_with("var "))  //
            load_line(defs, cmd());

        if (line.starts_with("find_package "))  //
            packages.emplace_back(cmd());
        if (line.starts_with("link_libraries ")) {
            load_find_package2(defs, packages, cmd());
            packages.clear();
        }
    }
}

inline profiler prof_var_get{"variables::get"};
str_vec variables::get(std::string_view name,
                       context const& current_context) const {
    // auto _ = prof_var_get.time();
    str_vec result;
    auto it = defs.find(name);
    if (it == defs.end()) return {};
    for (auto&& var : it->second) {
        if (var.matches(current_context)) {
            result.insert(result.end(), var.values.begin(), var.values.end());
        }
    }
    // deduplicate(result);
    return result;
}

static vector<std::pair<str, str>> get_macros() {
    vector<std::pair<str, str>> macros;

    auto output = file::temp();
    print(2, "get_macros output fd {}\n", output.fileno());

    str_vec args;
    append(
        args, resource->compiler_flags, "-E", "-dM", "-x", "c++", "/dev/null");
    subprocess::status result =
        subprocess(args).outerr(output.cfile()).throw_on_error(false).run();

    auto map = output.map();

    if (!result.success()) {
        ::print(0, "{} \033[31mFAILED\033[0m:\n", fmt::join(args, " "));
        ::print(0, "{}", map.view());
        throw error("subprocess failed");
    }
    // print(0, "compiler macros:\n{}\n", map.view());

    for (auto&& line : split(map.view(), '\n')) {
        if (!line.starts_with("#define ")) continue;
        auto desc = line.substr(8);
        auto i = nfind(desc, ' ');

        auto var = desc.substr(0, i);
        if (i == desc.size()) macros.emplace_back(var, "");
        else macros.emplace_back(var, desc.substr(i + 1));
    }

    return macros;
}

void variables::init(std::string_view filename) {
    auto macros = get_macros();

    auto config = file::temp();
    {
        str fdstr = ::format("{}", config.fileno());

        fmt_buf cmd;
        cmd("source {};", resource->dir / "config_preamble.sh"sv);
        if (path::exists(filename)) cmd("source {};", filename);

        subprocess("bash", "-c", cmd.view())
            .env(macros)
            .env("__fd", fdstr)  //
            .env("__verbose", fmt::format("{}", max_verbosity))  //
            .run();
    }
    load(config.map().view());
}
