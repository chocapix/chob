#pragma once

#include <str/str.h>

#include "subproject.h"

struct installer
{
    main_project const* main;
    str prefix;
    str data;
    str include;

    installer(main_project const* main, std::string_view prefix)
        : main{main}
        , prefix{prefix}
        , data{main->root / "data/share"}
        , include{main->root / "include"}  //
    {}

    void operator()() {
        install_data();
        install_bins();

        if (!main->libs.empty()) {
            install_include();
            install_libs();
            install_pkgconfig();
        }
    }

  private:
    void install_data();
    void install_bins();
    void install_include();
    void install_libs();
    void install_pkgconfig();
};
