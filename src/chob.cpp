#include <sstream>
#include <stdexcept>
#include <thread>

#include <argv.h>
#include <path.h>
#include <str/algo.h>
#include <util/subprocess.h>
#include <util/timer.h>

#include "common/error.h"
#include "common/print.h"
#include "installer.h"
#include "rebuilder/database.h"
#include "rebuilder/rebuilder.h"
#include "resource.h"
#include "sha/sha256.h"
#include "subproject.h"

struct cli : argv
{
    // clang-format off
    flag help                = {this, "  -h, --help        print this help and exit"};
    opt<unsigned> jobs       = {this, "  -j, --jobs=N      run up to N jobs in parallel"};
    opt<std::string> mode    = {this, "  -m, --mode=mode   build mode (release, debug or dev)"};
    flag test                = {this, "  -t, --test        build tests"};
    flag reset               = {this, "  -r, --reset       reset main project"};
    flag reset_all           = {this, "      --reset-all   reset all projects"};
    opt<std::string> install = {this, "  -i, --install=prefix\n"
                                      "                    install to directory"};
    opt<std::string> create  = {this, "  -c, --create=template\n"
                                      "                    create a project from a template"};
    flag verbose             = {this, "  -v, --verbose     increase verbosity level"};
    flag db_dump             = {this, "      --dump        dump database (for debugging only)"};

    opt<std::string> dir  = {this, "  project           project root directory"};
    // clang-format on
};

struct resource_init
{
    [[nodiscard]] resource_init(str_view argv0) {
        assert(resource == 0);
        resource = new struct resource;
        try {
            resource->init(argv0);
        } catch (...) {
            delete resource;
            resource = 0;
            throw error("could not load share/chob/");
        }
    }
    ~resource_init() {
        delete resource;
        resource = 0;
    }
};

namespace
{
template<typename T>
T parse(char const* text) {
    auto ss = std::stringstream(text);
    T val;
    if (ss >> val) return val;
    throw error("failed to parse '{}'", text);
}
}  // namespace

profiler prof_main("main");
int main(int argc, char** argv) try
{
    auto _ = prof_main();
    auto res = resource_init(argv[0]);

    if (argc == 6 && argv[1] == "--configure"sv) {
        max_verbosity = parse<size_t>(argv[2]);
        return main_configure(argv[3], argv[4], argv[5]);
    }

    if (argc >= 4 && argv[1] == "--static-link"sv) {
        max_verbosity = parse<size_t>(argv[2]);
        str prod = argv[3];
        auto objs = str_vec(argv + 4, argv + argc);
        return main_static_link(prod, objs);
    }

    profiler::enable();
    print(2, "SHA256: {}\n", SHA256::impl);
    struct cli cli;
    cli.jobs = std::thread::hardware_concurrency() + 2;
    cli.mode = "release";
    cli.dir = ".";
    cli.parse(argc, argv);
    if (cli.help) {
        cli.print_help(argv[0]);
        return 0;
    }
    max_verbosity = cli.verbose.value;

    if (!cli.create.value.empty()) {
        if (path::exists(cli.dir.value))
            throw error("file exists: {}", cli.dir.value);
        str tmplate = resource->dir / "templates" / cli.create.value;
        if (!path::exists(tmplate))
            throw error("template not found: {}", cli.create.value);

        auto target = path::filename(tmplate);
        path::copy(tmplate, target, path::recursive);

        path::chdir(target);
        subprocess("git", "init").run();

        return 0;
    }

    str root{cli.dir.value};
    if (root.empty())  //
        throw error("Must specify a project root");

    if (!path::exists(root))
        throw error("No such file or directory: '{}'", cli.dir.value);

    if (!path::status(root).is_dir())
        throw error("Not a directory: '{}'", cli.dir.value);

    root = path::realpath(root);
    str build = path::realpath(".");

    if (root == build) {
        build = root / "build";
        path::create_dirs(build);
        path::chdir(build);
        build = path::realpath(build);
    }

    if (cli.reset_all.value) {
        str dir = build / "_chob" / cli.mode.value;
        database::full_reset(dir / "_db");
        // database::srcdeps_reset(dir / "_db");
        return 0;
    }

    if (cli.reset.value) {
        str dir = build / "_chob" / cli.mode.value;
        database::reset_main(dir / "_db", root);
        // database::srcdeps_reset(dir / "_db");
        return 0;
    }

    main_project main_proj{root,  //
                           cli.mode.value,
                           build,
                           cli.jobs.value,
                           cli.test.value != 0};
    if (cli.db_dump.value) {
        main_proj.db.dump();
        return 0;
    }

    main_proj.schedule();
    main_proj.wait();
    main_proj.handle_pending();
    main_proj.create_ctest();

    print(1,
          "bins:  {}\ntests: {}\nlibs:  {}\n",
          main_proj.bins,
          main_proj.tests,
          main_proj.libs);

    auto data_dir = main_proj.root / "data/share";
    if (path::exists(data_dir))
        path::copy(data_dir,
                   main_proj.build_dir / "share",
                   path::overwrite | path::recursive);

    if (!cli.install.value.empty()) {
        str prefix = path::absolute(cli.install.value);
        installer install{&main_proj, prefix};
        install();
    }

} catch (std::exception& err) {
    fmt::print(stderr, "error: {}\n", err.what());
    return 2;
} catch (rebuilder::task_failed) {
    fmt::print(stderr, "error: job failed\n");
    return 2;
} catch (...) {
    fmt::print(stderr, "unknown internal error\n");
    return 2;
}
