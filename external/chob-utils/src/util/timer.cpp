// #include <bit>
// #include <charconv>
#include <cmath>
#include <cassert>
#include <chrono>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string_view>
#include <vector>

#include "util/timer.h"

using namespace std::string_view_literals;
using my_clock = std::chrono::high_resolution_clock;

timer::time_point timer::now() {
    return my_clock::now().time_since_epoch().count();
}

double timer::duration::seconds() const {
    my_clock::duration d{dur};
    return std::chrono::duration<double>(d).count();
}

using u64 = std::uint64_t;
using u32 = std::uint32_t;
using u8 = std::uint8_t;

template<typename T>
static constexpr u64 make(T a0, T a1, T a2, T a3, T a4) {
    return  //
        (u64(u8(a0)) << 0) |  //
        (u64(u8(a1)) << 8) |  //
        (u64(u8(a2)) << 16) |  //
        (u64(u8(a3)) << 24) |  //
        (u64(u8(a4)) << 32);
}
static constexpr u64 zero64 = make('0', '0', '0', '0', '0');
static constexpr u64 dot64 = make('.', '.', '.', '.', '.');

static void fmt10k(double x, char* buf) {
    // clang-format off
    static constexpr double factor[4] = {1000., 100., 10., 1.};

    int index = (x >= 9.995) + (x >= 99.95) + (x >= 999.5);
    auto n = u32(x * factor[index] + 0.5);

    assert(n < 10000u);

    u64 lo = zero64 + make(n / 1000u, n / 100u % 10u, n / 10u % 10u, n % 10u, 0u);
    u64 pt = dot64;
    u64 hi = lo << 8;

    static constexpr u64 lo_mask[4] = {0x00000000ff, 0x000000ffff, 0x0000ffffff, 0x00ffffffff};
    static constexpr u64 pt_mask[4] = {0x000000ff00, 0x0000ff0000, 0x00ff000000, 0xff00000000};
    static constexpr u64 hi_mask[4] = {0xffffff0000, 0xffff000000, 0xff00000000, 0x0000000000};

    lo &= lo_mask[index];
    pt &= pt_mask[index];
    hi &= hi_mask[index];

    u64 txt = lo | pt | hi;

    static_assert(std::endian::native == std::endian::little);
    std::memcpy(buf, &txt, 5);
    // clang-format on
}

static void from_chars(timer::string& text, char const* chars) {
    strncpy(text.buf, chars, sizeof(text.buf));
    text.buf[sizeof(text.buf) - 1] = '\0';
}

timer::string timer::str(double seconds) noexcept {
    timer::string text;

    double t = seconds;

    if (std::signbit(t)) [[unlikely]] {
        from_chars(text, "-xxx s");
        return text;
    }

    if (t < 9999.5) [[likely]] {
        static constexpr double scale[4] = {1e9, 1e6, 1e3, 1.};
        static constexpr char si_prefix[4] = {'n', 'u', 'm', ' '};
        int index = (t >= .995e-6) + (t >= .995e-3) + (t >= .995);
        fmt10k(t * scale[index], text.buf);

        if (t < 0.995) text.buf[4] = ' ';
        text.buf[5] = si_prefix[index];
        text.buf[6] = 's';
        text.buf[7] = '\0';
        return text;
    }

    if (t < 99999.5) [[likely]] {
        u64 n = u64(t + .5);
        snprintf(text.buf, sizeof(text.buf), "%llu s", (unsigned long long)n);
        return text;
    }

    if (t > 999998.) t = 999999.;
    u64 n = u64(t + .5);

    snprintf(text.buf, sizeof(text.buf), "%llus", (unsigned long long)n);
    return text;
}

profiler* profiler::head = 0;
static bool enabled = false;

void profiler::enable(bool val) { enabled = val; }

struct cmp_total
{
    bool operator()(profiler const* a, profiler const* b) const {
        return a->duration.dur > b->duration.dur;
    }
};
struct cmp_per_call
{
    bool operator()(profiler const* a, profiler const* b) const {
        double as = a->duration.seconds() / double(a->count);
        double bs = b->duration.seconds() / double(b->count);
        return as > bs;
    }
};

static void print(profiler const* p) {
    if (p->count == 0) return;
    double per_call = p->duration.seconds() / double(p->count);
    std::printf("%s, %s/call, %8lu calls: %s\n",
                p->duration.str().buf,
                timer::str(per_call).buf,
                p->count,
                p->name);
}

profiler::~profiler() {
    if (!enabled) return;
    if (this != head) return;

    char* env = getenv("CHOB_PROFILE");
    if (env == 0) return;

    std::vector<profiler*> profs;
    for (profiler* prof = head; prof; prof = prof->next) profs.push_back(prof);

    if (env == "per_call"sv)
        std::sort(profs.begin(), profs.end(), cmp_per_call());
    else std::sort(profs.begin(), profs.end(), cmp_total());

    for (auto&& prof : profs) print(prof);
}
