#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <system_error>

#include <dirent.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "util/sys/posix.h"
#include "util/sys/std.h"
#include "util/timer.h"

namespace
{
[[noreturn]] void sys_error(std::string const& msg) {
    throw std::system_error(errno, std::generic_category(), msg);
}

std::string func(char const* call_expr) {
    while (*call_expr == ':') ++call_expr;

    for (char const* ptr = call_expr; *ptr; ++ptr)
        if (*ptr == '(') return {call_expr, size_t(ptr - call_expr)};
    return "?";
}

#define call(test, expr)                                                       \
    do {                                                                       \
        auto r = ::expr;                                                       \
        if (test) return r;                                                    \
        sys_error(func(#expr));                                                \
    } while (0);

}  // namespace

namespace sys
{

// posix.h

profiler prof_open{"sys::open"};
int open(const char* pathname, int flags, mode_t mode) {
    auto _ = prof_open();
    call(r >= 0, open(pathname, flags, mode));
}
profiler prof_close{"sys::close"};
int close(int filedes) {
    auto _ = prof_close();
    call(r >= 0, close(filedes));
}
off_t lseek(int fildes, off_t offset, int whence) {
    call(r >= 0, lseek(fildes, offset, whence));
}
int lstat(char const* path, struct stat *buf)
{
    call(r >= 0, lstat(path, buf));
}
int chdir(char const* path) { call(r >= 0, chdir(path)); }
int ftruncate(int filedes, off_t length) {
    call(r >= 0, ftruncate(filedes, length));
}
profiler prof_mmap{"sys::mmap"};
void* mmap(void* addr, size_t len, int prot, int flags, int fd, off_t offset) {
    auto _ = prof_mmap();
    call(r != (void*)-1, mmap(addr, len, prot, flags, fd, offset));
}
profiler prof_munmap{"sys::munmap"};
int munmap(void* ptr, size_t len) {
    auto _ = prof_munmap();
    call(r >= 0, munmap(ptr, len));
}

int mkstemp(char* tmplate) { call(r >= 0, mkstemp(tmplate)); }
char* mkdtemp(char* tmplate) { call(r != 0, mkdtemp(tmplate)); }

int unlink(char const* path) { call(r >= 0, unlink(path)); }
int dup2(int oldfd, int newfd) { call(r >= 0, dup2(oldfd, newfd)); }
int wait4(pid_t pid, int* stat_loc, int options, struct rusage* rusage) {
    call(r >= 0, wait4(pid, stat_loc, options, rusage));
}
pid_t fork() { call(r >= 0, fork()); }
int execvp(char const* path, char* const argv[]) {
    call(r >= 0, execvp(path, argv));
}
int execve(char const* path, char* const argv[], char* const envp[]) {
    call(r >= 0, execve(path, argv, envp));
}

// std.h

profiler prof_fopen{"sys::fopen"};
FILE* fopen(char const* path, char const* mode) {
    auto _ = prof_fopen();
    auto r = ::fopen(path, mode);
    if (r != 0) return r;
    using namespace std::string_literals;
    sys_error("fopen: '"s + path + "'"s);
    // call(r != 0, fopen(path, mode));
}
profiler prof_fclose{"sys::fclose"};
int fclose(FILE* stream) {
    auto _ = prof_fclose();
    call(r == 0, fclose(stream));
}
FILE* fdopen(int filedes, char const* mode) {
    call(r != 0, fdopen(filedes, mode));
}

size_t fwrite(void const* ptr, size_t size, size_t nitems, FILE* stream) {
    call(r > 0, fwrite(ptr, size, nitems, stream));
}
long ftell(FILE* stream) { call(r >= 0, ftell(stream)); }
int fseek(FILE* stream, long offset, int whence) {
    call(r >= 0, fseek(stream, offset, whence));
}
FILE* tmpfile(void) { call(r != 0, tmpfile()); }

int closedir(DIR* dirp) { call(r >= 0, closedir(dirp)); }
DIR* opendir(const char* filename) { call(r != 0, opendir(filename)); }
struct dirent* readdir(DIR* dirp)
{
    errno = 0;
    struct dirent* entry = ::readdir(dirp);
    if (entry != 0 || errno == 0) return entry;
    throw std::system_error(errno, std::generic_category(), "readdir");
}

char* realpath(char const* file_name, char* resolved_name) {
    call(r != 0, realpath(file_name, resolved_name));
}
}  // namespace sys
