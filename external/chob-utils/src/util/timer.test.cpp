#include <cstring>
#include <iostream>
#include <sstream>

#include "util/timer.h"

struct test_case
{
    double x;
    char const* expected;
};

test_case tab[] = {
    {1.23457e-12, "0.00 ns"}, {1.23457e-11, "0.01 ns"},
    {1.23457e-10, "0.12 ns"}, {1.23457e-09, "1.23 ns"},
    {1.23457e-08, "12.3 ns"}, {1.23457e-07, "123. ns"},
    {1.23457e-06, "1.23 us"}, {1.23457e-05, "12.3 us"},
    {0.000123457, "123. us"}, {0.00123457, "1.23 ms"},
    {0.0123457, "12.3 ms"},   {0.123457, "123. ms"},
    {1.23457, "1.235 s"},     {12.3457, "12.35 s"},
    {123.457, "123.5 s"},     {1234.57, "1235. s"},
    {12345.7, "12346 s"},     {123457, "123457s"},
    {1.23457e+06, "999999s"}, {0., 0},
};

template<typename T>
T parse(char const* text) {
    std::stringstream ss(text);
    T val;
    if (ss >> val)
        return val;
    throw 42;
}

profiler prof_toto {"toto"};
profiler prof_titi {"titi"};
int main(int argc, char** argv) {
    auto _ = prof_toto();
    profiler::enable();
    for (int i = 1; i < argc; ++i) {
        std::cout << timer::str(parse<double>(argv[i])) << "\n";
    }

    for (test_case* ptr = tab; ptr->expected; ptr++) {
        auto _ = prof_titi();
        // size_t n = strlen(ptr->expected);
        std::string actual = timer::str(ptr->x).buf;
        if (actual != ptr->expected) {
            // strncmp(actual, ptr->expected, n) != 0) {
            std::cerr << "error " << ptr->x << " expected " << ptr->expected
                      << " actual " << actual << "\n";
            return 2;
        }
    }
}
