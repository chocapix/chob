#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <new>

#include "util/allocator.h"

#include <sys/mman.h>
#include <unistd.h>

char* allocator_impl::base;
std::size_t allocator_impl::index;
std::size_t allocator_impl::capacity;

static std::size_t pagesize;
static std::size_t total_reserved;
static bool verbose;

void allocator_impl::grow(size_type bytes) {
    if (pagesize == 0) {
        pagesize = (size_t)getpagesize();
        verbose = getenv("VERBOSE_ALLOCATOR");
    }

    capacity = capacity * 2 + pagesize;

    if (capacity < bytes) capacity = align(bytes, pagesize);

    assert(bytes <= capacity);

    if (verbose) std::printf("reserving %lu bytes\n", capacity);
    void* ptr = mmap(
        0, capacity, PROT_READ | PROT_WRITE, MAP_ANON | MAP_PRIVATE, -1, 0);

    if (ptr == (void*)-1) throw std::bad_alloc{};
    total_reserved += capacity;
    base = (char*)ptr;
    index = 0;

    
}

static std::size_t total_allocated;
static std::size_t total_deallocated;

void* allocator_impl::debug_alloc(size_type bytes, size_type alignment) {
    index = align(index, alignment);
    if (index + bytes > capacity) [[unlikely]]
        grow(bytes);

    char* ptr = base + index;
    index += bytes;
    total_allocated += bytes;
    return ptr;
}

void allocator_impl::debug_dealloc(void* ptr, size_type bytes) {
    std::memset(ptr, 0xef, bytes);
    total_deallocated += bytes;
}

void allocator_impl::print_stats() {
    std::printf("reserved %lu allocated %lu freed %lu\n",
                total_reserved,
                total_allocated,
                total_deallocated);
}

static struct end_check
{
    ~end_check() {
        if (total_allocated > total_deallocated) {
            std::fprintf(stderr,
                         "\033[35mwarning\033[m: leaked %lu bytes\n",
                         total_allocated - total_deallocated);
            return;
        }
        if (total_allocated < total_deallocated) {
            std::fprintf(
                stderr,
                "\033[35mwarning\033[m: deallocatted extra %lu bytes\n",
                total_allocated - total_deallocated);
            return;
        }

        if (verbose)
            std::printf("allocated %lu bytes, reserved %lu\n",
                        total_allocated,
                        total_reserved);
    }
} end_check;
