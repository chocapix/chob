#include <stdexcept>
#include <string>
#include <vector>

#include "util/subprocess.h"

#include "testing.h"

using namespace std::string_view_literals;

TEST_CASE("true") { subprocess("true").run(); }

TEST_CASE("false") {
    try {
        subprocess("false").run();
        REQUIRE(false);
    } catch (std::runtime_error& err) {
        REQUIRE(err.what() == "subprocess failed"sv);
    }
}

int main(int argc, char** argv) {
    std::vector<std::string> args(argv + 1, argv + argc);
    if (args.empty()) return testing::main(argc, argv);

    subprocess(args).run();
}
