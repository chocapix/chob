#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

#include "util/allocator.h"

#include "testing.h"

extern char const* test_strings[];

using string = std::basic_string<char, std::char_traits<char>, allocator<char>>;
using str_vec = std::vector<string, allocator<string>>;

TEST_CASE("one word") {
    str_vec vec;
    vec.emplace_back("toto");
    REQUIRE(vec[0] == "toto");
}

TEST_CASE("test_strings") {
    str_vec vec;
    for (size_t i = 0; test_strings[i]; ++i) vec.emplace_back(test_strings[i]);
    for (size_t i = 0; test_strings[i]; ++i) REQUIRE(vec[i] == test_strings[i]);
}

int main(int argc, char** argv) { return testing::main(argc, argv); }
