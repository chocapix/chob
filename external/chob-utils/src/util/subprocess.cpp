#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <string>
#include <vector>

#include "util/subprocess.h"

#include <sys/wait.h>

#include "path.h"
#include "util/str_view.h"
#include "util/sys/posix.h"

extern char** environ;

bool subprocess::status::success() const {
    return WIFEXITED(status) && WEXITSTATUS(status) == 0;
}

subprocess::status subprocess::wait4(pid_t pid, int options) {
    subprocess::status stat;
    stat.pid = sys::wait4(pid, &stat.status, options, &stat.ru);
    return stat;
}

struct subprocess::impl
{
    struct kv_pair
    {
        std::string key, val;
        kv_pair(std::string_view key, std::string_view val)
            : key(key)
            , val(val) {}
    };

    std::string path;
    std::vector<std::string> argv;
    std::vector<kv_pair> env;

    FILE* out = 0;
    FILE* err = 0;
    bool throw_on_error = true;

    static void dup2(FILE* stream, int fd) {
        if (stream == 0) return;
        sys::dup2(fileno(stream), fd);
    }

    pid_t detach() {
        if (argv.empty()) throw std::runtime_error("subprocess: empty argv");

        pid_t pid = sys::fork();
        if (pid > 0) return pid;

        if (path.empty()) path = ::path::lookup(argv[0]);
        // if (path.empty()) throw std::runtime_error("subprocess: empty path");

        std::vector<char*> cargv;
        for (auto&& arg : argv) cargv.push_back(arg.data());
        cargv.push_back(0);
        for (auto&& [k, v] : env) setenv(k.data(), v.data(), 1);

        // char *exe_path = path.data();
        // std::cerr << "exe_path: " << (exe_path ? exe_path : "(null)") << "\n";
        //
        // char **exe_argv = cargv.data();
        // std::cerr << "exe_argv: " << (void*)exe_argv << "\n";
        // if (exe_argv) {
        //     for (char **ptr = exe_argv; *ptr; ++ptr) {
        //
        //     }
        // }
        // char **exe_env = environ;



        // sys::close(0);
        dup2(out, 1);
        dup2(err, 2);

        sys::execve(path.data(), cargv.data(), environ);
        return -1;
    }

    status run() {
        pid_t pid = detach();
        status stat = wait4(pid);
        if (throw_on_error && !stat.success())
            throw std::runtime_error("subprocess failed");
        return stat;
    }
};

subprocess::subprocess()
    : pimpl(new impl) {}
subprocess::~subprocess() {}

subprocess& subprocess::env(std::string_view key, std::string_view value) {
    pimpl->env.emplace_back(key, value);
    return *this;
}
subprocess& subprocess::out(FILE* stream) {
    pimpl->out = stream;
    return *this;
}
subprocess& subprocess::err(FILE* stream) {
    pimpl->err = stream;
    return *this;
}
subprocess& subprocess::throw_on_error(bool value) {
    pimpl->throw_on_error = value;
    return *this;
}

subprocess::status subprocess::run() { return pimpl->run(); }
pid_t subprocess::detach() { return pimpl->detach(); }

void subprocess::add_argv(std::string_view arg) {
    pimpl->argv.emplace_back(arg);
}
