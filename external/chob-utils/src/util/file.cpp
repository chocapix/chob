#include <filesystem>
#include <iostream>
#include <stdexcept>
#include <string_view>

#include "util/file.h"

#include <sys/fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#include "util/sys/posix.h"
#include "util/sys/std.h"

prot_flags::prot_flags(char const* fmode)
    : prot(0)
    , flags(0)  //
{
    using namespace std::string_literals;
    switch (fmode[0]) {
        case 'r':
            prot |= PROT_READ;
            flags = O_RDONLY;
            break;
        case 'w':
        case 'a':
            prot |= PROT_WRITE;
            flags = O_WRONLY;
            break;
        default: throw std::runtime_error("invalid file open mode: "s + fmode);
    }
    switch (fmode[1]) {
        case '\0': return;
        case '+':
            flags = O_RDWR;
            prot = PROT_READ | PROT_WRITE;
            break;
        default: throw std::runtime_error("invalid file open mode: "s + fmode);
    }
    if (fmode[2] != '\0')
        throw std::runtime_error("invalid file open mode: "s + fmode);
}

// file_mapping

file_mapping::file_mapping(int fd,
                           char const* mode,
                           size_t len,
                           std::string_view namev)
    : name(namev)
    , prot(prot_flags(mode).prot) {
    map(fd, len);
}

void file_mapping::unmap() { sys::munmap(ptr, len + 1); }

void file_mapping::map(int fd, size_t new_size) {
    len = new_size;
    ptr = sys::mmap(0, len + 1, prot, MAP_SHARED, fd, 0);
}

// ffile

file::file(std::string_view namev, char const* mode)
    : name(namev)
    , mode(mode)
    , stream(sys::fopen(name.c_str(), mode))
    , unlink_on_close(false)
//
{
    // std::cerr << getpid() << " " << this << ":  file '" << name << "' fd "
    //           << (stream ? fileno() : -1) << (unlink_on_close ? " unlink" :
    //           "")
    //           << "\n";
}
file::file(FILE* stream)
    : name()
    , mode("w+")
    , stream(stream)
    , unlink_on_close(false)  //
{
    // std::cerr << getpid() << " " << this << ":  file '" << name << "' fd "
    // << (stream ? fileno() : -1) << (unlink_on_close ? " unlink" : "")
    // << "\n";
}
file::file(temp_t, std::string_view tmplatev)
    : name(tmplatev)
    , mode("w+")
    , unlink_on_close(true)
//
{
    int fd = sys::mkstemp(name.data());
    stream = sys::fdopen(fd, mode);
    // std::cerr << getpid() << " " << this << ":  file '" << name << "' fd "
    // << (stream ? fileno() : -1) << (unlink_on_close ? " unlink" : "")
    // << "\n";
}
file file::temp() { return {sys::tmpfile()}; }

file::~file() {
    // std::cerr << getpid() << " " << this << ": ~file '" << name << "' fd "
    // << (stream ? fileno() : -1) << (unlink_on_close ? " unlink" : "")
    // << "\n";
    if (unlink_on_close) sys::unlink(name.data());
    close();
}

void file::close() {
    if (stream != 0) {
        // std::cerr << getpid() << ": close '" << name << "' fd "
        // << (stream ? fileno() : -1)
        // << (unlink_on_close ? " temp" : "") << "\n";
        sys::fclose(stream);
    }
}

int file::fileno() const { return ::fileno(stream); }

size_t file::write(void const* ptr, size_t size, size_t nitems) {
    return sys::fwrite(ptr, size, nitems, stream);
}

size_t file::size() const {
    long prev = sys::ftell(stream);
    sys::fseek(stream, 0, SEEK_END);
    long end = sys::ftell(stream);
    sys::fseek(stream, prev, SEEK_SET);

    // std::cout << "size " << name << ": " << end << "\n";

    return size_t(end);
}

file_mapping file::map() const { return {fileno(), mode, size(), name}; }

// mapped_file

mapped_file::mapped_file(std::string_view namev, char const* mode)
    : name{namev} {
    auto pf = prot_flags(mode);
    prot = pf.prot;
    fd = sys::open(name.c_str(), O_CREAT | pf.flags, 0644);
    off_t end = sys::lseek(fd, 0, SEEK_END);
    len = (size_t)end;
    ptr = sys::mmap(0, len + 1, prot, MAP_SHARED, fd, 0);
}
mapped_file::~mapped_file() {
    sys::munmap(ptr, len + 1);
    sys::close(fd);
}

void mapped_file::resize(size_t new_size) {
    sys::munmap(ptr, len + 1);
    sys::ftruncate(fd, (off_t)new_size);
    len = new_size;
    ptr = sys::mmap(0, len + 1, prot, MAP_SHARED, fd, 0);
}

tempdir::tempdir(std::string_view namev)
    : name(namev) {
    sys::mkdtemp(name.data());
    std::filesystem::create_directory(name);
}

tempdir::~tempdir() { std::filesystem::remove_all(name); }
