#include <stdexcept>
#include <vector>

#include "serial.h"

#include "str/map.h"
#include "str/set.h"
#include "testing.h"

TEST_CASE("simple") {
    int input = 42;
    serial::serializer ser;
    ser.write(input);

    serial::deserializer des;
    des.buf = ser.buf;

    int output;
    des.read(output);

    REQUIRE(input == output);
}

TEST_CASE("to_str") {
    int input = 42;
    str bytes = serial::to_str(input);
    auto output = serial::from_str<int>(bytes);

    REQUIRE(input == output);
}

struct pouet
{
    using is_serial = void;
    int i;
    double d;
    auto operator<=>(pouet const&) const = default;
};

TEST_CASE("aggregate") {
    pouet input{42, 69.};
    str bytes = serial::to_str(input);
    auto output = serial::from_str<pouet>(bytes);
    REQUIRE(input == output);
}

struct pouet2 {
    using is_serial = int;

    // pouet2() : p{42, 69.}, i(666) {}

    std::vector<int> v;
    pouet p;
    int i;
    auto operator<=>(pouet2 const&) const = default;
};

TEST_CASE("aggregate2") {
    pouet2 input {{1, 2}, {64, 69.}, 42};
    str bytes = serial::to_str(input);
    auto output = serial::from_str<pouet2>(bytes);
    REQUIRE(input == output);
}

TEST_CASE("vector") {
    std::vector<int> input{1, 2, 3};
    str bytes = serial::to_str(input);
    auto output = serial::from_str<std::vector<int>>(bytes);
    REQUIRE(input == output);
}

TEST_CASE("corrupt") {
    std::vector<int> input{1, 2, 3};
    str bytes = serial::to_str(input);
    bytes = str_view(bytes).substr(0, bytes.size() / 2);

    try {
        auto output = serial::from_str<std::vector<int>>(bytes);
        REQUIRE(false);
    } catch (std::runtime_error& err) {
        REQUIRE(err.what() == "deserialize: unexpected end of input"sv);
    }
}

bool equals(str_set const& a, str_set const& b) {
    for (auto&& x : a)
        if (!b.contains(x)) return false;
    for (auto&& x : b)
        if (!a.contains(x)) return false;
    return true;
}

TEST_CASE("str_set") {
    str_set input{"un", "deux", "trois"};
    str bytes = serial::to_str(input);
    auto output = serial::from_str<str_set>(bytes);
    REQUIRE(equals(input, output));
}

template<typename V>
bool is_submap(str_map<V> const& a, str_map<V> const& b) {
    for (auto&& [k, v] : a) {
        auto it = b.find(k);
        if (it == b.end()) return false;
        if (it->second != v) return false;
    }
    return true;
}
template<typename V>
bool equals(str_map<V> const& a, str_map<V> const& b) {
    return is_submap(a, b) && is_submap(b, a);
}

TEST_CASE("str_map") {
    str_map<int> input{{"un", 1}, {"deux", 2}, {"trois", 3}};
    str bytes = serial::to_str(input);
    auto output = serial::from_str<str_map<int>>(bytes);
    REQUIRE(equals(input, output));
}

int main(int argc, char** argv) { return testing::main(argc, argv); }
