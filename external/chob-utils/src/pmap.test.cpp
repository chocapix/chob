#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <random>
#include <sstream>
#include <stdexcept>

#include "pmap.h"

#include <unistd.h>

#include "testing.h"
#include "util/file.h"
#include "util/timer.h"
#include "util/wyhash++.h"

using namespace std::string_literals;

template<typename T>
using str_map = std::unordered_map<std::string, T>;

using u64 = std::uint64_t;

struct tempfile
{
    std::string filename;
    int fd;

    tempfile(std::string_view filename_v)
        : filename(filename_v) {
        fd = mkstemp(filename.data());
        if (fd < 0)
            throw std::runtime_error("pmap.test: mkstemp: "s + strerror(errno));
    }
    ~tempfile() {
        int r = close(fd);
        if (r < 0) std::cerr << "pmap.test: close: " << strerror(errno) << "\n";
        r = unlink(filename.c_str());
        if (r < 0)
            std::cerr << "pmap.test: unline: " << strerror(errno) << "\n";
    }
};

TEST_CASE("mapped_file") {
    tempfile file{"./pmap2-test-XXXXXX"};

    {
        mapped_file map{file.filename, "r+"};
        REQUIRE(map.size() == 0);
        map.resize(42);
        REQUIRE(map.size() == 42);
    }
    {
        mapped_file map{file.filename, "r+"};
        REQUIRE(map.size() == 42);
    }
}

struct checked_map
{
    str_map<std::string> expected;
    std::unique_ptr<pmap> actual;

    checked_map(std::string_view filename)
        : expected{}
        , actual{new pmap{filename, pmap::do_check{}}} {
        for (auto&& [k, v] : *actual) expected.emplace(k, v);

        require_equal();
    }

    void reload() {
        std::string filename{actual->filename()};
        actual.reset();
        actual.reset(new pmap{filename});

        require_equal();
    }

    void put(std::string const& key, std::string_view val) {
        expected.insert_or_assign(key, val);
        actual->put(key, val);

        require_equal();
    }

    void require_equal() const {
        REQUIRE(expected.size() == actual->size());
        for (auto&& [k, v] : *actual) {
            auto it = expected.find(std::string(k));
            REQUIRE(it != expected.end());
            REQUIRE(it->second == v);
        }

        for (auto&& [k, v] : expected) {
            std::string_view val = actual->get(k);
            REQUIRE(val.data() != 0);
            REQUIRE(val == v);
        }
    }
};

TEST_CASE("simple") {
    tempfile file{"./pmap2-test-XXXXXX"};
    checked_map map{file.filename};

    map.put("alice", "1");
    map.reload();
}

template<typename T>
std::string to_str(T const& value) {
    std::stringstream ss;
    ss << value;
    return ss.str();
}

TEST_CASE("one hundred") {
    tempfile file{"./pmap2-test-XXXXXX"};
    checked_map map{file.filename};

    for (int i = 1; i < 100; ++i) {
        if (i % 15 == 0) map.reload();
        map.put(to_str(i), to_str(i + 1));
    }
}

TEST_CASE("collect") {
    tempfile file{"./pmap2-test-XXXXXX"};
    checked_map map{file.filename};

    wyhash::rand rng{42};
    auto rsize = [&]
    { return std::uniform_int_distribution<u64>(0, 100)(rng); };
    auto rchar = [&] {
        return char(
            std::uniform_int_distribution<unsigned char>('a', 'z')(rng));
    };
    auto rstring = [&]
    {
        std::string str;
        for (u64 i = 0, end = rsize(); i < end; ++i) {
            str += rchar();
        }
        return str;
    };

    std::vector<std::string> keys  //
        {"zero", "un", "deux", "trois", "quatre", "cinq", "six"};

    for (unsigned i = 0; i < 200; ++i) {
        auto key = keys[i % keys.size()];
        auto val = rstring();
        map.put(key, val);
    }
}

TEST_CASE("constant size put time") {
    tempfile file{"./pmap2-test-XXXXXX"};
    pmap map{file.filename};

    wyhash::rand rng{42};
    auto rsize = [&]
    { return std::uniform_int_distribution<u64>(0, 100)(rng); };
    auto rchar = [&] {
        return char(
            std::uniform_int_distribution<unsigned char>('a', 'z')(rng));
    };
    auto rstring = [&]
    {
        std::string str;
        for (u64 i = 0, end = rsize(); i < end; ++i) {
            str += rchar();
        }
        return str;
    };

    const u64 num_keys = 128;
    const u64 num_puts = 10000;
    std::string_view val = "toto";

    std::vector<std::string> keys;
    for (u64 i = 0; i < num_keys; ++i) {
        keys.push_back(rstring());
        map.put(keys.back(), val);
    }

    timer tim;
    for (u64 i = 0; i < num_puts; ++i) map.put(keys[i % num_keys], val);
    auto time = tim.elapsed();

    std::cout << "pmap::put time: " << time.str() << ", "
              << timer::str(time.seconds() / num_puts) << " per call\n";
}

TEST_CASE("big") {
    tempfile file{"./pmap2-test-XXXXXX"};
    checked_map map{file.filename};

    wyhash::rand rng{42};
    auto rsize = [&]
    { return std::uniform_int_distribution<u64>(0, 100)(rng); };
    auto rchar = [&] {
        return char(
            std::uniform_int_distribution<unsigned char>('a', 'z')(rng));
    };
    auto rstring = [&]
    {
        std::string str;
        for (u64 i = 0, end = rsize(); i < end; ++i) {
            if (i == 0) str += '(';
            else if (i == end - 1) str += ')';
            else str += rchar();
        }
        return str;
    };

    for (unsigned i = 0; i < 2000; ++i) {
        map.put(rstring(), rstring());
    }
}

int test(int argc, char** argv) {
    (void)argc, (void)argv;

    return testing::main(argc, argv);
}

int main(int argc, char** argv) {
    if (argc == 1) return test(argc, argv);

    pmap map{argv[1], pmap::do_check{}};

    for (auto&& [k, v] : map) (void)k, (void)v;

    for (int i = 2; i < argc; ++i) {
        std::string_view op = argv[i];
        if (op == "put" && i + 2 < argc) {
            map.put(argv[i + 1], argv[i + 2]);
            i += 2;
        } else if (op == "get" && i + 1 < argc) {
            auto v = map.get(argv[i + 1]);
            std::cout << argv[i + 1] << ": " << v << "\n";
            i += 1;
        } else if (op == "iter") {
            for (auto&& [k, v] : map) {
                std::cout << k << ": " << v << "\n";
            }
        } else {
            throw std::runtime_error("no op: {}"s + std::string(op));
        }
    }
}
