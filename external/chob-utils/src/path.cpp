
#include <cassert>
#include <filesystem>

#include "path.h"

#include <dirent.h>
#include <sys/param.h>
#include <unistd.h>

#include "str/algo.h"
#include "util/sys/posix.h"
#include "util/sys/std.h"

struct dirent_iterator
{
    DIR* dir;

    struct iterator
    {
        dirent_iterator* di;
        dirent* entry;
        using difference_type = ptrdiff_t;
        using value_type = dirent;

        iterator& operator++() {
            assert(entry != 0);
            entry = sys::readdir(di->dir);
            return *this;
        }
        iterator operator++(int) {
            iterator copy{*this};
            ++*this;
            return copy;
        }
        dirent const& operator*() const { return *entry; }

        bool operator==(iterator const& other) const {
            return entry == other.entry;
        }
        bool operator!=(iterator const& other) const {
            return !(*this == other);
        }
    };

    dirent_iterator(str const& path) { dir = sys::opendir(path.c_str()); }
    ~dirent_iterator() { ::closedir(dir); }

    iterator begin() { return {this, sys::readdir(dir)}; }
    iterator end() { return {this, 0}; }
};

static void files(str_vec& list,
                  str_view path,
                  path::options opt = path::no_options) {
    for (auto&& entry : dirent_iterator(str{path})) {
        if (entry.d_name == "."sv || entry.d_name == ".."sv) continue;
        str file = path / entry.d_name;
        list.push_back(file);
        if ((opt & path::recursive) && (entry.d_type & DT_DIR))
            files(list, file, opt);
    }
}

static void directories(str_vec& list,
                        str_view path,
                        path::options opt = path::no_options) {
    for (auto&& entry : dirent_iterator(str{path})) {
        if (entry.d_name == "."sv || entry.d_name == ".."sv) continue;
        str file = path / entry.d_name;
        if (entry.d_type & DT_DIR) list.push_back(file);
        if ((opt & path::recursive) && (entry.d_type & DT_DIR))
            files(list, file, opt);
    }
}
namespace path
{

str_view relative_to(str_view full, str_view base) {
    if (!is_prefix(base, full)) throw std::runtime_error("not a prefix");
    if (full.size() == base.size()) return ".";
    str_view r = full.substr(base.size() + 1);
    path_assert(r == fs::path(full).lexically_proximate(base));
    return r;
}

str current() {
    char buf[MAXPATHLEN + 1];
    char* ptr = getcwd(buf, MAXPATHLEN);
    path_assert(ptr == fs::current_path());
    (void)ptr;
    return buf;
}

void chdir(str_view path) {
    str spath{path};
    sys::chdir(spath.c_str());
}

str absolute(str_view path) {
    if (is_absolute(path)) return str{path};

    char buf[MAXPATHLEN + 1];
    char* ptr = getcwd(buf, MAXPATHLEN);
    assert(ptr != 0), (void)ptr;

    str_view cwd = buf;
    str r = make(cwd, path);
    path_assert(str_view{r} == fs::absolute(path));
    return r;
}

str_vec files(str_view dir, options opt) {
    str_vec list;
    ::files(list, dir, opt);
    return list;
}

str_vec directories(str_view dir, options opt) {
    str_vec list;
    ::directories(list, dir, opt);
    return list;
}

void remove(str_view path) { std::filesystem::remove(path); }
void remove_all(str_view path) { std::filesystem::remove_all(path); }

void create_symlink(str_view target, str_view link, options opt) {
    using namespace std::filesystem;
    auto stat = symlink_status(link);
    bool exists = status_known(stat) && stat.type() != file_type::not_found;
    bool is_dir = status_known(stat) && stat.type() == file_type::directory;

    str_view link_dir = is_dir ? link : parent(link);
    str real_link = is_dir ? link_dir / filename(target) : str{link};

    if (opt & relative) {
        target = relative_to(target, link_dir);
    }

    // if ((opt & overwrite) && exists) {
    //     remove(real_link);
    // }

    if (exists) {
        if (opt & overwrite) remove(real_link);
        else return;
    }

    std::filesystem::create_symlink(target, str_view{real_link});
}

void create_dir(str_view path) { std::filesystem::create_directory(path); }

void create_dirs(str_view dir) { std::filesystem::create_directories(dir); }

void copy(str_view from, str_view to, options opt) {
    std::filesystem::copy_options fsopt = std::filesystem::copy_options::none;
    if (opt & recursive) fsopt |= std::filesystem::copy_options::recursive;
    if (opt & overwrite)
        fsopt |= std::filesystem::copy_options::overwrite_existing;

    std::filesystem::copy(from, to, fsopt);
}

static bool exists_no_follow(std::filesystem::path const& path) {
    auto stat = std::filesystem::symlink_status(path);
    return std::filesystem::status_known(stat)
        && stat.type() != std::filesystem::file_type::not_found;
}

bool exists(str_view path, options opt) {
    std::filesystem::path fspath{path};
    if (opt & no_follow) return exists_no_follow(fspath);
    else return std::filesystem::exists(fspath);
}

struct stat status(str_view path)
{
    struct stat st;
    sys::lstat(str{path}.c_str(), &st);
    return st;
}

str realpath(str_view path) {
    char buf[PATH_MAX];
    str spath{path};
    sys::realpath(spath.c_str(), buf);
    return str{buf};
}

str lookup(std::string_view name) {
    char const* env = std::getenv("PATH");
    if (env == 0) return str{name};

    str full;
    for (str_view dir : split(env, ':')) {
        full = dir;
        full.push_back('/');
        full.append(name);
        if (path::exists(full)) return full;
    }

    return str{name};
}
}  // namespace path
