#include <bit>
#include <cstdint>
#include <cstring>

#include "sha/sha256.h"

#include "util/timer.h"
#include "util/types.h"

using u32 = std::uint32_t;
using u64 = std::uint64_t;

static u64 to_be(u64 x) {
    if constexpr (std::endian::native == std::endian::big) return x;
    else return __builtin_bswap64(x);
}
static u32 to_be(u32 x) {
    if constexpr (std::endian::native == std::endian::big) return x;
    else return __builtin_bswap32(x);
}

u32 SHA256::K[64] = {
    0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1,
    0x923F82A4, 0xAB1C5ED5, 0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3,
    0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174, 0xE49B69C1, 0xEFBE4786,
    0x0FC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA,
    0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147,
    0x06CA6351, 0x14292967, 0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13,
    0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85, 0xA2BFE8A1, 0xA81A664B,
    0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070,
    0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A,
    0x5B9CCA4F, 0x682E6FF3, 0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208,
    0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2,
};

SHA256::state SHA256::initial_hash = {
    {0x6a09e667,
     0xbb67ae85,
     0x3c6ef372,
     0xa54ff53a,
     0x510e527f,
     0x9b05688c,
     0x1f83d9ab,
     0x5be0cd19},
};

inline profiler prof_sha256{"sha256"};
str SHA256::operator()(std::string_view sv) {
    auto _ = prof_sha256();
    reset();
    insert(sv.data(), sv.size());
    finalize();
    return get();
}

SHA256& SHA256::insert(const void* data, size_t size) {
    const char* bytes = (const char*)data;
    length += size;
    if (buf_size + size < 64) [[likely]] {
        std::memcpy(buf.t + buf_size, bytes, size);
        buf_size += size;
        return *this;
    }

    if (buf_size > 0) [[likely]] {
        size_t fill_size = 64 - buf_size;
        std::memcpy(buf.t + buf_size, bytes, fill_size);
        size -= fill_size;
        bytes += fill_size;
        buf_size = 0;
        block_loop(hash.u, buf.t, 64);
    }

    size_t left_over = size % 64;
    size_t loop_size = size - left_over;
    block_loop(hash.u, bytes, loop_size);
    bytes += loop_size;

    std::memcpy(buf.t, bytes, left_over);
    buf_size = left_over;
    return *this;
}

str SHA256::get() {
    unsigned char arr[32];
    for (size_t i = 0; i < 8; ++i) {
        u32 be = to_be(hash.u[i]);
        std::memcpy(&arr[4 * i], &be, 4);
    }

    str s;
    s.resize(64);
    static constexpr char hex[] = "0123456789abcdef";
    for (size_t i = 0; i < 32; ++i) {
        s[2 * i + 0] = hex[arr[i] / 16];
        s[2 * i + 1] = hex[arr[i] % 16];
    }
    return s;
}
SHA256& SHA256::finalize() {
    alignas(64) char local_buf[block_size * 2] = {
        0,
    };
    std::memcpy(local_buf, buf.t, buf_size);
    local_buf[buf_size++] = (char)0x80;
    u64 be_length = to_be(length * 8);
    size_t S = sizeof be_length;
    buf_size = ((buf_size + S + 63) / 64) * 64;
    std::memcpy(local_buf + buf_size - S, &be_length, S);
    block_loop(hash.u, local_buf, buf_size);
    return *this;
}

extern char const* sha256_impl_generic;
void sha256_block_loop_generic(u32* hash, char const* data, size_t length);

char const* SHA256::impl = sha256_impl_generic;
void (*SHA256::block_loop)(u32* hash,
                           const char* data,
                           size_t size) = sha256_block_loop_generic;

#if defined(__ARM_ARCH)
extern char const* sha256_impl_arm64;
void sha256_block_loop_arm64(u32* hash, char const* data, size_t length);

#    if defined(__linux__)
#        include <sys/auxv.h>

static struct init
{
    init() {
        if (getauxval(AT_HWCAP) & HWCAP_SHA2) {
            SHA256::impl = sha256_impl_arm64;
            SHA256::block_loop = sha256_block_loop_arm64;
        }
    }
} init;

#    elif defined(__APPLE__)

static struct init
{
    init() {
        SHA256::impl = sha256_impl_arm64;
        SHA256::block_loop = sha256_block_loop_arm64;
    }
} init;

#    endif

#elif defined(__x86_64__)

extern char const* sha256_impl_x86_64;
void sha256_block_loop_x86_64(u32* hash, char const* data, size_t length);

static struct init
{
    static void cpuid(u32& eax,
                      u32& ebx,
                      u32& ecx,
                      u32& edx) {
        asm volatile("cpuid"
                     : "=a"(eax), "=b"(ebx), "=c"(ecx), "=d"(edx)
                     : "0"(eax), "2"(ecx));
    }
    static constexpr u32 bit(int index) { return u32(1) << index; }

    init() {
        u32 eax = 0, ebx = 0, ecx = 0, edx = 0;

        eax = 0, cpuid(eax, ebx, ecx, edx);
        if (eax < 7) return;

        eax = 1, cpuid(eax, ebx, ecx, edx);
        bool has_sse4_1 = edx & bit(19);

        eax = 7, ecx = 0, cpuid(eax, ebx, ecx, edx);
        bool has_sha = ebx & bit(29);

        if (has_sse4_1 && has_sha) {
            SHA256::impl = sha256_impl_x86_64;
            SHA256::block_loop = sha256_block_loop_x86_64;
        }
    }
} init;

#endif
