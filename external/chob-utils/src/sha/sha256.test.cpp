#include "sha/sha256.h"

#include "str/str.h"
#include "testing.h"

struct test_case
{
    const char* input;
    const char* expected;
};

test_case cases_string[] = {
    {"",  //
     "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"},

    {"a",  //
     "ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb"},

    {"ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb",
     "da3811154d59c4267077ddd8bb768fa9b06399c486e1fc00485116b57c9872f5"},

    {"e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855ca9781"
     "12ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bbda3811154d59"
     "c4267077ddd8bb768fa9b06399c486e1fc00485116b57c9872f5",
     "4e25c1c500d6de089d849816681f6feb6747caa067c4868569ffc069b7201440"},

    {0, 0},
};

TEST_CASE("SHA256 small strings") {
    for (test_case const* c = cases_string; c->input; c++) {
        str actual = SHA256()(c->input);

        REQUIRE(c->expected == actual);
    }
}

int main(int argc, char* argv[]) {
    std::cout << "testing " << SHA256::impl << "\n";
    return testing::main(argc, argv);
}
