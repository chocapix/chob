#include "str/algo.h"

#include "str/set.h"

str_vec& deduplicate(str_vec& vec) {
    str_set seen;
    size_t w = 0;
    for (size_t r = 0; r < vec.size(); ++r) {
        auto [_, inserted] = seen.emplace(vec[r]);
        if (!inserted) continue;

        if (w < r) vec[w] = vec[r];
        w++;
    }
    vec.resize(w);
    return vec;
}
