#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "str/str.h"

#include "str/algo.h"
#include "str/vec.h"
#include "testing.h"

TEST_CASE("simple") {
    str_view expected = "toto";
    str actual{expected};
    REQUIRE(expected == actual);
}

extern char const* test_strings[];

TEST_CASE("test_strings") {
    str_vec vec;
    for (size_t i = 0; test_strings[i]; ++i) vec.emplace_back(test_strings[i]);

    for (size_t i = 0; test_strings[i]; ++i)
        REQUIRE(vec[i] == str_view(test_strings[i]));
}

TEST_CASE("deduplicate") {
    str_vec v0{"a", "b", "c"}, v = v0;
    deduplicate(v);
    REQUIRE(v == v0);

    str_vec actual{"a", "b", "a", "c"}, expected{"a", "b", "c"};
    deduplicate(actual);
    REQUIRE(actual == expected);
}

int main(int argc, char** argv) { return testing::main(argc, argv); }
