#include <iostream>

#include "str/map.h"

#include "testing.h"

TEST_CASE("simple") {
    map<int, int> one;
    REQUIRE(one.size() == 0);
    one.emplace(1, 1);
    REQUIRE(one.size() == 1);
    REQUIRE(one[1] == 1);
}

extern char const* test_strings[];

TEST_CASE("test_strings") {
    map<str, size_t> sizes;
    size_t i = 0;
    for (i = 0; test_strings[i]; ++i) {
        REQUIRE(sizes.size() == i);
        str_view sv{test_strings[i]};
        sizes.emplace(sv, sv.size());
    }

    for (auto&& [word, size] : sizes) {
        REQUIRE(word.size() == size);
    }

    for (i = 0; test_strings[i]; ++i) {
        auto it = sizes.find(test_strings[i]);
        REQUIRE(it != sizes.end());
        auto&& [word, size] = *it;

        REQUIRE(word.size() == size);
    }
}

int main(int argc, char** argv) {
    return testing::main(argc, argv);
    // map<int, str> toto;
    //
    // for (int i = 0; i < argc; ++i) {
    //     toto.emplace(i, argv[i]);
    // }
    //
    // for (auto&& [i, arg] : toto) {
    //     std::cout << i << " -> " << str_view(arg) << "\n";
    // }
}
