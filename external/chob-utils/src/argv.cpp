#include <cassert>
#include <iostream>
#include <sstream>
#include <stdexcept>

#include "argv.h"

#include <str/algo.h>

argv::basic_opt::basic_opt(argv* a, char const* def)
    : def{def}
    , code{0} {
    parse(def);

    if (is_positional) a->posargs.push_back(this);
    else a->options.push_back(this);
}

void argv::basic_opt::parse(std::string_view def) {
    auto skip_spaces = [&]
    { def.remove_prefix(nfind_first_not_of(def, " \t\n")); };
    auto parse_name = [&]
    {
        size_t end = nfind_first_of(def, " \t\n,=");
        name = def.substr(0, end);
        def.remove_prefix(end);
    };

    skip_spaces();

    is_positional = !def.starts_with("-");
    if (is_positional) return parse_name();

    if (def.starts_with("--")) {
        def.remove_prefix(2);
        return parse_name();
    }

    assert(def.size() >= 2);
    code = def[1];
    def.remove_prefix(2);

    if (def.starts_with("=")) return;

    skip_spaces();
    if (!def.starts_with(",")) return;
    def.remove_prefix(1);
    skip_spaces();

    assert(def.starts_with("--"));
    def.remove_prefix(2);
    parse_name();
}

struct arg_view
{
    char** argv;
    int i;
    std::string_view str;

    arg_view()
        : argv{0}
        , i{0}
        , str{} {}
    arg_view(char** argv, int i)
        : argv{argv}
        , i{i} {
        if (argv[i]) str = argv[i];
    }

  private:
    arg_view(char** argv, int i, std::string_view str)
        : argv{argv}
        , i{i}
        , str{str} {}

  public:
    char operator[](size_t i) const { return str[i]; }

    static constexpr size_t npos = std::string_view::npos;

    size_t find(char c) const { return str.find(c); }

    arg_view substr(size_t pos, size_t count) const {
        return {argv, i, str.substr(pos, count)};
    }
    arg_view substr(size_t pos) const { return {argv, i, str.substr(pos)}; }

    void remove_prefix(size_t count) { str.remove_prefix(count); }
    char const* c_str() const {
        assert(str.data() == 0 || strlen(str.data()) == str.size());
        return str.data();
    }

    bool is_nil() const { return str.data() == 0; }
};

struct arg_error : std::runtime_error
{
    arg_view arg;
    std::string msg;

    arg_error(arg_view const& arg, std::string const& msg)
        : std::runtime_error(text(arg, msg)) {}

    static std::string text(arg_view const& arg, std::string const& msg) {
        std::stringstream ss;

        ss << arg.argv[0] << ":" << arg.i << ": " << msg << "\n";
        ss << "  " << arg.argv[arg.i] << "\n";
        ptrdiff_t begin = arg.str.data() - arg.argv[arg.i];
        for (ptrdiff_t i = 0; i < begin + 2; ++i) ss << " ";
        ss << "^";
        for (size_t i = 1; i < arg.str.length(); ++i) ss << "~";
        ss << "\n";

        return ss.str();
    }
};

static argv::basic_opt* get_opt(argv* a, std::string_view name) {
    for (argv::basic_opt* opt : a->options)
        if (opt->name == name) return opt;
    return nullptr;
}

static argv::basic_opt* get_opt(argv* a, char code) {
    for (argv::basic_opt* opt : a->options)
        if (opt->code == code) return opt;
    return nullptr;
}

struct process_info
{
    bool skip_next;
    bool code_break;
};

template<typename Key>
static process_info process(argv* a,
                            Key&& key,
                            arg_view name,
                            arg_view& value) {
    auto opt = get_opt(a, key);

    if (opt == 0) throw arg_error{name, "unknown option"};
    if (!opt->is_flag() && value.is_nil())
        throw arg_error{name, "missing argument"};

    bool name_value_inline = name.i == value.i;
    constexpr bool is_code = std::is_same_v<Key, char>;
    if constexpr (!is_code)
        if (opt->is_flag() && name_value_inline)
            throw arg_error{value, "unexpected argument"};

    bool parse_ok = opt->set(value.c_str());
    if (!parse_ok) throw arg_error{value, "couldn't parse argument"};

    bool skip_next = !opt->is_flag() && !name_value_inline;
    bool code_break = skip_next || !opt->is_flag();
    return {skip_next, code_break};
}

void argv::parse(int argc, char** argv) {
    progname = argv[0];
    size_t ipos = 0;
    bool is_past_dashdash = false;
    for (int i = 1; i < argc; ++i) {
        arg_view arg{argv, i};

        bool is_positional = arg[0] != '-' || arg[1] == 0 || is_past_dashdash;
        if (is_positional) {
            if (ipos >= posargs.size()) continue;
            bool parse_ok = posargs[ipos]->set(arg.c_str());
            if (!parse_ok) throw arg_error{arg, "couldn't parse argument"};
            ipos++;
            continue;
        }

        assert(arg[0] == '-');
        if (arg[1] == '-') {
            if (arg[2] == 0) {
                is_past_dashdash = true;
                continue;
            }

            arg.remove_prefix(2);

            arg_view name, value;
            size_t eq_pos = arg.find('=');
            if (eq_pos != arg.npos) {
                name = arg.substr(0, eq_pos);
                value = arg.substr(eq_pos + 1);
            } else {
                name = arg;
                value = arg_view{argv, i + 1};
            }

            i += process(this, name.str, name, value).skip_next;
            continue;
        }

        for (size_t j = 1; j < arg.str.size(); ++j) {
            arg_view code = arg.substr(j, 1);
            arg_view value;
            if (j + 1 < arg.str.size()) value = arg.substr(j + 1);
            else value = arg_view{argv, i + 1};

            auto info = process(this, code[0], code, value);
            i += info.skip_next;
            if (info.code_break) break;
        }
    }
}

void argv::print_help(char const* progname) {
    std::string msg = "Usage: ";
    msg += progname;
    if (options.size() > 0) msg += " [options]";
    for (auto&& opt : posargs) {
        msg += " [";
        msg += opt->name;
        msg += "]";
    }
    msg += '\n';

    if (!posargs.empty()) {
        msg += "\nPositionals:\n";
        for (auto&& opt : posargs) {
            msg += opt->def;
            msg += '\n';
        }
    }

    if (!options.empty()) {
        msg += "\nOptions:\n";
        for (auto&& opt : options) {
            msg += opt->def;
            msg += '\n';
        }
    }

    std::cout << msg;
}
