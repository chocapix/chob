#include <cmath>
#include <cstdint>
#include <cstring>
#include <stdexcept>
#include <string>
#include <string_view>
#include <utility>
#include <vector>

#include "pmap.h"

#include <sys/fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#include "util/file.h"
#include "util/wyhash++.h"

using namespace std::string_literals;

namespace
{
[[noreturn]] void pmap_error(std::string const& msg) {
    throw std::runtime_error(msg);
}

using u32 = std::uint32_t;
using u64 = std::uint64_t;

namespace filedesc
{
// All pmap files should start with the magic_string
constexpr std::string_view magic_string = "PMAPv1 \n";
static_assert(magic_string.size() == sizeof(u64));

// filedesc layout :
// - header
// - strings
// - 2 * strings.cap string objects
// - table
// - (mask + 1) buckets
//
// the strings are split in two parts: indices [0..cap) and [cap..2*cap)
// when we run out of space, we switch to the other part and reinsert
// all the strings from the table
//
// the table is a robin-hood style hashtable
// with the PSL and a portion of the hash merged to speed up some
// operations
struct header
{
    u64 magic;
};
struct strings
{
    u64 cap;
    u64 size;
    u64 free;
    u64 offset;
};
struct string
{
    u32 size;
};
struct table
{
    double load_factor;
    u64 mask;
    u64 cap;
    u64 size;
};
struct psl_hash
{
    u32 pslh;
};
struct bucket
{
    psl_hash pslh;
    u32 ikey;
    u32 ival;
};

static_assert(sizeof(header) % alignof(string) == 0);
static_assert(sizeof(strings) % alignof(string) == 0);

static_assert(sizeof(header) % alignof(bucket) == 0);
static_assert(sizeof(strings) % alignof(bucket) == 0);
static_assert(sizeof(string) % alignof(bucket) == 0);
static_assert(sizeof(table) % alignof(bucket) == 0);

}  // namespace filedesc

static constexpr u32 max_u32 = u32(-1);

struct data
{
    data(void* ptr, size_t size)
        : ptr{(char*)ptr}
        , end{this->ptr + size} {}

    size_t bytes() const { return size_t(end - ptr); }

    std::string_view view() const { return {ptr, bytes()}; }

    template<typename T>
    T* load(u64 count = 1) {
        if (count > bytes() / sizeof(T)) pmap_error("pmap: corrupt filedesc");
        if (reinterpret_cast<uintptr_t>(ptr) % alignof(T) != 0)
            pmap_error("pmap: corrupt filedesc");

        auto ret = (T*)(void*)ptr;
        u64 bytes = count * sizeof(T);
        ptr += bytes;
        return ret;
    }

  private:
    char* ptr;
    char* end;
};

struct header : filedesc::header
{
    static header* load(data& dat) {
        if (!dat.view().starts_with(filedesc::magic_string))
            pmap_error("pmap: not a pmap filedesc");
        auto ret = dat.load<header>();
        return ret;
    }
};

struct string : filedesc::string
{
    char const* data() const { return (char*)(void*)(this + 1); }
    char* data() { return (char*)(void*)(this + 1); }
    // char data[];

    static string* load(::data& dat, u64 count) {
        return dat.load<string>(count);
    }
};
static_assert(sizeof(string) == sizeof(u32));

struct strings : filedesc::strings
{
    string const* tab() const { return (string*)(void*)(this + 1); }
    string* tab() { return (string*)(void*)(this + 1); }
    // string tab[];

    std::string_view get(u64 index) const {
        string const& str = at(index);
        return {str.data(), str.size};
    }

    std::string_view get_other(u64 index) const {
        string const& str = at(index, cap - offset);
        return {str.data(), str.size};
    }

    static u64 slots(std::string_view text) { return slots(text.size()); }
    static u64 slots(u64 bytes) {
        return 1 + (bytes + sizeof(string) - 1) / sizeof(string);
    }

    bool fits(u64 slots) const { return cap - size >= slots; }

    bool would_fit_after_collect(u64 slots) const {
        return cap - size + free >= slots;
    }

    u32 put(std::string_view text) {
        u64 slots = strings::slots(text);
        u64 index = size;
        size += slots;
        if (index > max_u32) pmap_error("pmap: string index too large");
        return put_at(text, u32(index));
    }

    void replace(std::string_view text, u32 index) {
        u64 old_slots = slots(get(index));
        u64 new_slots = slots(text);
        free += old_slots - new_slots;

        put_at(text, index);
    }

    void remove(u32& index) {
        u64 slots = strings::slots(get(index));
        free += slots;
        index = 0;
    }

    void flip_copy() {
        u64 new_offset = cap - offset;
        std::memcpy(tab() + new_offset, tab() + offset, size * sizeof(string));
        offset = new_offset;
    }

    void flip_for_collect() {
        offset = cap - offset;
        tab()[offset].size = 0;
        size = 1;
        free = 0;
    }

    string const& at(u64 index) const { return at(index, offset); }
    string& at(u64 index) { return at(index, offset); }

    string const& at(u64 index, u64 offset) const {
        return tab()[offset + index];
    }
    string& at(u64 index, u64 offset) { return tab()[offset + index]; }

  private:
    u32 put_at(std::string_view text, u32 index) {
        if (text.size() > max_u32) pmap_error("pmap: string too long");
        auto size = u32(text.size());
        string& str = at(index);
        str.size = size;
        std::memcpy(str.data(), text.data(), size);
        return index;
    }

  public:
    static u64 bytes(u64 cap) {
        return sizeof(strings) + 2 * cap * sizeof(string);
    }

    static strings* load(data& dat) {
        strings* str = dat.load<strings>();
        str->check_valid();

        string* tab = string::load(dat, 2 * str->cap);
        if (tab != str->tab()) pmap_error("pmap: internal error");

        return str;
    }

    void check_valid() const {
        if (cap > max_u32) pmap_error("pmap: string tab too large");
        if (size > cap) pmap_error("pmap: corrupt string tab");
        if (free > size) pmap_error("pmap: corrupt string tab");
        if (!(offset == 0 || offset == cap))
            pmap_error("pmap: corrupt string tab");
    }
};

struct psl_hash : filedesc::psl_hash
{
    static constexpr int hash_bits = 8;
    static constexpr u32 hash_mod = u32(1) << hash_bits;

    static u32 psl(filedesc::psl_hash pslh) { return pslh.pslh / hash_mod; }
    static u32 hash(filedesc::psl_hash pslh) { return pslh.pslh % hash_mod; }

    static filedesc::psl_hash from_hash(u64 hash) {
        return {u32(hash % hash_mod) + hash_mod};
    }
};
void operator++(filedesc::psl_hash& pslh) { pslh.pslh += psl_hash::hash_mod; }
bool operator==(filedesc::psl_hash pslh, u32 val) { return pslh.pslh == val; }
bool operator!=(filedesc::psl_hash pslh, u32 val) { return pslh.pslh != val; }
bool operator==(filedesc::psl_hash a, filedesc::psl_hash b) {
    return a.pslh == b.pslh;
}
bool operator>(filedesc::psl_hash a, filedesc::psl_hash b) {
    return a.pslh > b.pslh;
}

struct table_index
{
    u64 mask;
    u64 index;
    operator u64() const { return index; }
    void operator++() { index = (index + 1) & mask; }
};

struct bucket : filedesc::bucket
{
};
struct table : filedesc::table
{
    // bucket tab[];
    bucket* tab() { return (bucket*)(void*)(this + 1); }
    bucket const* tab() const { return (bucket*)(void*)(this + 1); }

    table_index index(u64 hash) const { return {mask, hash & mask}; }

    bucket const* begin() const { return &tab()[0]; }
    bucket const* end() const { return &tab()[mask + 1]; }
    bucket* begin() { return &tab()[0]; }
    bucket* end() { return &tab()[mask + 1]; }

    void set_mask(u64 new_mask) {
        mask = new_mask;
        cap = capacity(mask, load_factor);
    }

    static u64 capacity(u64 mask, double load) {
        return u64(std::floor(load * double(mask + 1)));
    }

    static u64 bytes(u64 mask) {
        return sizeof(table) + (mask + 1) * sizeof(bucket);
    }

    static table* load(data& dat) {
        table* tab = dat.load<table>();
        tab->check_valid();

        bucket* buckets = dat.load<bucket>(tab->mask + 1);
        if (tab->tab() != buckets) pmap_error("pmap: internal error");

        return tab;
    }

    void check_valid() const {
        if (mask > max_u32) pmap_error("pmap: table too large");
        if (std::popcount(mask + 1) != 1)
            pmap_error("pmap: corrupt filedesc (table mask)");
        if (!(min_load <= load_factor && load_factor <= max_load))
            pmap_error("pmap: corrupt filedesc (load factor)");
        if (cap != capacity(mask, load_factor))
            pmap_error("pmap: corrupt filedesc (capacity)");
        if (size > cap) pmap_error("pmap: corrupt filedesc (size)");
    }

    static constexpr double min_load = 0.1;
    static constexpr double max_load = 1.0;
};

static_assert(sizeof(header) == sizeof(filedesc::header));
static_assert(sizeof(string) == sizeof(filedesc::string));
static_assert(sizeof(strings) == sizeof(filedesc::strings));
static_assert(sizeof(bucket) == sizeof(filedesc::bucket));
static_assert(sizeof(table) == sizeof(filedesc::table));

}  // namespace

struct pmap::impl
{
    mapped_file map;
    header* hdr;
    strings* str;
    table* tab;

    impl(std::string_view filename)
        : map(filename, "a+") {
        if (map.size() == 0)  //
            init();
        load();
    }

    impl(std::string_view filename, do_check)
        : impl(filename) {
        check_valid();
    }

    std::string_view filename() const { return map.filename(); }

    size_t size() const { return tab->size; }

    bool put(std::string_view key, std::string_view val) {
        if (tab->size >= tab->cap) [[unlikely]]
            tab_grow();

        auto hash = this->hash(key);
        auto pslh = psl_hash::from_hash(hash);
        auto index = tab->index(hash);
        for (;; ++index, ++pslh) {
            if (pslh == tab->tab()[index].pslh) {
                if (key == str->get(tab->tab()[index].ikey)) {
                    str_replace_val(val, index);
                    return false;
                }
            } else if (pslh > tab->tab()[index].pslh) {
                str_reserve(key, val);
                tab_insert({pslh, str->put(key), str->put(val)}, index);
                tab->size++;
                return true;
            }
        }
    }

    std::string_view get(std::string_view key) const {
        auto hash = this->hash(key);
        auto pslh = psl_hash::from_hash(hash);
        auto index = tab->index(hash);
        for (;; ++index, ++pslh) {
            if (pslh == tab->tab()[index].pslh) {
                if (key == str->get(tab->tab()[index].ikey))
                    return str->get(tab->tab()[index].ival);
            } else if (pslh > tab->tab()[index].pslh)  //
                return {};
        }
    }

    template<typename F>
    void foreach(F&& f) const {
        for (auto& b : *tab)
            if (b.pslh != 0) f(str->get(b.ikey), str->get(b.ival));
    }

    void tab_insert(bucket b, table_index index) {
        for (;; ++b.pslh, ++index) {
            if (tab->tab()[index].pslh == 0) {
                tab->tab()[index] = b;
                return;
            }
            b = std::exchange(tab->tab()[index], b);
        }
    }

    void tab_reinsert(bucket b) {
        u64 hash = this->hash(str->get(b.ikey));
        b.pslh = psl_hash::from_hash(hash);
        auto index = tab->index(hash);
        for (;; ++b.pslh, ++index)
            if (b.pslh > tab->tab()[index].pslh)  //
                return tab_insert(b, index);
    }

    // string helper

    void str_replace_val(std::string_view text, table_index index) {
        if (str->slots(str->get(tab->tab()[index].ival)) >= str->slots(text))
            return str->replace(text, tab->tab()[index].ival);
        str->remove(tab->tab()[index].ival);
        str_reserve(text);
        tab->tab()[index].ival = str->put(text);
    }

    // reallocation

    template<typename... T>
    void str_reserve(T&&... text) {
        str_reserve_slots((str->slots(text) + ...));
    }

    void str_reserve_slots(u64 slots) {
        if (str->fits(slots)) [[likely]]
            return;
        if (str->would_fit_after_collect(slots)) [[likely]]
            return str_collect();
        do {
            str_grow();
        } while (!str->fits(slots));
    }

    void str_collect() {
        str->flip_for_collect();

        for (auto&& b : *tab) {
            if (b.pslh != 0) {
                auto collect = [this](u32& index)
                {
                    if (index == 0) return;
                    std::string_view text = str->get_other(index);
                    index = u32(str->put(text));
                    if (index == 0) pmap_error("index = 0");
                };
                collect(b.ikey);
                collect(b.ival);
            }
        }
    }

    void str_grow() {
        if (str->offset != 0) str->flip_copy();
#ifndef NDEBUG
        check_valid();
#endif

        u64 new_bytes = 2 * str->cap * sizeof(string);
        file_extend(new_bytes);
        tab_move(new_bytes);

        str_expand();
    }

    void tab_grow() {
        if (str->offset != 0) str->flip_copy();
#ifndef NDEBUG
        check_valid();
#endif

        // careful with the order of operations here because tab_new() can
        // resize the filedesc and change this->tab
        table* new_tab = tab_new(tab->mask * 2 + 1);
        table* old_tab = tab;
        tab = new_tab;

        for (auto&& b : *old_tab)
            if (b.pslh != 0) tab_reinsert(b);

        str_expand();
    }

    void str_expand() {
        if (reinterpret_cast<uintptr_t>(tab) % alignof(strings) != 0)
            pmap_error("pmap: internal error (alignment)");
        string* str_end = str->tab() + 2 * str->cap;
        string* tab_begin = (string*)(void*)tab;
        str->cap += u64(tab_begin - str_end) / 2;
    }

    void tab_move(u64 offset) {
        auto old_location = (char*)(void*)tab;
        u64 bytes = table::bytes(tab->mask);
        auto new_location = (table*)(void*)(old_location + offset);
        std::memmove(new_location, old_location, bytes);
        tab = new_location;
    }

    table* tab_new(u64 mask) {
        table* new_tab = (table*)file_extend(table::bytes(mask));
        *new_tab = *tab;
        new_tab->set_mask(mask);
        return new_tab;
    }

    void* file_extend(u64 bytes) {
        u64 old_bytes = map.size();
        map.resize(old_bytes + bytes);
        load();
        return map.at(old_bytes);
    }

    static u64 hash(std::string_view text) {
        return wyhash::hash(text.data(), text.size());
    }

    // initialization

    void init() {
        double load_factor = 0.75;
        u64 str_cap = 7;
        u64 tab_mask = 3;
        u64 magic = 0;
        static_assert(filedesc::magic_string.size() == sizeof magic);
        std::memcpy(&magic, filedesc::magic_string.data(), sizeof magic);

        u64 bytes =
            sizeof(header) + strings::bytes(str_cap) + table::bytes(tab_mask);
        map.resize(bytes);
        char* ptr = (char*)map.data();
        auto append = [&](auto const& obj)
        {
            std::memcpy(ptr, &obj, sizeof obj);
            ptr += sizeof obj;
        };

        append(filedesc::header{.magic = magic});
        append(filedesc::strings{
            .cap = str_cap,
            .size = 1,
            .free = 0,
            .offset = 0,
        });
        for (u64 i = 0; i < 2 * str_cap; ++i)
            append(filedesc::string{.size = 0});
        append(filedesc::table{
            .load_factor = load_factor,
            .mask = tab_mask,
            .cap = table::capacity(tab_mask, load_factor),
            .size = 0,
        });
        for (u64 i = 0; i <= tab_mask; ++i)
            append(filedesc::bucket{
                .pslh = {0},
                .ikey = 0,
                .ival = 0,
            });
    }
    void load() {
        data dat{map.data(), map.size()};
        hdr = header::load(dat);
        str = strings::load(dat);
        tab = table::load(dat);
    }

    void check_valid() {
        std::vector<bool> used(str->cap, false);
        auto iset = [&](u64 index)
        {
            if (index >= str->size) pmap_error("pmap: string out of range");
            u64 size = strings::slots(str->at(index).size);
            if (size > str->size - index)
                pmap_error("pmap: string out of range");

            for (u64 i = 0; i < size; ++i) {
                u64 j = index + i;
                if (used[j]) pmap_error("pmap: string overlap");
                used[j] = true;
            }
        };

        u64 count = 0;
        iset(0);
        for (u64 i = 0; i <= tab->mask; ++i) {
            bucket const& b = tab->tab()[i];
            u32 psl = psl_hash::psl(b.pslh);

            if (psl == 0) {
                if (b.pslh != 0) pmap_error("pmap: invalid psl");
                continue;
            }

            ++count;

            if (b.ikey != 0) iset(b.ikey);
            else pmap_error("pmap: invalid indices");
            if (b.ival != 0) iset(b.ival);

            auto hash = this->hash(str->get(b.ikey));
            auto pslh = psl_hash::from_hash(hash);
            if (psl_hash::hash(pslh) != psl_hash::hash(b.pslh))
                pmap_error("pmap: invalid hash");

            u64 j = hash & tab->mask;
            u64 distance = j <= i ? i - j : tab->mask + 1 + i - j;

            if (distance + 1 != psl) pmap_error("pmap: invalid psl");
        }

        u64 used_count = 0;
        for (auto b : used) used_count += b;

        if (used_count != str->size - str->free)
            pmap_error("pmap: bad string count");

        if (count != tab->size) pmap_error("pmap: bad bucket count");
    }
};

pmap::pmap(std::string_view filename)
    : pimpl{new impl{filename}} {}
pmap::pmap(std::string_view filename, do_check)
    : pimpl{new impl{filename, do_check{}}} {}

pmap::~pmap() {}

size_t pmap::size() const { return pimpl->size(); }

bool pmap::put(std::string_view key, std::string_view val) {
    return pimpl->put(key, val);
}

std::string_view pmap::get(std::string_view key) const {
    return pimpl->get(key);
}

static void advance(pmap::iterator& it) {
    auto pos = (bucket const*)it.ptr;
    auto last = (bucket const*)it.end;

    while (pos < last && pos->pslh == 0) ++pos;
    it.ptr = pos;
}

pmap::iterator pmap::begin() const {
    pmap::iterator it{pimpl.get(), pimpl->tab->begin(), pimpl->tab->end()};
    advance(it);
    return it;
}

pmap::iterator& pmap::iterator::operator++() {
    auto pos = (bucket const*)ptr;
    ptr = pos + 1;
    advance(*this);
    return *this;
}

pmap::kv_pair pmap::iterator::operator*() const {
    auto b = (bucket const*)ptr;
    return {pimpl->str->get(b->ikey), pimpl->str->get(b->ival)};
}

std::string_view pmap::filename() const { return pimpl->filename(); }
