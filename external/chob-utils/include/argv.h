#pragma once

#include <sstream>
#include <vector>

// #include "str/str.h"

struct argv
{
    struct basic_opt
    {
        basic_opt(argv *a, char const* def);

        virtual ~basic_opt() {}
        virtual bool is_flag() const = 0;
        virtual bool set(char const* text) = 0;

        void parse(std::string_view def);
        std::string def;
        std::string_view name;
        char code;
        bool is_positional;
    };

    template<typename T>
    struct opt : basic_opt
    {
        T value;
        operator T const&() const { return value; }

        void operator=(T const& x) { value = x; }

        opt(argv *a, char const* def)
            : basic_opt(a, def)
            , value {} {}
        virtual bool is_flag() const { return false; }

        virtual bool set(char const* text) {
            if constexpr (std::is_same_v<T, std::string>) {
                value = text;
                return true;
            } else {
                std::stringstream ss {text};
                return bool(ss >> value);
            }
        }
    };

    struct flag : opt<size_t> {
        flag(argv *a, char const*def) : opt<size_t>(a, def) {
            value = 0;
        }
        virtual bool is_flag() const { return true; }

        virtual bool set(char const*) {
            value++;
            return true;
        }
    };

    void parse(int argc, char** argv);
    void print_help(char const* progname);

    std::string progname;
    std::vector<basic_opt*> options;
    std::vector<basic_opt*> posargs;
};
