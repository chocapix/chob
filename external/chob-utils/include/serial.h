#pragma once

#include <concepts>
#include <cstring>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "reflect.h"
#include "str/str.h"

namespace serial
{
template<typename T>
concept serializable = requires { typename T::is_serial; };

template<typename T>
concept basic_type = std::integral<T> || std::floating_point<T>;

struct serializer
{
    str buf;

    template<basic_type T>
    void write(T const& value) {
        append(&value, sizeof value);
    }

    template<serializable T>
    void write(T const& value) {
        reflect::for_each(
            [&](auto index) { write(reflect::get<index>(value)); }, value);
    }

    void write(std::string const& value) {
        write(value.size());
        buf.append(value);
    }
    void write(str const& value) {
        write(value.size());
        buf.append(value);
    }

    template<typename T, typename Alloc>
    void write(std::vector<T, Alloc> const& value) {
        write(value.size());
        for (auto&& elem : value) write(elem);
    }

    template<typename K, typename V, typename Hash, typename Eq, typename Alloc>
    void write(std::unordered_map<K, V, Hash, Eq, Alloc> const& value) {
        write(value.size());
        for (auto&& [k, v] : value) {
            write(k);
            write(v);
        }
    }

    template<typename V, typename Hash, typename Eq, typename Alloc>
    void write(std::unordered_set<V, Hash, Eq, Alloc> const& value) {
        write(value.size());
        for (auto&& v : value) write(v);
    }

  private:
    void append(const void* data, size_t size) {
        buf.append((char const*)data, size);
    }
};

struct deserializer
{
    str_view buf;

    template<basic_type T>
    void read(T& value) {
        str_view bytes = extract(sizeof value);
        std::memcpy(&value, bytes.data(), bytes.size());
    }

    template<serializable T>
    void read(T& value) {
        reflect::for_each([&](auto index) { read(reflect::get<index>(value)); },
                          value);
    }

    void read(std::string& value) {
        size_t size;
        read(size);
        value = extract(size);
    }
    void read(str& value) {
        size_t size;
        read(size);
        value = extract(size);
    }

    template<typename T, typename Alloc>
    void read(std::vector<T, Alloc>& value) {
        size_t size;
        read(size);
        value.resize(size);
        for (auto& elem : value) read(elem);
    }

    template<typename V, typename Hash, typename Eq, typename Alloc>
    void read(std::unordered_set<V, Hash, Eq, Alloc>& value) {
        size_t size;
        read(size);
        for (size_t i = 0; i < size; ++i) {
            V v;
            read(v);
            value.emplace(v);
        }
    }
    template<typename K, typename V, typename Hash, typename Eq, typename Alloc>
    void read(std::unordered_map<K, V, Hash, Eq, Alloc>& value) {
        size_t size;
        read(size);
        for (size_t i = 0; i < size; ++i) {
            K k;
            read(k);
            V v;
            read(v);
            value.emplace(k, v);
        }
    }

  private:
    str_view extract(size_t size) {
        if (size > buf.size())
            throw std::runtime_error("deserialize: unexpected end of input");
        str_view ret{buf.data(), size};
        buf.remove_prefix(size);
        return ret;
    }
};

template<typename T>
str to_str(T const& obj) {
    serializer ser;
    ser.write(obj);
    return std::move(ser.buf);
}

template<typename T>
T from_str(str_view data) {
    T x;
    deserializer deser{data};
    deser.read(x);
    return x;
}

}  // namespace serial
