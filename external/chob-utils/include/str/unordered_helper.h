#pragma once

#include <concepts>
#include <functional>

#include "str.h"
#include "util/wyhash++.h"

struct str_less : public std::less<str_view>
{
    using is_transparent = int;

    auto operator()(str const& a, str const& b) const {
        return std::less<str_view>::operator()(a, b);
    }
    auto operator()(str_view a, str const& b) const {
        return std::less<str_view>::operator()(a, b);
    }
    auto operator()(str const& a, str_view b) const {
        return std::less<str_view>::operator()(a, b);
    }
    auto operator()(str const& a, char const* b) const {
        return std::less<str_view>::operator()(a, b);
    }
    auto operator()(char const* a, str const& b) const {
        return std::less<str_view>::operator()(a, b);
    }
};

struct str_equal_to : public std::equal_to<str_view>
{
    using is_transparent = int;

    auto operator()(str const& a, str const& b) const {
        return std::equal_to<str_view>::operator()(a, b);
    }
    auto operator()(str_view a, str const& b) const {
        return std::equal_to<str_view>::operator()(a, b);
    }
    auto operator()(str const& a, str_view b) const {
        return std::equal_to<str_view>::operator()(a, b);
    }
    auto operator()(str const& a, char const* b) const {
        return std::equal_to<str_view>::operator()(a, b);
    }
    auto operator()(char const* a, str const& b) const {
        return std::equal_to<str_view>::operator()(a, b);
    }

    template<typename T>
    auto operator()(T const* a, T const* b) const {
        return a == b;
    }

    template<typename T>
    auto operator()(T* a, T* b) const {
        return a == b;
    }
};

struct str_stdhash : public std::hash<str_view>
{
    using is_transparent = int;
    using std::hash<str_view>::operator();

    auto operator()(str const& str) const {
        return std::hash<str_view>::operator()(str);
    }
    auto operator()(char const* str) const {
        return std::hash<str_view>::operator()(str);
    }
};

struct str_wyhash
{
    using is_transparent = int;

    auto hash(str_view str) const {
        return wyhash::hash(str.data(), str.size());
    }

    auto operator()(std::integral auto i) const {
        using wyhash::u64;
        return wyhash::hash(u64(i));
    }

    template<typename T>
    auto operator()(T const* ptr) const {
        return wyhash::hash(uintptr_t(ptr));
    }

    template<typename T>
    auto operator()(T* ptr) const {
        return wyhash::hash(uintptr_t(ptr));
    }

    auto operator()(str_view str) const { return hash(str); }
    auto operator()(str const& str) const { return hash(str); }
    auto operator()(char const* str) const { return hash(str); }
};

#define USE_WYHASH
#ifdef USE_WYHASH
using str_hash = str_wyhash;
#else
using str_hash = str_stdhash;
#endif
