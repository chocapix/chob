#pragma once

#include <unordered_set>

#include "str/unordered_helper.h"

template<typename T>
using set = std::unordered_set<T, str_hash, str_equal_to, allocator<T>>;

using str_set = set<str>;
