#pragma once

#include <unordered_map>

#include "unordered_helper.h"

template<typename K, typename V>
using map = std::unordered_map<K,
                               V,
                               str_hash,
                               std::equal_to<K>,
                               allocator<std::pair<K const, V>>>;

template<typename V>
using str_map = std::unordered_map<str,
                                   V,
                                   str_hash,
                                   str_equal_to,
                                   allocator<std::pair<const str, V>>>;


template<typename V>
using str_multimap =
    std::unordered_multimap<str,
                            V,
                            str_hash,
                            str_equal_to,
                            allocator<std::pair<const str, V>>>;
