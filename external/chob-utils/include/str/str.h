#pragma once

// Simple string class, expected to be used with the no-free allocator

#include <algorithm>
#include <cstddef>
#include <cstring>
#include <memory>
#include <string_view>
#include <type_traits>

#include "util/allocator.h"

// clang-format off
template<typename T>
concept string_view_like = requires(T a) {
   { a.data() } -> std::convertible_to<char const*>;
   { a.size() } -> std::convertible_to<std::size_t>;
};
// clang-format on

struct string_base
{
    using size_type = std::size_t;

  protected:
    static char empty_string[1];
    struct M
    {
        char* ptr;
        size_type len;
        size_type cap;
    } m;

    bool is_nil() const { return m.ptr == empty_string; }
};
inline char string_base::empty_string[1] = {0};

template<typename Alloc>
struct string
    : string_base
    , private Alloc
{
    // member types

    using size_type = std::size_t;
    using iterator = char*;
    using const_iterator = char const*;

    // member functions

    string() {
        m.ptr = empty_string;
        m.len = 0;
        m.cap = 0;
    }

    string(char const* s, size_type count) { init(s, count); }

    string(char const* s)
        : string{s, std::strlen(s)} {}

    string(string const& other)
        : string(other.m.ptr, other.m.len) {}

    string(string&& other)
        : string{} {
        swap(other);
    }

    template<string_view_like StringView>
    explicit string(StringView const& t)
        : string(t.data(), t.size()) {}

    string& operator=(string const& other) {
        assign(other.data(), other.size());
        return *this;
    }

    string& operator=(string&& other) {
        swap(other);
        return *this;
    }

    string& operator=(char const* s) {
        assign(s, std::strlen(s));
        return *this;
    }

    string& operator+=(char const* s) {
        append(s);
        return *this;
    }

    template<string_view_like StringView>
    string& operator+=(StringView const& t) {
        append(t.data(), t.size());
        return *this;
    }

    template<string_view_like StringView>
    string& operator=(StringView const& t) {
        assign(t.data(), t.size());
        return *this;
    }

    void assign(char const* s, size_type count) {
        reserve(count);
        char_copy(m.ptr, s, count);
        if (count < m.len) memset(m.ptr + count, 0, m.len - count);
        m.len = count;
    }

    void append(char const* s) { append(s, std::strlen(s)); }

    void append(char const* s, size_type count) {
        reserve(size() + count);
        char_copy(m.ptr + m.len, s, count);
        m.len += count;
    }

    template<string_view_like StringView>
    void append(StringView const& t) {
        append(t.data(), t.size());
    }

    void push_back(char c) {
        reserve(size() + 1);
        m.ptr[m.len++] = c;
    }

    // element access
    char operator[](size_t i) const { return m.ptr[i]; }
    char& operator[](size_t i) { return m.ptr[i]; }

    char const* data() const { return m.ptr; }
    char* data() { return m.ptr; }
    char const* c_str() const { return m.ptr; }

    operator std::string_view() const noexcept { return {m.ptr, m.len}; }

    // iterators

    iterator begin() { return m.ptr; }
    const_iterator begin() const { return m.ptr; }

    iterator end() { return m.ptr + m.len; }
    const_iterator end() const { return m.ptr + m.len; }

    // capacity

    bool empty() const { return size() == 0; }

    size_type size() const { return m.len; }

    void resize(size_type new_size) {
        reserve(new_size);
        if (new_size < m.len)
            std::memset(m.ptr + new_size, 0, m.len - new_size);
        m.len = new_size;
    }

    void reserve(size_type request) {
        if (request <= m.cap) return;

        size_type new_cap = m.cap * 2;
        if (request < new_cap) request = new_cap;
        realloc(request);
    }
    size_type capacity() const { return m.cap; }

    // modifiers

    void clear() {
        std::memset(m.ptr, 0, m.len);
        m.len = 0;
    }

    void swap(string& other) {
        M tmp = m;
        m = other.m;
        other.m = tmp;
    }

    // search

    size_type find(char ch, size_type pos = 0) const {
        return std::string_view(*this).find(ch, pos);
    }

    // operations

    bool starts_with(std::string_view sv) const {
        return std::string_view(*this).starts_with(sv);
    }
    bool starts_with(char const* text) const {
        return std::string_view(*this).starts_with(text);
    }

    bool ends_with(std::string_view sv) const {
        return std::string_view(*this).ends_with(sv);
    }
    bool ends_with(char const* text) const {
        return std::string_view(*this).ends_with(text);
    }

    string substr(size_type pos = 0, size_type count = npos) const {
        return string{std::string_view(*this).substr(pos, count)};
    }

    // constants

    static const size_type npos = size_type(-1);

  private:
    static void char_copy(char* dst, char const* src, size_type count) {
        std::copy(src, src + count, dst);
        // std::memcpy(dst, src, count);
    }

    void init(char const* s, size_type count) {
        m.ptr = allocate(count + 1);
        m.len = count;
        m.cap = count;
        char_copy(m.ptr, s, count);
        if constexpr (!allocator_zeroes) m.ptr[m.len] = '\0';
    }
    using Alloc::allocate;
    using Alloc::deallocate;
    void realloc(size_type new_cap) {
        char* new_ptr = allocate(new_cap + 1);
        char_copy(new_ptr, m.ptr, m.len);
        if constexpr (!allocator_zeroes)
            std::memset(new_ptr + m.len, 0, new_cap - m.len);

        dealloc();
        m.ptr = new_ptr;
        m.cap = new_cap;
    }

  public:
    ~string() { dealloc(); }

  private:
    void dealloc() {
        if (!is_nil()) deallocate(m.ptr, m.cap + 1);
    }
    template<typename T, typename, typename = void>
    struct is_zero_type
    {
        static constexpr bool value = false;
    };
    template<typename T, typename U>
    struct is_zero_type<T, U, std::void_t<typename T::is_zero_initialized>>
    {
        static constexpr bool value = true;
    };

  public:
    static constexpr bool allocator_zeroes = is_zero_type<Alloc, int>::value;
};

#ifdef DEBUG_STRING
#    include <string>
struct debug_string
{
    using size_type = std::size_t;
    std::string expected;
    string<allocator<char>> actual;

  private:
    debug_string(std::string const& expected,
                 string<allocator<char>> const& actual)
        : expected(expected)
        , actual(actual) {
        check();
    }

  public:
    debug_string() { check(); }

    debug_string(char const* s, size_type count)
        : expected(s, count)
        , actual(s, count) {
        check();
    }
    debug_string(char const* s)
        : expected(s)
        , actual(s) {
        check();
    }
    debug_string(debug_string const& other)
        : expected(other.expected)
        , actual(other.actual) {
        check();
    }
    debug_string(debug_string&& other)
        : expected(std::move(other.expected))
        , actual(std::move(other.actual)) {
        check();
    }
    template<string_view_like StringView>
    explicit debug_string(StringView const& t)
        : expected(t)
        , actual(t) {
        check();
    }

    debug_string& operator=(debug_string const& other) {
        expected = other.expected;
        actual = other.actual;
        check();
        return *this;
    }

    debug_string& operator=(debug_string&& other) {
        expected = std::move(other.expected);
        actual = std::move(other.actual);
        check();
        return *this;
    }

    debug_string& operator=(char const* s) {
        expected = s;
        actual = s;
        check();
        return *this;
    }

    template<string_view_like StringView>
    debug_string& operator=(StringView const& t) {
        expected = t;
        actual = t;
        check();
        return *this;
    }

    debug_string& operator+=(char const* s) {
        expected += s;
        actual += s;
        return *this;
    }

    template<string_view_like StringView>
    debug_string& operator+=(StringView const& t) {
        expected += t;
        actual += t;
        return *this;
    }

    void append(char const* s, size_type count) {
        expected.append(s, count);
        actual.append(s, count);
        check();
    }

    template<string_view_like StringView>
    void append(StringView const& t) {
        expected.append(t);
        actual.append(t);
        check();
    }

    void push_back(char c) {
        expected.push_back(c);
        actual.push_back(c);
        check();
    }

    struct char_ref
    {
        char& expected;
        char& actual;

        void operator=(char c) {
            expected = c;
            actual = c;
        }
    };
    char_ref operator[](size_type i) { return {actual[i], expected[i]}; }
    char operator[](size_type i) const { return actual[i]; }

    char const* data() const { return actual.data(); }
    char const* c_str() const {
        check();
        return actual.c_str();
    }
    operator std::string_view() const noexcept {
        check();
        return std::string_view(actual);
    }

    size_type size() const { return actual.size(); }

    bool empty() const { return actual.empty(); }

    void reserve(size_type new_cap) {
        expected.reserve(new_cap);
        actual.reserve(new_cap);
    }

    void resize(size_type new_size) {
        expected.resize(new_size);
        actual.resize(new_size);
        check();
    }

    size_type find(char ch, size_type pos = 0) const {
        return actual.find(ch, pos);
    }

    // operations

    bool starts_with(std::string_view sv) const {
        return actual.starts_with(sv);
    }
    bool starts_with(char const* text) const {
        return actual.starts_with(text);
    }

    bool ends_with(std::string_view sv) const { return actual.ends_with(sv); }
    bool ends_with(char const* text) const { return actual.ends_with(text); }

    debug_string substr(size_type pos = 0, size_type count = npos) const {
        return {expected.substr(pos, count), actual.substr(pos, count)};
    }
    void check() const {
        if (std::string_view(actual) != std::string_view(expected))
            throw std::runtime_error("bad string");
    }

    static constexpr size_type npos = string<allocator<char>>::npos;
};
using str = debug_string;
#else
using str = string<allocator<char>>;
#endif

using str_view = std::string_view;

inline str operator+(str const& a, str const& b) {
    str r = a;
    r += b;
    return r;
}

template<string_view_like SV>
inline str operator+(str const& a, SV const& b) {
    str r = a;
    r += b;
    return r;
}

template<string_view_like SV>
inline str operator+(SV const& a, str const& b) {
    auto r = str(a);
    r += b;
    return r;
}

#define define_operator(op)                                                    \
    inline bool operator op(str const& a, str const& b) {                      \
        return str_view(a) op str_view(b);                                     \
    }

define_operator(==);
define_operator(!=);
define_operator(<);
define_operator(<=);
define_operator(>=);
define_operator(>);

#undef define_operator

using namespace std::string_view_literals;
