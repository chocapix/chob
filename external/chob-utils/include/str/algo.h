#pragma once

#include <cassert>

#include "str/vec.h"

template<typename T, typename Offset>
auto at(str_view data, Offset offset) {
    assert(size_t(offset) < data.size());
    assert(ptrdiff_t(offset) >= 0);
    static_assert(std::is_integral_v<Offset>);
    return reinterpret_cast<T const*>(data.data() + offset);
}

template<typename T>
static inline void append1(str_vec& v, T&& arg) {
    if constexpr (std::is_convertible_v<T, std::string_view>
                  || std::is_convertible_v<T, std::string>)
    {
        v.emplace_back(arg);
    } else {
        for (auto&& x : arg) v.emplace_back(x);
    }
}

template<typename... T>
static inline void append(str_vec& v, T&&... arg) {
    (append1(v, std::forward<T>(arg)), ...);
}

template<typename T>
bool contains(str_vec const& set, T&& elem) {
    for (auto&& x : set)
        if (x == elem) return true;
    return false;
}

[[maybe_unused]] inline std::string_view operator-(std::string_view str,
                                                   std::string_view substr) {
    return str.substr(0, size_t(substr.data() - str.data()));
}

struct split_range
{
    std::string_view str;
    char delim;

    struct iterator
    {
        using difference_type = std::ptrdiff_t;
        using value_type = std::string_view;

        std::string_view curr;
        char const* end;
        char delim;

        struct end_t
        {
        };

        iterator() = default;
        iterator(iterator const&) = default;
        iterator(std::string_view s, char delim)
            : curr{s.data(), 0}
            , end{s.data() + s.size()}
            , delim{delim} {
            curr = next_word();
        }
        iterator(std::string_view s, char delim, end_t)
            : curr{s.data() + s.size(), 0}
            , end{s.data() + s.size()}
            , delim{delim} {}

        bool operator==(iterator const& i) const {
            return curr.data() == i.curr.data();
        }
        bool operator!=(iterator const& i) const {
            return curr.data() != i.curr.data();
        }
        const std::string_view* operator->() const { return &curr; }
        const std::string_view& operator*() const { return curr; }
        iterator& operator++() {
            curr = next_word();
            return *this;
        }
        iterator operator++(int) {
            iterator copy = *this;
            curr = next_word();
            return copy;
        }

        std::string_view next_word() {
            char const* ptr = curr.data() + curr.size();
            while (ptr < end && *ptr == delim) ++ptr;

            char const* word_end = ptr;
            while (word_end < end && *word_end != delim) ++word_end;

            return {ptr, size_t(word_end - ptr)};
        }
    };

    static_assert(std::forward_iterator<iterator>);

    iterator begin() const { return {str, delim}; }
    iterator end() const { return {str, delim, iterator::end_t{}}; }
};

[[maybe_unused]] inline split_range split(std::string_view str, char delim) {
    return {str, delim};
}

[[maybe_unused]] inline str_vec vsplit(std::string_view str, char delim) {
    str_vec vec;
    for (auto&& x : split(str, delim)) vec.emplace_back(x);
    return vec;
}

[[maybe_unused]] inline split_range lines(std::string_view str) {
    return {str, '\n'};
}

str_vec& deduplicate(str_vec& vec);

template<typename String>
size_t nfind(String&& str, char c) {
    size_t pos = str.find(c);
    return pos == str.npos ? str.size() : pos;
}

template<typename String>
size_t nfind_first_of(String&& str, std::string_view set) {
    size_t pos = str.find_first_of(set);
    return pos == str.npos ? str.size() : pos;
}

template<typename String>
size_t nfind_first_not_of(String&& str, std::string_view set) {
    size_t pos = str.find_first_not_of(set);
    return pos == str.npos ? str.size() : pos;
}

inline std::string_view skip(std::string_view text, std::string_view set) {
    return text.substr(nfind_first_not_of(text, set));
}

inline std::string_view skip_to(std::string_view text, std::string_view set) {
    return text.substr(nfind_first_of(text, set));
}

template<typename... T>
inline bool starts_with_any(std::string_view text, T&&... prefix) {
    return (false || ... || text.starts_with(prefix));
}
