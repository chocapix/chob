#pragma once

#include <vector>

#include "str.h"

template <typename T>
using vector = std::vector<T, allocator<T>>;
using str_vec = vector<str>;
