#pragma once

#include <cstdint>
#include <string_view>

#include "str/str.h"

struct SHA256
{
    static double seconds;
    static constexpr size_t block_size = 64;
    struct buffer
    {
        alignas(64) char t[block_size];
    };
    struct state
    {
        alignas(32) std::uint32_t u[8];
    };
    static state initial_hash;
    static std::uint32_t K[64];

  private:
    buffer buf;
    state hash;
    size_t buf_size;
    uint64_t length;

  public:
    static void (*block_loop)(std::uint32_t* hash,
                              const char* data,
                              size_t size);

    static const char* impl;

    SHA256() { reset(); }
    str operator()(std::string_view sv);

    SHA256& reset() {
        hash = initial_hash;
        buf_size = 0;
        length = 0;
        return *this;
    }
    SHA256& insert(std::string_view data) {
        return insert(data.data(), data.size());
    }
    SHA256& insert(const void* data, size_t size);
    SHA256& finalize();
    str get();
};
