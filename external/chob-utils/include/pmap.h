#pragma once

// #include <functional>
// #include <memory>
#include <memory>
#include <string_view>

/**
 * A persistant map.
 */
struct pmap
{
  private:
    struct impl;

  public:
    struct do_check
    {
    };

    pmap(std::string_view filename);
    pmap(std::string_view filename, do_check);
    ~pmap();

    std::string_view filename() const;

    size_t size() const;

    struct kv_pair
    {
        std::string_view key;
        std::string_view value;
    };
    struct end_t
    {
    };
    struct iterator
    {
        impl const* pimpl;
        void const* ptr;
        void const* end;
        using difference_type = std::ptrdiff_t;
        using value_type = kv_pair;

        kv_pair operator*() const;

        iterator& operator++();
        void operator++(int) { ++*this; }

        bool operator==(end_t) const { return ptr == end; }
    };
    static_assert(std::input_iterator<iterator>);

    iterator begin() const;
    end_t end() const { return {}; }

    std::string_view get(std::string_view key) const;
    bool put(std::string_view key, std::string_view val);

  private:
    std::unique_ptr<impl> pimpl;
};
