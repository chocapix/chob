#pragma once

// #include <iostream>
#include <string>
#include <string_view>

// #include <unistd.h>

#include "util/types.h"

struct prot_flags
{
    int prot;
    int flags;
    prot_flags(char const* fmode);
};

struct file_mapping : noncopyable
{
    file_mapping(int fd,
                 char const* mode,
                 size_t len,
                 std::string_view namev = "");
    ~file_mapping() {
        if (ptr != 0) unmap();
    }

    file_mapping(file_mapping&& other)
        : file_mapping() {
        swap(other);
    }
    file_mapping& operator=(file_mapping&& other) {
        swap(other);
        return *this;
    }

    std::string_view filename() const { return name; }

    std::string_view view() const { return {(char const*)ptr, len}; }

    size_t size() const { return len; }

    void* at(size_t offset) { return (char*)ptr + offset; }
    void* data() { return ptr; }

    void swap(file_mapping& other) {
        using std::swap;
        swap(name, other.name);
        swap(prot, other.prot);
        swap(len, other.len);
        swap(ptr, other.ptr);
    }

  private:
    file_mapping()
        : name()
        , prot(0)
        , len(0)
        , ptr(0) {}
    void remap(int fd, size_t size) {
        unmap();
        map(fd, size);
    }
    void map(int fd, size_t size);
    void unmap();

    std::string name;
    int prot;
    size_t len;
    void* ptr;
};

struct file : noncopyable
{
  private:
    std::string name;
    char const* mode;
    FILE* stream;
    bool unlink_on_close;

  public:
    struct temp_t
    {
    };
    file()
        : name()
        , mode(0)
        , stream(0)
        , unlink_on_close(false) {}
    file(std::string_view namev, char const* mode);
    file(temp_t, std::string_view tmplatev);

    file(file&& other)
        : file() {
        swap(other);
    }
    file& operator=(file&& other) {
        swap(other);
        return *this;
    }

    template<typename StringView>
    size_t write(StringView const& sv) {
        return write(sv.data(), 1, sv.size());
    }
    size_t write(void const* ptr, size_t size, size_t nitems);

    FILE* cfile() const { return stream; }

  private:
    file(FILE* stream);

  public:
    ~file();
    size_t size() const;
    file_mapping map() const;
    int fileno() const;

    static file temp();

    void swap(file& other) {
        // std::cerr << getpid() << " " << this << ": swap " << &other << "\n";
        using std::swap;
        swap(name, other.name);
        swap(mode, other.mode);
        swap(stream, other.stream);
        swap(unlink_on_close, other.unlink_on_close);
    }

  private:
    void close();
};

struct mapped_file : noncopyable
{
    mapped_file(std::string_view namev, char const* mode);
    ~mapped_file();

    std::string_view filename() const { return name; }
    size_t size() const { return len; }

    std::string_view view() const { return {(char const*)ptr, len}; }

    void* at(size_t offset) { return (char*)ptr + offset; }
    void* data() { return ptr; }

    void resize(size_t new_size);

  private:
    std::string name;
    int prot;
    int fd;
    void* ptr;
    size_t len;
};

struct tempdir
{
  private:
    std::string name;

  public:
    tempdir(std::string_view namev);
    ~tempdir();
    std::string_view path() const { return name; }
};
