#pragma once

struct noncopyable
{
    noncopyable() = default;
  private:
    noncopyable(noncopyable const&) = delete;
    void operator=(noncopyable const&) = delete;
};
