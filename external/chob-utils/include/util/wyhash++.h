#pragma once

#include <cstddef>
#include <cstdint>
#include <cstring>

#include <sys/time.h>

namespace wyhash
{

using u8 = std::uint8_t;
using u32 = std::uint32_t;
using u64 = std::uint64_t;
using u128 = __uint128_t;

// clang-format off
static inline u32 hi(u64  x) { return u32(x >> 32); }
static inline u64 hi(u128 x) { return u64(x >> 64); }

static inline u32 lo(u64  x) { return u32(x); }
static inline u64 lo(u128 x) { return u64(x); }

static inline u64  hi_lo(u32 h, u32 l) { return (u64(h) << 32) | l; }
static inline u32 make32(u8 x3, u8 x2, u8 x1, u8 x0)
{
    return 
        (u32(x3) << 24) |
        (u32(x2) << 16) |
        (u32(x1) <<  8) |
         u32(x0) 
    ;
}

static inline u128 wmul(u64 a, u64 b) { return u128(a) * b; }
static inline void mum(u64& a, u64& b) {
    u128 ab = wmul(a, b);
    a = lo(ab);
    b = hi(ab);
}
static inline u64 mix(u64 a, u64 b) {
    mum(a, b);
    return a ^ b;
}

#ifdef __LITTLE_ENDIAN__
static inline u64 to_le(u64 x) { return x; }
static inline u32 to_le(u32 x) { return x; }
#else
static inline u64 to_le(u64 x) { return __builtin_bswap64(x); }
static inline u32 to_le(u32 x) { return __builtin_bswap32(x); }
#endif
// clang-format on

static inline u64 read8(u8 const* data) {
    u64 val;
    std::memcpy(&val, data, sizeof val);
    return to_le(val);
}
static inline u32 read4(u8 const* data) {
    u32 val;
    std::memcpy(&val, data, sizeof val);
    return to_le(val);
}
static inline u32 read3(u8 const* data, size_t k) {
    return make32(0, data[0], data[k / 2], data[k - 1]);
}

static constexpr u64 secret0 = 0x2d358dccaa6c78a5ull;
static constexpr u64 secret1 = 0x8bb84b93962eacc9ull;
static constexpr u64 secret2 = 0x4b33a62ed433d4a3ull;
static constexpr u64 secret3 = 0x4d5a2da51de1aa47ull;

static constexpr u64 default_seed = 0xcc3e3d748c613659ull;

struct random_device
{
    random_device(u64 seed = default_seed)
        : seed{seed} {}
    u64 operator()() {
        u64 teed = time_seed();
        teed = mix(teed ^ secret0, seed ^ secret1);
        seed = mix(teed ^ secret0, secret2);
        return mix(seed, secret3);
    }

  private:
    static u64 time_seed() {
        timeval tv;
        gettimeofday(&tv, 0);
        return hi_lo(u32(tv.tv_sec), u32(tv.tv_usec));
    }
    u64 seed;
};

struct rand
{
    using result_type = u64;
    static constexpr u64 min() { return 0; }
    static constexpr u64 max() { return ~u64(0); }

    rand(u64 seed = default_seed)
        : state{seed} {}

    void seed(u64 seed) { state = seed; }

    u64 operator()() {
        state += secret0;
        return mix(state, state ^ secret1);
    }

    u64 operator()(u64 sup) {
        auto m = wmul(operator()(), sup);
        if (lo(m) < sup) {
            sup = -sup % sup;
            while (lo(m) < sup) m = wmul(operator()(), sup);
        }
        return hi(m);
    }

    u64 uniform(u64 lo, u64 hi) { return lo + operator()(hi - lo); }

  private:
    u64 state;
};

static inline u64 hash(u64 key) { return mix(key, u64(0x9e3779b97f4a7c15ull)); }

static inline u64 hash(void const* key, size_t len, u64& seed);
static inline u64 hash(void const* key, size_t len) {
    u64 seed = default_seed;
    return hash(key, len, seed);
}

template<typename T>
concept StringLike = requires(T str) {
                         sizeof str[0];
                         (const void*)str.data();
                         (std::size_t) str.size();
                     };

template<StringLike string>
static inline u64 hash(string&& str) {
    auto data = (void const*)str.data();
    auto len = (std::size_t)str.size();
    return hash(data, len * sizeof str[0]);
}

static inline u64 hash(char const* str) { return hash(str, std::strlen(str)); }

static inline u64 hash(void const* key, std::size_t len, u64& seed) {
    auto data = static_cast<u8 const*>(key);
    seed = mix(seed ^ secret0, secret1);
    u64 a, b;
    if (len <= 16) [[likely]] {
        if (len >= 4) [[likely]] {
            a = hi_lo(read4(data + 0),  //
                      read4(data + len / 8 * 4));
            b = hi_lo(read4(data + len - 4),
                      read4(data + len - 4 - len / 8 * 4));
        } else if (len > 0) [[likely]] {
            a = read3(data, len);
            b = 0;
        } else a = b = 0;
    } else {
        size_t i = len;
        if (i >= 48) [[unlikely]] {
            u64 see1 = seed, see2 = seed;
            do {
                [[likely]];
                seed = mix(read8(data + 0) ^ secret1, read8(data + 8) ^ seed);
                see1 = mix(read8(data + 16) ^ secret2, read8(data + 24) ^ see1);
                see2 = mix(read8(data + 32) ^ secret3, read8(data + 40) ^ see2);
                data += 48;
                i -= 48;
            } while (i >= 48);
            seed ^= see1 ^ see2;
        }
        while (i > 16) [[unlikely]] {
            seed = mix(read8(data + 0) ^ secret1, read8(data + 8) ^ seed);
            data += 16;
            i -= 16;
        }
        a = read8(data + i - 16);
        b = read8(data + i - 8);
    }
    a ^= secret1;
    b ^= seed;
    mum(a, b);
    return mix(a ^ secret0 ^ len, b ^ secret1);
}

}  // namespace wyhash
