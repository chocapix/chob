#pragma once

#include <string_view>

struct split_range
{
    std::string_view str;
    char delim;

    struct iterator
    {
        using difference_type = std::ptrdiff_t;
        using value_type = std::string_view;

        std::string_view curr;
        char const* end;
        char delim;

        struct end_t
        {
        };

        iterator() = default;
        iterator(iterator const&) = default;
        iterator(std::string_view s, char delim)
            : curr{s.data(), 0}
            , end{s.data() + s.size()}
            , delim{delim} {
            curr = next_word();
        }
        iterator(std::string_view s, char delim, end_t)
            : curr{s.data() + s.size(), 0}
            , end{s.data() + s.size()}
            , delim{delim} {}

        bool operator==(iterator const& i) const {
            return curr.data() == i.curr.data();
        }
        bool operator!=(iterator const& i) const {
            return curr.data() != i.curr.data();
        }
        const std::string_view* operator->() const { return &curr; }
        const std::string_view& operator*() const { return curr; }
        iterator& operator++() {
            curr = next_word();
            return *this;
        }
        iterator operator++(int) {
            iterator copy = *this;
            curr = next_word();
            return copy;
        }

        std::string_view next_word() {
            char const* ptr = curr.data() + curr.size();
            while (ptr < end && *ptr == delim) ++ptr;

            char const* word_end = ptr;
            while (word_end < end && *word_end != delim) ++word_end;

            return {ptr, size_t(word_end - ptr)};
        }
    };

    static_assert(std::forward_iterator<iterator>);

    iterator begin() const { return {str, delim}; }
    iterator end() const { return {str, delim, iterator::end_t{}}; }
};

inline split_range split(std::string_view str, char delim) {
    return {str, delim};
}
