#pragma once

#include <cstdio>
// #include <string_view>

#include <dirent.h>

namespace sys
{
using std::FILE;

FILE* fopen(char const* pathv, char const* mode);
FILE* fdopen(int filedes, char const* mode);
int fclose(FILE* stream);
size_t fwrite(void const* ptr, size_t size, size_t nitems, FILE* stream);
long ftell(FILE* stream);
int fseek(FILE* stream, long offset, int whence);

FILE* tmpfile();

int closedir(DIR*);
DIR* opendir(const char*);
struct dirent* readdir(DIR*);

char* realpath(char const* file_name, char* resolved_name);
};  // namespace sys
