#pragma once

#include <cstdlib>

#include <sys/types.h>
#include <sys/stat.h>

namespace sys
{
int open(const char* pathname, int flags, mode_t mode = 0);
int close(int filedes);
int unlink(char const* path);
int lstat(char const* path, struct stat *buf);

int chdir(char const*path);

off_t lseek(int fildes, off_t offset, int whence);
int ftruncate(int filedes, off_t length);

void* mmap(void* addr, size_t len, int prot, int flags, int fd, off_t offset);
int munmap(void* ptr, size_t len);

int mkstemp(char* tmplate);
char* mkdtemp(char* tmplate);

int dup2(int oldfd, int newfd);
pid_t fork();
int execvp(char const*path, char *const argv[]); 
int execve(char const*path, char *const argv[], char *const envp[]); 
int wait4(pid_t pid, int *stat_loc, int options, struct rusage *rusage);
}  // namespace sys
