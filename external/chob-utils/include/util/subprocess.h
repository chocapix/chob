#pragma once

#include <memory>
#include <string_view>
#include <type_traits>

#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>

#include "file.h"

struct subprocess
{
    struct status
    {
        pid_t pid;
        int status;
        rusage ru;

        bool success() const;
    };

    subprocess();
    ~subprocess();

    template<typename... T>
    subprocess(T&&... args)
        : subprocess() {
        argv(std::forward<T>(args)...);
    }

    template<typename... T>
    subprocess& argv(T&&... args) {
        (argv1(std::forward<T>(args)), ...);
        return *this;
    }

    subprocess& env(std::string_view key, std::string_view value);
    template<typename Range>
    subprocess& env(Range&& range) {
        for (auto&& [k, v] : range) env(k, v);
        return *this;
    }

    subprocess& throw_on_error(bool value);

    subprocess& out(FILE* stream);
    subprocess& err(FILE* stream);
    subprocess& outerr(FILE* stream) { return this->out(stream).err(stream); }

    subprocess& out(file& fil) { return out(fil.cfile()); }
    subprocess& err(file& fil) { return err(fil.cfile()); }
    subprocess& outerr(file& fil) { return outerr(fil.cfile()); }

    status run();
    pid_t detach();
    static status wait4(pid_t pid = -1, int options = 0);

  private:
    template<typename T>
    void argv1(T&& arg) {
        if constexpr (std::is_convertible_v<T, std::string_view>)  //
            add_argv(arg);
        else
            for (auto&& a : arg) argv1(a);
    }

    void add_argv(std::string_view arg);
    struct impl;
    std::unique_ptr<impl> pimpl;
};
