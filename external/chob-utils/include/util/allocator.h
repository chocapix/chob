#pragma once
#include <cstddef>

struct allocator_impl
{
    using is_zero_initialized = int;
    using size_type = std::size_t;

    template<size_type sz, size_type alignment>
    static void* alloc(size_type n) {
        size_type bytes = sz * n;
        index = align(index, alignment);
        if (index + bytes > capacity) [[unlikely]]
            grow(bytes);

        char* ptr = base + index;
        index += bytes;
        return ptr;
    }

    static void* debug_alloc(size_type bytes, size_type align);
    static void debug_dealloc(void* ptr, size_type bytes);

    static void print_stats();

  private:
    static char* base;
    static size_type index;
    static size_type capacity;

    static size_type align(size_type i, size_type a) {
        return (i + (a - 1)) / a * a;
    }

    static void grow(size_type bytes);
};

/**
 *  A very fast allocator that never release memory to the system and garantees
 * memory is zero-initialized.
 */
template<typename T>
struct fast_allocator : allocator_impl
{
    using value_type = T;
    template<typename U>
    struct rebind
    {
        using other = fast_allocator<U>;
    };
    fast_allocator() {}
    template<typename U>
    fast_allocator(fast_allocator<U>) {}

    T* allocate(size_type n) { return (T*)alloc<sizeof(T), alignof(T)>(n); }
    void deallocate(T*, size_type) {}
};

// Debug version, a poor man's ASAN
template<typename T>
struct debug_allocator : allocator_impl
{
    using value_type = T;
    template<typename U>
    struct rebind
    {
        using other = debug_allocator<U>;
    };
    debug_allocator() {}
    template<typename U>
    debug_allocator(debug_allocator<U>) {}

    T* allocate(size_type n) {
        return (T*)debug_alloc(sizeof(T) * n, alignof(T));
    }
    void deallocate(T* p, size_type n) { debug_dealloc(p, sizeof(T) * n); }
};

#ifdef NDEBUG
template<typename T>
using allocator = fast_allocator<T>;
#else
template<typename T>
using allocator = debug_allocator<T>;
#endif

template<typename T>
bool operator==(allocator<T> const&, allocator<T> const&) {
    return true;
}
template<typename T>
bool operator!=(allocator<T> const&, allocator<T> const&) {
    return false;
}
