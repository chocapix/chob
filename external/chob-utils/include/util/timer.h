#pragma once

struct timer
{
    using time_point = long long;

    struct string
    {
        char buf[16];
        operator char const*() const { return buf; }
    };

    static string str(double seconds) noexcept;

    struct duration
    {
        time_point dur{0};

        double seconds() const;

        string str() const { return ::timer::str(seconds()); }

        duration& operator+=(duration other) {
            dur += other.dur;
            return *this;
        }
    };

    timer()
        : start(now()) {}

    static time_point now();

    static duration elapsed(time_point start, time_point stop) {
        return {stop - start};
    }
    duration elapsed(time_point now) const { return elapsed(start, now); }
    duration elapsed() const { return elapsed(now()); }

  private:
    time_point start;
};

struct profiler
{
    static profiler* head;
    char const* name;
    timer::duration duration;
    long count;
    profiler* next;
    unsigned long depth;

    static void enable(bool val = true);

    profiler(char const* name)
        : name(name)
        , count(0)
        , next(head)
        , depth(0)  //
    {
        head = this;
    }

    struct scope
    {
        timer tim;
        profiler* prof;

        scope(profiler* prof)
            : tim{}
            , prof{prof}  //
        {
            prof->depth++;
        }

        void done() {
            if (prof == 0) return;
            prof->depth--;
            if (prof->depth == 0) {
                prof->count++;
                prof->duration += tim.elapsed();
            }
            prof = 0;
        }

        ~scope() { done(); }
    };

    [[nodiscard]] scope operator()() { return {this}; }

    ~profiler();
};

#define TIMER_CAT_(a, b) a##b
#define TIMER_CAT(a, b) TIMER_CAT_(a, b)

#define PROFILE_FUNCTION                                                       \
    static profiler TIMER_CAT(profile_, __LINE__){__PRETTY_FUNCTION__};        \
    auto TIMER_CAT(profile_obj_, __LINE__) = TIMER_CAT(profile_, __LINE__)()

#define PROFILE_SCOPE(name)                                                    \
    static profiler TIMER_CAT(profile_, __LINE__){name};                       \
    auto TIMER_CAT(profile_obj_, __LINE__) = TIMER_CAT(profile_, __LINE__)()
