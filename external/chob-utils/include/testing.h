#pragma once

#include <cassert>
#include <functional>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <string_view>
#include <vector>

namespace testing
{
struct test_case
{
    char const* name;
    std::function<void(void)> fun;

    test_case(char const* name, std::function<void(void)> fun)
        : name{name}
        , fun{fun}  //
    {}
};

static struct test_cases
{
    std::vector<test_case> cases;
    size_t assert_count = 0;

    ~test_cases() {
        std::cout << assert_count << " assertions passed\n"
                  << cases.size() << " test cases\n";
    }
} test_cases;

static inline int main(int argc, char** argv) {
    (void)argc, (void)argv;

    try {
        for (auto&& test : test_cases.cases) test.fun();
    } catch (std::runtime_error& err) {
        std::cerr << "error: " << err.what() << "\n";
        return 2;
    } catch (...) {
        std::cerr << "unknown error\n";
        return 2;
    }

    return 0;
}

struct test_case_adder
{
    test_case_adder(char const* name, std::function<void(void)> fun) {
        test_cases.cases.emplace_back(name, fun);
    }
};

[[noreturn]] inline void require_fail(char const* cond,
                                      char const* func,
                                      char const* file,
                                      int line) {
    std::string_view filev{file};
    size_t pos = filev.find_last_of('/');
    if (pos != filev.npos) filev.remove_prefix(pos + 1);

    std::stringstream ss;
    ss << "REQUIRE failed (" << cond << "), function " << func << " in " << file
       << ":" << line;
    throw std::runtime_error(ss.str());
}
}  // namespace testing

#define CAT_(pre, post) pre##post
#define CAT(pre, post) CAT_(pre, post)

#define UNIQUE(pre) CAT(pre, __LINE__)

#define TEST_CASE_(name, uname)                                                \
    static void uname();                                                       \
    static testing::test_case_adder CAT(add_, uname){name, uname};             \
    static void uname()

#define TEST_CASE(name) TEST_CASE_(name, UNIQUE(test_))

#define REQUIRE(cond)                                                          \
    do {                                                                       \
        if (cond) {                                                            \
            testing::test_cases.assert_count++;                                \
            break;                                                             \
        }                                                                      \
        testing::require_fail(#cond, __FUNCTION__, __FILE__, __LINE__);        \
    } while (0)
