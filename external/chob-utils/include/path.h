#pragma once

#include "util/types.h"

#include <sys/stat.h>

#ifdef CHECKED_STRING
#    include <filesystem>
#    include <stdexcept>
#    define path_assert(cond)                                                  \
        do {                                                                   \
            if (cond) break;                                                   \
            throw std::runtime_error("path failed");                           \
        } while (0)
#    include <cassert>
namespace fs = std::filesystem;
#else
#    define path_assert(cond) ((void)0)
#endif
#include "str/str.h"
#include "str/vec.h"

inline str operator/(str_view a, str_view b) {
    if (b.starts_with('/')) return str{b};

    str r;
    r.reserve(a.size() + 1 + b.size());
    r.append(a);
    if (!a.ends_with('/')) r.push_back('/');
    r.append(b);
    return r;
}

namespace path
{
using u64 = std::uint64_t;
enum options : u64
{
    no_options = 0,
    no_follow = 1,
    recursive = 2,
    overwrite = 4,
    relative = 8,
};
inline options operator|(options a, options b) {
    return options(u64(a) | u64(b));
}

template<typename... StringViewLike>
inline str make(str_view first, StringViewLike&&... others) {
    size_t size = first.size()
        + ((1 + str_view{std::forward<StringViewLike>(others)}.size()) + ...
           + 0);
    str r;
    r.reserve(size);
    r.append(first);
    ((r.push_back('/'), r.append(std::forward<StringViewLike>(others))), ...);
    return r;
}

inline bool is_absolute(str_view path) {
    bool r = path.starts_with('/');
    path_assert(r == fs::path(path).is_absolute());
    return r;
}

inline str_view parent(str_view path) {
    size_t slash = path.find_last_of('/');
    size_t end = slash == path.npos ? 0 : slash == 0 ? 1 : slash;
    while (end > 1 && path[end - 1] == '/') --end;
    str_view r{path.data(), end};
    path_assert(r == fs::path(path).parent_path());
    return r;
}

inline str_view filename(str_view path) {
    size_t slash = path.find_last_of('/');
    if (slash != path.npos) path.remove_prefix(slash + 1);
    path_assert(path == fs::path(path).filename());
    return path;
}

inline str replace_filename(str_view path, str_view rep) {
    size_t slash = path.find_last_of('/');

    if (slash == path.npos) return str{rep};

    str_view par = path.substr(0, slash);
    str r;
    r.reserve(par.size() + 1 + rep.size());
    r.append(par);
    r.push_back('/');
    r.append(rep);

    path_assert(str_view{r} == fs::path{path}.replace_filename(rep));
    return r;
}

inline str_view extension(str_view path) {
    str_view name = filename(path);
    size_t dot = name.find_last_of('.');
    if (dot == name.npos || dot == 0) {
        str_view r = path;
        r.remove_prefix(r.size());
        path_assert(r == fs::path(path).extension());
        return r;
    }
    size_t end = name.size();
    str_view r{name.data() + dot, size_t(end - dot)};
    path_assert(r == fs::path(path).extension());
    return r;
}

inline str_view without_extension(str_view path) {
    str_view name = filename(path);
    size_t dot = name.find_last_of('.');
    if (dot == name.npos || dot == 0) {
        str_view r = path;
        path_assert(r == fs::path(path).replace_extension());
        return r;
    }

    char const* begin = path.data();
    char const* end = &name[dot];
    str_view r{begin, size_t(end - begin)};
    path_assert(r == fs::path(path).replace_extension());
    return r;
}

inline str replace_extension(str_view path, str_view ext = {}) {
    str_view path_we = without_extension(path);
    str r;
    r.reserve(path_we.size() + ext.size());
    r.append(path_we);
    r.append(ext);
    path_assert(str_view{r} == fs::path(path).replace_extension(ext));
    return r;
}

inline bool is_prefix(str_view pre, str_view path) {
    if (!path.starts_with(pre)) return false;
    if (path.size() == pre.size()) return true;
    return path[pre.size()] == '/';
}

str_view relative_to(str_view full, str_view base);
str current();
void chdir(str_view path);

struct scoped_wd
{
    str prev;
    scoped_wd(str_view path)
        : prev(current()) {
        chdir(path);
    }
    ~scoped_wd() { chdir(prev); }
};
[[nodiscard]] inline scoped_wd push_dir(str_view path) { return {path}; }

str absolute(str_view path);

str_vec files(str_view dir, options opt = no_options);
str_vec directories(str_view dir, options opt = no_options);

void create_symlink(str_view target, str_view link, options opt = no_options);
void copy(str_view from, str_view to, options opt = no_options);

void create_dir(str_view path);
void create_dirs(str_view path);
inline void create_parent(str_view path) { create_dirs(parent(path)); }

bool exists(str_view path, options opt = no_options);

struct stat : ::stat {
    bool is_dir() const { return st_mode & S_IFDIR; }
};

struct stat status(str_view path);

void remove(str_view path);
void remove_all(str_view path);

str realpath(str_view path);
str lookup(str_view name);

}  // namespace path
