#include "util/timer.h"

#include "nanobench.h"

double next(double x, double lo, double hi) {
    x *= 17.;
    if (x > hi)
        x = lo;
    return x;
}

int main() {
	PROFILE_FUNCTION;

    using namespace ankerl::nanobench;
    using namespace std::chrono_literals;

	profiler::enable(getenv("CHOB_PROFILE") != 0);

    double x = 1e-12;
    Bench bench;
    bench.minEpochTime(50ms);
    bench.run("generate",
              [&]
              {
                  x = next(x, 1e-12, 1e4);
                  doNotOptimizeAway(x);
              });
    bench.run("convert str_view",
              [&]
              {
                  x = next(x, 1e-12, 1e4);
                  auto text = timer::str(x);
                  doNotOptimizeAway(text);
              });
}
