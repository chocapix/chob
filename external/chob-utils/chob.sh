lib chobutils

cflags += -std=gnu++20
cflags += -Wall -Wextra -Wpedantic -Wconversion -Wsign-conversion
#flags debug += -fsanitize=address

if [ -n "$__ARM_ARCH" ]; then
    cflags src/sha/sha256_block_loop.o += -mcpu=generic+crypto
elif [ -n "$__x86_64__" ]; then
    cflags src/sha/sha256_block_loop.o += -msse4.1 -msha
fi
