#!/bin/sh

set -e

dir=..

if [ -n "$1" ]; then
	dir="$1"
fi

sed -e "s|@CHOB_UTILS_ROOT@|$dir|" <"$dir/docs/Doxyfile.in" >Doxyfile

doxygen Doxyfile

