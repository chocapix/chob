# This a stripped down version of fmt 10.2.1 (no docs or tests)
lib fmt

cflags += -std=gnu++20
cflags += -Wall -Wextra -Wpedantic -Wconversion -Wsign-conversion
#flags debug += -fsanitize=address
