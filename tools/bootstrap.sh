#!/bin/bash

set -e

root="$1"
objs="$(realpath ./bootstrap)"

if [ -z "$CXX" ]; then
	CXX=c++
fi

cflags="-std=gnu++20 -Wall -Wextra -Wconversion -Wsign-conversion -g"

cd $root

run() {
	echo "$@"
	"$@"
}

fmt_objs=""
for i in external/fmt/src/*.cc; do
	mkdir -p "$(dirname $objs/$i.o)"
	fmt_objs+=" $objs/$i.o"
	run $CXX $cflags -Iexternal/fmt/include -c -o $objs/$i.o $i &
done
wait
run ar -crs $objs/libfmt.a $fmt_objs

utils_objs=""
for i in \
	external/chob-utils/src/*.cpp \
	external/chob-utils/src/*/*.cpp \
	external/chob-utils/src/*/*/*.cpp
do
	if [[ $i =~ .*\.test\.cpp ]]; then continue; fi

	mkdir -p "$(dirname $objs/$i.o)"
	utils_objs+=" $objs/$i.o"
	run $CXX $cflags -Iexternal/chob-utils/include -c -o $objs/$i.o $i &
done
wait
run ar -crs $objs/libchob-utils.a $utils_objs

chob_objs=""
for i in src/*.cpp src/*/*.cpp; do
	if [[ $i =~ .*\.test\.cpp ]]; then continue; fi

	mkdir -p "$(dirname $objs/$i.o)"
	chob_objs+=" $objs/$i.o"

	run $CXX $cflags -Iexternal/chob-utils/include -Iexternal/fmt/include \
		-Isrc -c -o $objs/$i.o $i &
done
wait 
run $CXX -o $objs/chob $chob_objs $objs/libchob-utils.a $objs/libfmt.a

run cp -vr data/share $objs
