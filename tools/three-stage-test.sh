#!/bin/sh


set -e

mkdir build-stage1
mkdir build-stage2
mkdir build-stage3

echo Stage 1...
cd build-stage1
# cmake -GNinja -DCMAKE_BUILD_TYPE=Debug ..
# cmake --build .
../tools/bootstrap.sh ..
cd ..

echo Stage 2...
cd build-stage2
../build-stage1/bootstrap/chob ..
cd ..

echo Stage 3...
cd build-stage3
../build-stage2/chob .. --test
ctest -j
cd ..

diff \
    build-stage2/chob \
    build-stage3/chob 



