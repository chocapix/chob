#!/bin/sh

set -e

mode=$1

mkdir -p build-$mode
cd build-$mode

cmake -GNinja -DCMAKE_BUILD_TYPE=$mode ..
cmake --build .

ctest
