#!/bin/sh

set -e

dir=..

if [ -n "$1" ]; then
	dir="$1"
fi

dir="$dir/external/chob-utils"
dir="$(realpath $dir)"

"$dir/tools/doxygen.sh" $dir

